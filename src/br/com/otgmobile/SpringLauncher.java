package br.com.otgmobile;

import org.apache.log4j.Logger;

import br.com.otgmobile.config.Spring;
import br.com.otgmobile.server.MaxtrackCommandResponsesDaemon;
import br.com.otgmobile.server.MaxtrackCommandsDaemon;
import br.com.otgmobile.server.MaxtrackPositionsDaemon;
import br.com.otgmobile.service.socket.SocketIOTask;

public class SpringLauncher {
	private static final Logger logger = Logger.getLogger(SpringLauncher.class);
	private static Spring spring;
	
	
	public static synchronized Spring getSpring(){
		return spring;
	}
	
	public SpringLauncher() {
		try {
			spring = new Spring();
			//PilotgpsDaemonUDP pilotgpsDaemonUDP = spring.getPilotGPSDaemonUDP();
			//pilotgpsDaemonUDP.start();
//			PilotgpsDaemonTCP pilotgpsDaemonTCP = spring.getPilotGPSDaemonTCP();
//			pilotgpsDaemonTCP.start();
			MaxtrackPositionsDaemon maxtrackDaemon = spring.getMaxtrackPositionsDaemon();
			maxtrackDaemon.start();
			MaxtrackCommandsDaemon maxtrackCommandsDaemon = spring.getMaxtrackCommandsDaemon();
			maxtrackCommandsDaemon.start();
			MaxtrackCommandResponsesDaemon maxtrackCommandResponseDaemon = spring.getMaxtrackCommandResponsesDaemon();
			maxtrackCommandResponseDaemon.start();
//			NiagaraDaemon niagaraDaemon = spring.getNiagaraDaemon();
//			niagaraDaemon.start();
//			TK10XDaemon tk10xDaemon = spring.getTK10XDaemon();
//			tk10xDaemon.start();
//			SkypatrolDaemon skypatrolDaemon = spring.getSkypatrolServiceDaemon();
//			skypatrolDaemon.start();
//			NodeInitializer nodeInitializer = spring.getNodeInitializer();
//			nodeInitializer.start();
			SocketIOTask socketIODaemon = spring.getSocketIODaemon();
			socketIODaemon.start();
		} catch (Exception e) {
			logger.error(e.getMessage(), e.getCause());
		}
	}

	public static void main(String[] args) {
		logger.info("Starting System...");
		new SpringLauncher();
		logger.info("System initialized.");
	}

}
