package br.com.otgmobile.model;

public enum DeviceType {

	TK10X,
	ASPICORE,
	MAXTRACK,
	SKYPATROL,
	NIAGARA2,
	NIAGARAHW6
}
