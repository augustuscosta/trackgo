package br.com.otgmobile.model.skypatrol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class SkypatrolParser {
	
	private static final Logger logger = Logger.getLogger(SkypatrolParser.class);
	
	private final String finalCharacter = "$";
	private final String splitCharacter = ",";
	private final String ackString = "+SACK:";
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyyMMddHHmmss");
	private final StringBuilder ackStringBuilder = new StringBuilder();

	private final String message;
	private SkypatrolHeartbeat heartbeat;
	private SkypatrolMessage skypatrolMessage;

	public SkypatrolParser(String message) {
		this.message = message;
		if(isHeartbeat())
			heartbeat = parseHeartbeat();
		else if(isReportMessage())
			skypatrolMessage = parse();
	}
	
	public SkypatrolMessage getSkypatrolMessage() {
		return skypatrolMessage;
	}
	
	public SkypatrolHeartbeat getHeartbeat() {
		return heartbeat;
	}


	private SkypatrolMessage parse(){
		String[] data = getMessageData();
		SkypatrolMessage message = new SkypatrolMessage();
		message.setReportItemMask(getReportItenMask(data));
		message.setSackEnabled(getSackEnabled(data));
		message.setMessageType(getMessageType(data));
		message.setProtocolVersion(getProtocolVersion(data));
		message.setUniqueID(getUniqueId(data));
		message.setDeviceName(getDeviceName(data));
		message.setReportId(getReportId(data));
		message.setReportType(getReportType(data));
		message.setNumber(getNumber(data));
		message.setGpsAccuracy(getGpsAccuracy(data));
		message.setSpeed(getSpeed(data));
		message.setAzimuth(getAzimuth(data));
		message.setAltitude(getAltitude(data));
		message.setLongitude(getLongitude(data));
		message.setLatitude(getLatitude(data));
		message.setGpsUtcTime(getGpsUtcTime(data));
		message.setMobileCountryCode(getMobileCountryCode(data));
		message.setMobileNetworkCode(getMobileNetworkCode(data));
		message.setLocationAreaCode(getLocationAreaCode(data));
		message.setCellId(getCellId(data));
		message.setBatteryPercentage(getBatteryPercentage(data));
		message.setSendTime(getSendTime(data));
		message.setCountNumber(getCountNumber(data));
		return message;
	}
	
	private SkypatrolHeartbeat parseHeartbeat(){
		String[] data = getMessageData();
		SkypatrolHeartbeat heartbeat = new SkypatrolHeartbeat();
		heartbeat.setReportItemMask(getReportItenMask(data));
		heartbeat.setSackEnabled(getSackEnabled(data));
		heartbeat.setMessageType(getMessageType(data));
		heartbeat.setProtocolVersion(getProtocolVersion(data));
		heartbeat.setUniqueID(getUniqueId(data));
		heartbeat.setDeviceName(getDeviceName(data));
		heartbeat.setSendTime(getHeartBeatSendTime(data));
		heartbeat.setCountNumber(getHeartBeatCountNumber(data));
		return heartbeat;
	}
	
	public String getAcknowledgement(){
		if(isHeartbeat())
			return getHeartbeatAcknowledgement(heartbeat);
		else
			return getMessageAcknowledgement(skypatrolMessage);
	}
	
	public boolean needAcknowledgement(){
		if(isHeartbeat())
			return heartbeat.getSackEnabled();
		else
			return skypatrolMessage.getSackEnabled();
	}
	
	public String getHeartbeatAcknowledgement(SkypatrolHeartbeat heartbeat){
		ackStringBuilder.setLength(0);
		ackStringBuilder.append(ackString);
		ackStringBuilder.append(heartbeat.getCountNumber());
		ackStringBuilder.append(finalCharacter);
		return ackStringBuilder.toString();
	}
	
	public String getMessageAcknowledgement(SkypatrolMessage message){
		ackStringBuilder.setLength(0);
		ackStringBuilder.append(ackString);
		ackStringBuilder.append(message.getCountNumber());
		ackStringBuilder.append(finalCharacter);
		return ackStringBuilder.toString();
	}
	
	public boolean isReportMessage() {
		String[] data = getMessageData();
		if(data.length != 24){
			return false;
		}
		return true;
	}
	
	public boolean isHeartbeat() {
		String[] data = getMessageData();
		if(data.length != 9){
			return false;
		}
		return true;
	}
	
	private Integer getHeartBeatCountNumber(String[] data) {
		return parseIntegerHexValue(data, 8);
	}
	
	private Date getHeartBeatSendTime(String[] data) {
		return parseDateValue(data, 7);
	}

	private Integer getCountNumber(String[] data) {
		return parseIntegerHexValue(data, 23);
	}
	
	private Date getSendTime(String[] data) {
		return parseDateValue(data, 22);
	}

	private Integer getBatteryPercentage(String[] data) {
		return parseIntegerValue(data, 21);
	}

	private Integer getCellId(String[] data) {
		return parseIntegerValue(data, 20);
	}

	private Integer getLocationAreaCode(String[] data) {
		return parseIntegerHexValue(data, 19);
	}

	private Integer getMobileNetworkCode(String[] data) {
		return parseIntegerValue(data, 18);
	}

	private Integer getMobileCountryCode(String[] data) {
		return parseIntegerValue(data, 17);
	}

	private Date getGpsUtcTime(String[] data) {
		return parseDateValue(data, 16);
	}

	private Double getLatitude(String[] data) {
		return parseDoubleValue(data, 15);
	}

	private Double getLongitude(String[] data) {
		return parseDoubleValue(data, 14);
	}

	private Double getAltitude(String[] data) {
		return parseDoubleValue(data, 13);
	}

	private Integer getAzimuth(String[] data) {
		return parseIntegerValue(data, 12);
	}

	private Double getSpeed(String[] data) {
		return parseDoubleValue(data, 11);
	}

	private Integer getGpsAccuracy(String[] data) {
		return parseIntegerValue(data, 10);
	}

	private Integer getNumber(String[] data) {
		return parseIntegerValue(data, 9);
	}

	private Integer getReportType(String[] data) {
		return parseIntegerValue(data, 8);
	}

	private Integer getReportId(String[] data) {
		return parseIntegerValue(data, 7);
	}

	private String getDeviceName(String[] data) {
		return parseStringValue(data,6);
	}

	private String getUniqueId(String[] data) {
		return parseStringValue(data,5);
	}

	private Integer getProtocolVersion(String[] data) {
		 return parseIntegerHexValue(data, 4);
	}

	private SkypatrolMessageType getMessageType(String[] data) {
		return SkypatrolMessageType.getValue(parseStringValue(data, 3));
	}

	private Boolean getSackEnabled(String[] data) {
		String value = parseStringValue(data, 2);
		if(value.length() == 0)
			return null;
		if(value.equals("0"))
			return false;
		
		return true;
	}

	private String getReportItenMask(String[] data) {
		return parseStringValue(data,1);
	}
	
	private String parseStringValue(String[] data, int index){
		String value = data[index];
		if(value == null)
			return null;
		value = value.trim();
		if(value.length() == 0)
			return null;
		return value;
	}
	
	private Integer parseIntegerValue(String[] data, int index){
		String value = data[index];
		if(value == null)
			return null;
		value = value.trim();
		if(value.length() == 0)
			return null;
		return Integer.parseInt(value);
	}
	
	private Integer parseIntegerHexValue(String[] data, int index){
		String value = data[index];
		if(value == null)
			return null;
		value = value.trim();
		if(value.length() == 0)
			return null;
		return Integer.parseInt(value,16);
	}
	
	private Double parseDoubleValue(String[] data, int index){
		String value = data[index];
		if(value == null)
			return null;
		value = value.trim();
		if(value.length() == 0)
			return null;
		return Double.parseDouble(value);
	}
	
	private Date parseDateValue(String[] data, int index){
		String value = data[index];
		if(value == null)
			return null;
		value = value.replace(" ", "");
		if(value.length() == 0)
			return null;
		Date toReturn = null;
		try {
			toReturn = dateFormater.parse(value);
		} catch (ParseException e) {
			logger.error("Erro fazendo o parser da data: " + value,e);
		}
		return toReturn;
	}

	private String[] getMessageData(){
		String cutMessage = message.replace(finalCharacter, "");
		String[] data = cutMessage.split(splitCharacter);
		return data;
	}
}
