package br.com.otgmobile.model.skypatrol;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "comando_skypatrol")
public class ComandoSkypatrol {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "skypatrol_sequence", sequenceName = "skypatrol_sequence")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private ComandoSkypatrolType comandoSkypatrolType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ComandoSkypatrolType getComandoSkypatrolType() {
		return comandoSkypatrolType;
	}

	public void setComandoSkypatrolType(ComandoSkypatrolType comandoSkypatrolType) {
		this.comandoSkypatrolType = comandoSkypatrolType;
	}

}
