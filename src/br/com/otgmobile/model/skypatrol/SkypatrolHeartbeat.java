package br.com.otgmobile.model.skypatrol;

import java.util.Date;

public class SkypatrolHeartbeat {

	private String reportItemMask;
	private Boolean sackEnabled;
	private SkypatrolMessageType messageType;
	private Integer protocolVersion;
	private String uniqueID;
	private String deviceName;
	private Date sendTime;
	private Integer countNumber;
	
	public String getReportItemMask() {
		return reportItemMask;
	}
	public void setReportItemMask(String reportItemMask) {
		this.reportItemMask = reportItemMask;
	}
	public Boolean getSackEnabled() {
		return sackEnabled;
	}
	public void setSackEnabled(Boolean sackEnabled) {
		this.sackEnabled = sackEnabled;
	}
	public SkypatrolMessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(SkypatrolMessageType messageType) {
		this.messageType = messageType;
	}
	public Integer getProtocolVersion() {
		return protocolVersion;
	}
	public void setProtocolVersion(Integer protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public Integer getCountNumber() {
		return countNumber;
	}
	public void setCountNumber(Integer countNumber) {
		this.countNumber = countNumber;
	}
	
	
}
