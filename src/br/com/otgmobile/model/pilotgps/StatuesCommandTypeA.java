package br.com.otgmobile.model.pilotgps;

public class StatuesCommandTypeA {
	
	private boolean alarm;
	private int login;
	private boolean heartbeat;
	
	public boolean isAlarm() {
		return alarm;
	}
	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
	}
	public int getLogin() {
		return login;
	}
	public void setLogin(int login) {
		this.login = login;
	}
	public boolean isHeartbeat() {
		return heartbeat;
	}
	public void setHeartbeat(boolean heartbeat) {
		this.heartbeat = heartbeat;
	}
		
}
