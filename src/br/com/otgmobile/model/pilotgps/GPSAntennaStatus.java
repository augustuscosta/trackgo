package br.com.otgmobile.model.pilotgps;

public enum GPSAntennaStatus {
	UNKNOWN(11),
	CUT(10),
	SHORT(01),
	OK(00);
	private int value;
	
	private GPSAntennaStatus(int value) {
        this.value = value;
	}
}
