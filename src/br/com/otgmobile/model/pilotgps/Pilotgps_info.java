package br.com.otgmobile.model.pilotgps;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="pilotgps_info")
public class Pilotgps_info {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="pilotgps_info_ser_sequence", sequenceName="pilotgps_info_sequence")
	private Long id;
	
	private Boolean save;
	private String deviceID;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getSave() {
		return save;
	}
	public void setSave(Boolean save) {
		this.save = save;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

}
