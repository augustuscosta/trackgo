package br.com.otgmobile.model.aspicore;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.events.EventMessage;
import br.com.otgmobile.model.Message;

@Entity
@Table(name = "aspicore")
public class AspicoreMessage implements Message, EventMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "aspicore_sequence", sequenceName = "aspicore_sequence")
	private Long id;
	private String fullPacket = "";
	private String imei = "";
	private GPSData GPS;
	private GSMData outCell;
	private GSMData inCell;
	private String label = "";
	private boolean testMessage;

	public AspicoreMessage(String udpPacket) {
		outCell = new GSMData();
		inCell = new GSMData();
		GPS = new GPSData();
		testMessage = false;
		fullPacket = new String(udpPacket);
		parse();
	}

	private void parse() {
		fullPacket = fullPacket.replaceAll("\r", "");
		String[] items = fullPacket.split("\n");
		for (int i = 0; i < items.length; i++) {
			items[i] = items[i].replaceAll("\n", "");
			Pattern imeiPat = Pattern.compile("\\bIMEI (\\d+)\\b");
			Matcher match = imeiPat.matcher(items[i]);
			if (match.find()) {
				imei = match.group(1);
			} else {
				Pattern testPat = Pattern.compile("\\bGPRS Test Msg\\b");
				match = testPat.matcher(items[i]);
				if (match.find()) {
					testMessage = true;
				} else {
					Pattern outCellPat = Pattern
							.compile("\\bOutCell (\\d+) LAC (\\d+) Name ([\\S ]*) MCC (\\d+) MNC (\\d+)\\b");
					match = outCellPat.matcher(items[i]);
					if (match.find()) {
						outCell.setCellId(Long.valueOf(match.group(1))
								.longValue());
						outCell.setLocationArea(Long.valueOf(match.group(2))
								.longValue());
						outCell.setName(match.group(3));
						outCell.setCountryCode(Integer.valueOf(match.group(4))
								.intValue());
						outCell.setNetworkCode(Integer.valueOf(match.group(5))
								.intValue());
					} else {
						Pattern inCellPat = Pattern
								.compile("\\bIn Cell (\\d+) LAC (\\d+) Name ([\\S ]*) MCC (\\d+) MNC (\\d+)\\b");
						match = inCellPat.matcher(items[i]);
						if (match.find()) {
							inCell.setCellId(Long.valueOf(match.group(1))
									.longValue());
							inCell.setLocationArea(Long.valueOf(match.group(2))
									.longValue());
							inCell.setName(match.group(3));
							inCell.setCountryCode(Integer.valueOf(
									match.group(4)).intValue());
							inCell.setNetworkCode(Integer.valueOf(
									match.group(5)).intValue());
						} else {
							Pattern gpsPat = Pattern.compile("\\$GPRMC");
							match = gpsPat.matcher(items[i]);
							if (match.find()) {
								GPS.setGPRMC(items[i]);
							} else {
								Pattern labelPat = Pattern
										.compile("\\bLabel ([\\S ]*)\\b");
								match = labelPat.matcher(items[i]);
								if (match.find()) {
									label = match.group(1);
								}
							}
						}
					}
				}
			}
		}
	}

	public String getImei() {
		return imei;
	}

	public GPSData getGPS() {
		return GPS;
	}

	public GSMData getOutCell() {
		return outCell;
	}

	public GSMData getInCell() {
		return inCell;
	}

	public String getLabel() {
		return label;
	}

	public boolean isTestMessage() {
		return testMessage;
	}
	
	public Long getId() {
		return id;
	}

	@Override
	public Double getLatitude() {
		if(GPS != null){
			return GPS.getLat();
		}
		return null;
	}

	@Override
	public Double getLongitude() {
		if(GPS != null){
			return GPS.getLong();
		}
		return null;
	}

	@Override
	public Integer getAltitude() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCourse() {
		if(GPS != null){
			return GPS.getCourse();
		}
		return null;
	}

	@Override
	public Double getSpeed() {
		if(GPS != null){
			return GPS.getSpeed();
		}
		return null;
	}

	@Override
	public Boolean getMoving() {
		if(GPS != null){
			return GPS.getSpeed() > 03;
		}
		return null;
	}

	@Override
	public Date getDate() {
		if(GPS != null){
			return GPS.getTime().getTime();
		}
		return null;
	}

	@Override
	public Boolean getIgnition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getPanic() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRpm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getOpenedDoor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getEngineBlocked() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getDateInMills() {
		if(getDate() == null){
			return 0;
		}
		return getDate().getTime();
	}
}
