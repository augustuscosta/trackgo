package br.com.otgmobile.model.aspicore;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Embeddable;

@Embeddable
public class GPSData {
  private String gprmcLine = "";
  private Calendar posTime;
  private char status;
  private double posLat = 999;
  private double posLong = 999;
  private double speed = -1;
  private double course = -1;
  
  public GPSData() {
  	posTime = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
  }
  
  public GPSData(String gprmc) {
  	posTime = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
  	setGPRMC(gprmc);
  }
  
  public void setGPRMC(String gprmc) {
  	Matcher match;
    this.gprmcLine = new String(gprmc);
    Pattern gpsPat = Pattern.compile("\\$GPRMC,([\\d\\.]*),([AV]),(\\d\\d)([\\d\\.]*),([NS]),(\\d\\d\\d)([\\d\\.]*),([EW]),([\\d\\.]*),([\\d\\.]*),(\\d\\d\\d\\d\\d\\d),[\\S ]*\\b");
    match = gpsPat.matcher(gprmc);
	if (match.find()) {
      //group(2) = status
	  this.status = match.group(2).charAt(0);
	  //group(3) = lat_deg_part, group(4) = lat_min_part
	  this.posLat = Double.valueOf(match.group(3)).doubleValue() +
	    Double.valueOf(match.group(4)).doubleValue()/60.0;
	  //group(6) = long_deg_part, group(7) = long_min_part
	  this.posLong = Double.valueOf(match.group(6)).doubleValue() +
	    Double.valueOf(match.group(7)).doubleValue()/60.0;
	  //group(5) = N/S, group(8) = E/W
	  if (match.group(5).equalsIgnoreCase("S")) posLat *= -1.0;
	  if (match.group(8).equalsIgnoreCase("W")) posLong *= -1.0;
	  if (!match.group(9).equalsIgnoreCase(""))
	    this.speed = Double.valueOf(match.group(9)).doubleValue();
	  if (!match.group(10).equalsIgnoreCase(""))
	  	this.course = Double.valueOf(match.group(10)).doubleValue();
      //group(1) = utc_time, group(11) = utc_date
	  String timeDate;
	  if (match.group(11).equalsIgnoreCase("")) {
	  	StringBuffer currentDate = new StringBuffer();
	  	currentDate.append(posTime.get(Calendar.DAY_OF_MONTH));
	  	currentDate.append(posTime.get(Calendar.MONTH)+1);
	  	currentDate.append(posTime.get(Calendar.YEAR)%100);
	  	System.out.println(currentDate.toString());
	  	timeDate = currentDate.toString()+match.group(1)+"+0000";
	  } else timeDate = match.group(11)+match.group(1)+"+0000";
	  DateFormat formater = new SimpleDateFormat("ddMMyyHHmmss.SSSZ");
	  try {
	    posTime.setTime(formater.parse(timeDate));
	  } catch (ParseException e) {
	    e.printStackTrace();
	  }
	}
  }
  
  public void setTime(Calendar posTime) {
  	this.posTime = posTime;
  }
  
  public void setStatus(char status) {
  	this.status = status;
  }
  
  public void setLat(double posLat) {
  	this.posLat = posLat;
  }
  
  public void setLong(double posLong) {
  	this.posLong = posLong;
  }
  
  public void setSpeed(double speed) {
  	this.speed = speed;
  }
  
  public void setCourse(double course) {
  	this.course = course;
  }
  
  public String getGPRMC() {
  	return gprmcLine;
  }
  
  public Calendar getTime() {
  	return posTime;
  }
  
  public char getStatus() {
  	return status;
  }
  
  public double getLat() {
  	return posLat;
  }
  
  public double getLong() {
  	return posLong;
  }
  
  public double getSpeed() {
  	return speed;
  }
  
  public double getCourse() {
  	return course;
  }
}
