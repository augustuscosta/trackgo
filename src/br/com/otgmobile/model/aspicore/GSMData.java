/*
 * Created on Jan 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package br.com.otgmobile.model.aspicore;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author coriley
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
@Embeddable
public class GSMData {
  @Column(insertable=false,updatable=false)
  private long cellId = 0;
  @Column(insertable=false,updatable=false)
  private long locationArea = 0;
  @Column(insertable=false,updatable=false)
  private String name = "";
  @Column(insertable=false,updatable=false)
  private int countryCode = 0;
  @Column(insertable=false,updatable=false)
  private int networkCode = 0;
  
  public void setCellId(long cid) {
  	this.cellId = cid;
  }
  
  public void setLocationArea(long lac) {
  	this.locationArea = lac;
  }
  
  public void setName(String name) {
  	this.name = name;
  }
  
  public void setCountryCode (int mcc) {
  	this.countryCode = mcc;
  }
  
  public void setNetworkCode (int mnc) {
  	this.networkCode = mnc;
  }
  
  public long getCellId() {
  	return cellId;
  }
  
  public long getLocationArea() {
  	return locationArea;
  }
  
  public String getName() {
  	return name;
  }
  
  public int getCountryCode() {
  	return countryCode;
  }
  
  public int getNetworkCode() {
  	return networkCode;
  }
}
