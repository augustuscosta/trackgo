package br.com.otgmobile.model.events;

import java.io.Serializable;

import br.com.otgmobile.model.DeviceType;

public class EventOcurred implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4630022560172846425L;

	
	public EventOcurred(EventHistoryDetail eventDetail){
		if(eventDetail != null){
			setEventHistoryDetailId(eventDetail.getId());
			setMetricDate(eventDetail.getMetricDate().getTime());
			setLatitude(eventDetail.getLatitude());
			setLongitude(eventDetail.getLongitude());
		}
		
		if(eventDetail.getEvent() != null){
			setEventId(eventDetail.getEvent().getId());
			setEventName(eventDetail.getEvent().getName());
			setEventDescription(eventDetail.getEvent().getDescription());
		}
		
		if(eventDetail.getDevice() != null){
			setDeviceId(eventDetail.getDevice().getId());
			setDeviceCode(eventDetail.getDevice().getCode());
			setDeviceType(eventDetail.getDevice().getDeviceType());
			setDeviceName(eventDetail.getDevice().getName());
		}
	}
	
	private long eventHistoryDetailId;
	private Long eventId;
	private String eventName;
	private String eventDescription;
	private String deviceDescription;
	private DeviceType deviceType;
	private Long deviceId;
	private String deviceCode;
	private String deviceName;
	private Float latitude;
	private Float longitude;
	private Long metricDate;


	public Long getMetricDate() {
		return metricDate;
	}
	public void setMetricDate(Long metricDate) {
		this.metricDate = metricDate;
	}
	public long getEventHistoryDetailId() {
		return eventHistoryDetailId;
	}
	public void setEventHistoryDetailId(long eventHistoryDetailId) {
		this.eventHistoryDetailId = eventHistoryDetailId;
	}
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDescription() {
		return eventDescription;
	}
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}
	public String getDeviceDescription() {
		return deviceDescription;
	}
	public void setDeviceDescription(String deviceDescription) {
		this.deviceDescription = deviceDescription;
	}
	public DeviceType getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

}
