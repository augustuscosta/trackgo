package br.com.otgmobile.model.niagarahw06;

import java.util.BitSet;

import org.apache.commons.codec.binary.Hex;

import br.com.otgmobile.util.ByteUtil;

public class NiagaraHW06ServerMessageBody {
	
	public static String generateServerMessageBody(NiagaraHW06ServerMessageType serMessageType, Boolean activate, NiagraHw06APNConfiguration apnConfiguration, NiagaraServerConfiguration serverConfiguration){
		
		switch (serMessageType) {
		case CONFIGURATE_SLEEP_MODE:
			
			return configureDeepSleepMode();
	
		case ENABLE_WARNING_MESSAGES:
			
			return enableWarningMessages();
		
		case CONFIGURE_KEEP_ALIVE_MESSAGE:
			
			return configureAliveMessage();
		
		case DOP_CONFIGURATION:
			
			return dopConfiguration();
		
		case ISP_CONFIGURATION_SETTINGS:
			
			return ispConfigurationSettings(apnConfiguration);
		
		case SERVER_IP_CONFIGURATION:
			
			return serverIpConfiguration(serverConfiguration);
		
		case TRACKER_TIME_CONFIGURATION:
			
			return trackerTimerConfiguration();
			
		case DEEP_SLEEP_TRANSTION_TIME_CONFIGURATION:
			
			return deepSleepModeConfiguration();

		case START_EMERGENCY_TRACKING_PERIOD:
			
			return startEmergencyTrackingPeriod();
			
		case REQUEST_IMMEDIATE_POSITION:
			
			return requestImediatePosition();
			
		case WAKE_UP_SOFT_SLEEP_MODE:
			
			return wakeUpSleepMode();
			
		case RETURN_SOFT_SLEEP_MODE:
			
			return softSleepMode();

		case RESET_ODOMETER:
			
			return resetOdometer();
			
		case TELEMETRY_TRANSPARENT_COMMAND:
			
			return telemetry_transtransperentCommand();
			
		case SERVER_ACK:
			
			return null;
			
		case SERVER_NACK:
			
			return null;
			
		case START_A_NEW_FIRMWARE_UPDATE_DOWNLOAD:
			
			return startFirmWareDownload();
			
		case ABORT_FIRMWARE_DOWNLOAD:
			
			return abortFirmwareDownload();
			
		case NEW_DATA_PACKAGE:
			
			return newDataPackage();
			
		case FIRMWARE_SWITCH:
			
			return firmwareSwitch();
			
		case QUERY_FIRMWARE_VERSION:
			
			return queryFirWareVersion();
			
		case ACTUATOR_TURN_ON_AND_OFF:
			
			return getEngineBlock(activate);

		default:
			return null;
		}
	}

	private static String configureDeepSleepMode() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String enableWarningMessages() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String configureAliveMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	// Set The GPS Margin of error to get a Tracking Position
	private static String dopConfiguration() {
		return null;
	}

	//Mobile network apn settings
	private static String ispConfigurationSettings(NiagraHw06APNConfiguration apnConfiguration) {
		StringBuilder builder = new StringBuilder();
		builder.append(Integer.toHexString(apnConfiguration.getApn().length()));
		builder.append(convertToASCII(apnConfiguration.getApn()));
		builder.append(Integer.toHexString(apnConfiguration.getUserName().length()));
		builder.append(Integer.toHexString(apnConfiguration.getApn().length()));
		builder.append(Integer.toHexString(apnConfiguration.getPassword().length()));
		builder.append(Integer.toHexString(apnConfiguration.getApn().length()));		
		return builder.toString();
	}

	private static Object convertToASCII(String apn) {
		String a = "";
		for (Character character : apn.toCharArray()) {
			a += (int) character;
		}
		return a;
	}

	//Server IP configurations
	private static String serverIpConfiguration(NiagaraServerConfiguration serverConfiguration) {
		StringBuilder builder = new StringBuilder();
		builder.append(serverConfiguration.getServerIP());
		builder.append(serverConfiguration.getServerPort());
		builder.append(serverConfiguration.getBackupServerIP());
		builder.append(serverConfiguration.getBackupServerPort());
		builder.append(serverConfiguration.getBackupServer2IP());
		builder.append(serverConfiguration.getBackupServer2Port());
		builder.append(serverConfiguration.getBackupServer3IP());
		builder.append(serverConfiguration.getBackupServer3Port());
		return builder.toString();
	}

	private static String trackerTimerConfiguration() {
		return null;
	}

	private static String deepSleepModeConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String startEmergencyTrackingPeriod() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String requestImediatePosition() {
		return "";
	}

	private static String wakeUpSleepMode() {
		return "";
	}

	private static String softSleepMode() {
		return "";
	}

	private static String resetOdometer() {
		return "";
	}

	private static String telemetry_transtransperentCommand() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String startFirmWareDownload() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String abortFirmwareDownload() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String newDataPackage() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String firmwareSwitch() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String queryFirWareVersion() {
		// TODO Auto-generated method stub
		return null;
	}
	

	private static String getEngineBlock(Boolean activate) {
		BitSet bitSet = new BitSet(8);
		bitSet.set(7, activate);
		byte[] bytes = new byte[1];
		bytes = ByteUtil.fromBitSet(bitSet);
		return Hex.encodeHexString(bytes);
	}

}
