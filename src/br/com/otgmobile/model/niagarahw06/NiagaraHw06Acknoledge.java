package br.com.otgmobile.model.niagarahw06;

import br.com.otgmobile.util.CRC16CCITT;

public class NiagaraHw06Acknoledge {

	private String productIdentification;
	private String firmWareVersion;
	private String productserialnumber;
	private Integer sequenceControlNumber;
	
	public String getProductIdentification() {
		return productIdentification;
	}
	public void setProductIdentification(String productIdentification) {
		this.productIdentification = productIdentification;
	}
	public String getFirmWareVersion() {
		return firmWareVersion;
	}
	public void setFirmWareVersion(String firmWareVersion) {
		this.firmWareVersion = firmWareVersion;
	}
	public String getProductserialnumber() {
		return productserialnumber;
	}
	public void setProductserialnumber(String productserialnumber) {
		this.productserialnumber = productserialnumber;
	}
	public Integer getSequenceControlNumber() {
		return sequenceControlNumber;
	}
	public void setSequenceControlNumber(Integer sequenceControlNumber) {
		this.sequenceControlNumber = sequenceControlNumber;
	}
	
	public String generateHexAcknolegde(){
		String hexAcknoledge = productIdentification +firmWareVersion+ productserialnumber + 
				"85" + calculateSequanceControlHex();
		
		String ack = hexAcknoledge + CRC16CCITT.getHexCRC(hexAcknoledge);
		return ack.toUpperCase();
	}
	
	
	private String calculateSequanceControlHex() {
		if(sequenceControlNumber == null){
			return "00";
		}else if (sequenceControlNumber <16) {
			return "0"+Integer.toHexString(getSequenceControlNumber());
		}else{
			return Integer.toHexString(getSequenceControlNumber());
		}
	}
	
}
