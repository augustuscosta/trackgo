package br.com.otgmobile.model.niagarahw06;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.BitSet;
import java.util.Date;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import br.com.otgmobile.util.ByteUtil;

public class NiagaraHw06Parser {

	private static final int SECONDS_IN_MILLS = 1000;
	
	// Vendo melhor abordagem entre bytes e e strings hexadecimais
	private String messageInString;
	
	public NiagaraHw06Parser(String messageInString){
		this.messageInString = messageInString;
	}
	
	// Header Parser
	public String parseProductIdentification() {
		String productIdentificationHex = substringInProtocol(messageInString, 0, 2);

		return productIdentificationHex;
	}
	
	public String parseFirmWare() {
		String firmwareVersionHex = substringInProtocol(messageInString, 2, 4);

		return firmwareVersionHex;
	}
	
	public String parseProductSerialNumber() {
		return substringInProtocol(messageInString, 4, 10);
	}
	
	public Integer parseMessagetype() {
		String messageTypeInHex = substringInProtocol(messageInString, 10, 12);
		Integer messageType = hexToInteger(messageTypeInHex);

		return messageType;
	}
	
	public Integer parseSequenceControll() {
		String sequenceControlInHex = substringInProtocol(messageInString, 12, 14);
		Integer sequencControl = hexToInteger(sequenceControlInHex);

		return sequencControl;
	}
	
	
	// Message Body Parser
	public Date parseMessageDate() {
		String secondsofDateInHex = substringInProtocol(messageInString, 14, 22);
		Date date = getDateFromHexSeconds(secondsofDateInHex);
		
		return date;
	}
	
	public Date parsegpsDataDate() {
		String secondsofDateInHex = substringInProtocol(messageInString, 22, 30);
		Date date = getDateFromHexSeconds(secondsofDateInHex);
		
		return date;
	}
	
	public Double parseLatitude() {
		String sub = substringInProtocol(messageInString, 30, 38);
		String latitudeInHex = sub;
		String latitude = hexToCoordinate(latitudeInHex);
		return Double.parseDouble(latitude);
	}
	
	public Double parseLongitude() {
		String sub = substringInProtocol(messageInString, 38, 46);
		String latitudeInHex = sub;
		String latitude = hexToCoordinate(latitudeInHex);
		return Double.parseDouble(latitude);
	}
	
	public Integer parseSpeed() {
		String speedInHex = substringInProtocol(messageInString, 46, 48);
		Integer speed = hexToInteger(speedInHex);
		return  speed;
	}
	
	public Integer parseDirection() {
		String directionInHex = substringInProtocol(messageInString, 48, 50);
		Integer direction = hexToInteger(directionInHex);
		if(direction != null){
			return direction/2;
		}
		return null;
	}
	
	
	public Double parseAltitude() {
		String sub = substringInProtocol(messageInString, 50, 54);
		Integer altitude = hexToInteger(sub);
		return (double)altitude;
	}
	
	public Integer parseGpsStatus(){
		String statusInHex = substringInProtocol(messageInString, 54, 56);
		Integer status = Integer.parseInt(statusInHex);
		return  status;
	}
	
	public Integer parseNumberofSatelites() {
		String sub = substringInProtocol(messageInString, 56, 58);
		Integer numberSatellites = hexToInteger(sub);
		return numberSatellites;
	}
	
	public NiagaraHw06SensorStatus parseSensorStatusBits() {
		String sub = substringInProtocol(messageInString, 58, 60);
		int ignitionBit;
		int panicBit;
		try {
			byte[] sensorbyte = Hex.decodeHex(sub.toCharArray());
			BitSet bitset = ByteUtil.fromByteArray(sensorbyte);
			ignitionBit = bitset.get(7) ? 1:0;
			panicBit = bitset.get(6) ? 1:0;
			return NiagaraHw06SensorStatus.getValue(ignitionBit, panicBit);
		} catch (DecoderException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public NiagaraHw06ActuatorStatus parseActuatorStatus() {
		String sub = substringInProtocol(messageInString, 60, 62);
		int engineBit;
		try {
			byte[] sensorbyte = Hex.decodeHex(sub.toCharArray());
			BitSet bitset = ByteUtil.fromByteArray(sensorbyte);
			engineBit = bitset.get(7) ? 1:0;
			return NiagaraHw06ActuatorStatus.getValue(engineBit);
		} catch (DecoderException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Integer parseWarningType() {
		String sub = substringInProtocol(messageInString, 70, 72);
		Integer warningType = Integer.parseInt(sub, 16);
		return warningType;
	}
	
	public Integer parseWarningState() {
		String sub = substringInProtocol(messageInString, 72, 74);
		Integer warningType = Integer.parseInt(sub, 16);
		return warningType;
	}
	
	public Double parseOdometer() {
		String sub = substringInProtocol(messageInString, 62, 70);
		Integer odometer = Integer.parseInt(sub, 16);
		return (double)odometer;
	}
	
	
	// Auxiliary Methods
	public Integer hexToInteger(String code) {
		if (code == null || code.equals("")) {
			return 0;
		}
		return Integer.parseInt(code, 16);
	}
	
	public String substringInProtocol(String protocol, Integer start, Integer end) {
		try {
			return protocol.substring(start, end);
		} catch (StringIndexOutOfBoundsException ex) {
			return "";
		}
	}

	public Long hexToLong(String timerInHex) {
		if (timerInHex == null || timerInHex.equals("")) {
			return 0L;
		}
		return Long.parseLong(timerInHex, 16);
	}
	
	private Date getDateFromHexSeconds(String secondsofDateInHex) {
		Long dateSeconds = hexToLong(secondsofDateInHex);
		Long timeInMills = dateSeconds * SECONDS_IN_MILLS;
		Date date = new Date(timeInMills);
		return date;
	}
	
	public String hexToCoordinate(String latitudeInHex) {
		if (latitudeInHex == null || latitudeInHex.equals(""))
			return "0.0";

		int val = Long.valueOf(latitudeInHex, 16).intValue();
		BigDecimal coord = new BigDecimal(val / 360000.0d);
		coord = coord.round(new MathContext(8, RoundingMode.HALF_EVEN));
		return coord.toString();
	}
	
	//Data Parser return
	
	public NiagaraHW06message getParsedMessage(){
		NiagaraHW06message message = new  NiagaraHW06message();
		message.setProductIdentification(parseProductIdentification());
		message.setFirmWareVersion(parseFirmWare());
		message.setProductserialnumber(parseProductSerialNumber());
		message.setMessageType(parseMessagetype());
		message.setSequenceControlNumber(parseSequenceControll());
		message.setMessageDate(parseMessageDate());
		message.setGpsDataDate(parsegpsDataDate());
		message.setLatitude(parseLatitude());
		message.setLongitude(parseLongitude());
		message.setSpeed(parseSpeed());
		message.setDirection(parseDirection());
		message.setAltitude(parseAltitude());
		message.setGpsStatus(parseGpsStatus());
		message.setNumberOfSatelites(parseNumberofSatelites());
		message.setOdometer(parseOdometer());
		message.setActuatorStatus(parseActuatorStatus());
		message.setSensorStatus(parseSensorStatusBits());
		
		if(NiagaraHw06MessageType.WARNING_MESSAGE == message.getMessageType()){
			message.setWarning(parseWarningType(), parseWarningState());
		}
		return message;
	}
	
	public NiagaraHw06Acknoledge generateAcknoledgeMessage() {
		NiagaraHw06Acknoledge ack = new NiagaraHw06Acknoledge();
		ack.setProductIdentification(parseProductIdentification());
		ack.setFirmWareVersion(parseFirmWare());
		ack.setProductserialnumber(parseProductSerialNumber());
		ack.setSequenceControlNumber(parseSequenceControll());
		return ack;
	}
	
	@Override
	public String toString() {
		return 
				"productIDentification="              		+parseProductIdentification()+	"\n"+
				"FirmWareVersion="							+parseFirmWare()+				"\n"+
				"SerialNumber="								+parseProductSerialNumber()+		"\n"+
				"MessageType="								+parseMessagetype()+				"\n"+
				"SequenceControl="							+parseSequenceControll()+		"\n"+
				"Message Date="								+parseMessageDate()+			    "\n"+
				"Gps Data Date="								+parsegpsDataDate()+        	 	"\n"+
				"Gps  Status="								+parseGpsStatus()+				"\n"+
				"Altitude="									+parseAltitude()+				"\n"+
				"Direction="									+parseDirection()+				"\n"+
				"Speed="										+parseSpeed()+					"\n"+
				"Latitude= "									+parseLatitude()+				"\n"+
				"Longitude="									+parseLongitude()+				"\n"+
				"Number of satelites="						+parseNumberofSatelites()+		"\n"+
				"Odometer Value ="							+parseOdometer();
	}                                                                                
	
}
