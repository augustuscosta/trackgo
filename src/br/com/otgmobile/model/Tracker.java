package br.com.otgmobile.model;

public interface Tracker {

	public abstract Message getMessage();
	
	public boolean isValid();
	
}
