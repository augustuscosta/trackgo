package br.com.otgmobile.model.niagara2;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.joda.time.DateTime;

public class ParseProtocol {

	private String protocolInString;

	public ParseProtocol(String protocolInString) {
		this.protocolInString = protocolInString;
	}

	private static final String REGEX = "([0-9A-Fa-f]{2})";
	private static final Integer DEFAULT_YEAR = 1990;

	public String getCodeModule() {
		return null;
	}

	public Integer getOdometer() {
		String sub = substringInProtocol(protocolInString, 70, 78);
		Integer odometer = hexToInteger(sub);
		return odometer;
	}


	public Integer getHdop() {
		String sub = substringInProtocol(protocolInString, 64, 66);
		Integer hdop = hexToInteger(sub);
		return hdop;
	}

	public Integer getIdSatellitesView() {
		String sub = substringInProtocol(protocolInString, 56, 64);
		Integer idSetellites = Integer.parseInt(sub);
		return idSetellites;
	}

	public Integer getNumberOfSatellites() {
		String sub = substringInProtocol(protocolInString, 54, 56);
		Integer numberSatellites = hexToInteger(sub);
		return numberSatellites;
	}

	public Integer getGpsStatus() {
		String sub = substringInProtocol(protocolInString, 52, 54);
		Integer status = hexToInteger(sub);
		return status;
	}

	public Integer getAltitude() {
		String sub = substringInProtocol(protocolInString, 48, 52);
		String altitudeInHex = reverseHex(sub);
		Integer altitude = Integer.parseInt(altitudeInHex);
		return altitude;
	}

	public Integer getDirection() {
		String sub = substringInProtocol(protocolInString, 46, 48);
		Integer direction = hexToInteger(sub);
		return direction;
	}

	public Integer getSpeed() {
		String sub = substringInProtocol(protocolInString, 44, 46);
		Integer speed = hexToInteger(sub);
		return speed;
	}

	public Double getLongitude() {
		String sub = substringInProtocol(protocolInString, 36, 44);
		String longitudeInHex = reverseHex(sub);
		String longitude = hexToCoordinate(longitudeInHex);
		return Double.parseDouble(longitude);
	}

	public Double getLatitude() {
		String sub = substringInProtocol(protocolInString, 28, 36);
		String latitudeInHex = reverseHex(sub);
		String latitude = hexToCoordinate(latitudeInHex);
		return Double.parseDouble(latitude);
	}

	public String hexToCoordinate(String latitudeInHex) {
		if (latitudeInHex == null || latitudeInHex.equals(""))
			return "0.0";

		int val = Long.valueOf(latitudeInHex, 16).intValue();
		BigDecimal coord = new BigDecimal(val / 3600000.0d);
		coord = coord.round(new MathContext(8, RoundingMode.HALF_EVEN));
		return coord.toString();
	}

	public Date getGenerationTimestamp() {
		String sub = substringInProtocol(protocolInString, 20, 28);
		String timerInHex = reverseHex(sub);
		Long timerInLong = hexToLong(timerInHex);
		String binaryTimer = Long.toBinaryString(timerInLong);
		Date timer = getTimerFromBinary("0" + binaryTimer);

		return timer;
	}

	public Long hexToLong(String timerInHex) {
		if (timerInHex == null || timerInHex.equals("")) {
			return 0L;
		}
		return Long.parseLong(timerInHex, 16);
	}

	public Date getTimerFromBinary(String timerInBinary) {
		try {
			Integer year = DEFAULT_YEAR + binaryToInt(timerInBinary, 0, 6);
			Integer monthOfYear = binaryToInt(timerInBinary, 6, 10);
			Integer dayOfMonth = binaryToInt(timerInBinary, 10, 15);
			Integer hourOfDay = binaryToInt(timerInBinary, 15, 20);
			Integer minuteOfHour = binaryToInt(timerInBinary, 20, 26);
			Integer secondOfMinute = binaryToInt(timerInBinary, 26, 32);
			Calendar cal = Calendar.getInstance();
			cal.set(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute);
			
			return cal.getTime();
		} catch (Exception e) {
			return new Date();
		}
	}

	public Integer binaryToInt(String binaryTimer, int start, int end) {
		if (end > binaryTimer.length()) {
			return 0;
		}
		return Integer.parseInt(binaryTimer.substring(start, end), 2);
	}

	public String reverseHex(String hexadecimal) {
		StringBuilder reverse = new StringBuilder();
		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(hexadecimal);
		while (matcher.find()) {
			reverse.insert(0, matcher.group());
		}

		return reverse.toString();
	}

	public String getNumberTrakingPoints() {
		return null;
	}

	public Integer getRetryNumber() {
		String number = substringInProtocol(protocolInString, 16, 18);
		Integer numberRetry = hexToInteger(number);

		return numberRetry;
	}

	public String getEventCode() {
		return substringInProtocol(protocolInString, 12, 14);
	}

	public String getUniqueUnitIdentifier() {
		return substringInProtocol(protocolInString, 2, 12);
	}

	public String getFirmWareVersion() {
		String code = substringInProtocol(protocolInString, 0, 2);

		return code;
	}

	public String substringInProtocol(String protocol, Integer start, Integer end) {
		try {
			return protocol.substring(start, end);
		} catch (StringIndexOutOfBoundsException ex) {
			return "";
		}
	}

	public Integer hexToInteger(String code) {
		if (code == null || code.equals("")) {
			return 0;
		}
		return Integer.parseInt(code, 16);
	}

	public Integer getSequenceControlNumber() {
		String sequence = substringInProtocol(protocolInString, 14, 16);
		Integer sequenceInt = hexToInteger(sequence);
		return sequenceInt;
	}

	public Niagara2Message getProtocolNiagara2Message() {
		Niagara2Message protocol = new Niagara2Message();
		protocol.setFirmWareVersion(getFirmWareVersion());
		protocol.setUniqueUnitIdentifier(getUniqueUnitIdentifier());
		protocol.setEventCode(getEventCode());
		protocol.setSequenceControl(getSequenceControlNumber());
		protocol.setRetryNumber(getRetryNumber());
		protocol.setNumberTrakingPoints(getNumberTrakingPoints());
		protocol.setGenerationTimeStamp(getGenerationTimestamp());
		protocol.setLatitude(getLatitude());
		protocol.setLongitude(getLongitude());
		protocol.setSpeed(getSpeed());
		protocol.setDirection(getDirection());
		protocol.setAltitude(getAltitude());
		protocol.setGpsStatus(getGpsStatus());
		protocol.setNumberOfSatellites(getNumberOfSatellites());
		protocol.setIdSatellitesView(getIdSatellitesView());
		protocol.setHdop(getHdop());
		protocol.setOdometer(getOdometer());

		return protocol;
	}
	
	@Override
	public String toString() {
		return 
				                                                            
"getCodeModule="              				+getCodeModule()+              "\n" +
"getOdometer=                                "+getOdometer()+                  "\n" +
"getHdop=                                    "+getHdop()+                      "\n" +
"getIdSatellitesView=                        "+getIdSatellitesView()+          "\n" +
"getNumberOfSatellites=                      "+getNumberOfSatellites()+        "\n" +
"getGpsStatus=                               "+getGpsStatus()+                 "\n" +
"getAltitude=                                "+getAltitude()+                  "\n" +
"getDirection=                               "+getDirection()+                 "\n" +
"getSpeed=                                   "+getSpeed()+                     "\n" +
"getLongitude=                               "+getLongitude()+                 "\n" +
"getLatitude=                                "+getLatitude()+                  "\n" +
"getGenerationTimestamp=                     "+getGenerationTimestamp()+       "\n" +
"getNumberTrakingPoints=                     "+getNumberTrakingPoints()+       "\n" +
"getRetryNumber=                      		"+getRetryNumber()+               "\n" +
"getEventCode=                               "+getEventCode()+                 "\n" +
"getUniqueUnitIdentifier=                    "+getUniqueUnitIdentifier()+      "\n" +
"getFirmWareVersion=                        	"+getFirmWareVersion()+        	  "\n" +
"getSequenceControlNumber=                   "+getSequenceControlNumber();
	}

	public byte[] getAckNowlegment() {
		Acknowledge ack = new Acknowledge();
		ack.setVersionDescription(getFirmWareVersion());
		ack.setSequenceControlNumber(getSequenceControlNumber());
		ack.setUniqueUnitIdentifyer(getUniqueUnitIdentifier());
		ack.setRetryNumber(getRetryNumber());
		return ack.returnAcknowledge();
	}                                                                                
	

}
