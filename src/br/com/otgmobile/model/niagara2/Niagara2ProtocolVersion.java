package br.com.otgmobile.model.niagara2;

public enum Niagara2ProtocolVersion {
	
	UNTIL_PROTOCL_119_BR(0,0),
	PROTOCOL_120_AND_ABOVE(0,1);
	
	private int firstBit; 
	private int secondBit;
	
	private Niagara2ProtocolVersion(int firstBit, int secondBit){
		this.firstBit = firstBit;
		this.secondBit = secondBit;
	}
	
	public static Niagara2ProtocolVersion getValue(int firstBit, int secondBit){
		if(firstBit == 0 && secondBit == 0)
			return Niagara2ProtocolVersion.UNTIL_PROTOCL_119_BR;
		
		if(firstBit == 0 && secondBit == 1)
			return Niagara2ProtocolVersion.PROTOCOL_120_AND_ABOVE;
		
		return null;
	}

	public int getFirstBit() {
		return firstBit;
	}

	public int getSecondBit() {
		return secondBit;
	}
}
