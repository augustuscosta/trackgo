package br.com.otgmobile.model.niagara2;

import br.com.otgmobile.util.StringUtil;

public class Acknowledge {
	
	private String versionDescription;
	private String uniqueUnitIdentifyer;
	private EventCode eventCode;
	private Integer sequenceControlNumber;
	private Integer retryNumber;
	
	public Acknowledge(){
		this.eventCode = EventCode.fromValue(135);
	}
	
	public String getVersionDescription() {
		return versionDescription;
	}
	
	public void setVersionDescription(String versionDescription) {
		this.versionDescription = versionDescription;
	}
	
	public String getUniqueUnitIdentifyer() {
		return uniqueUnitIdentifyer;
	}
	
	public void setUniqueUnitIdentifyer(String uniqueUnitIdentifyer) {
		this.uniqueUnitIdentifyer = uniqueUnitIdentifyer;
	}
	
	public EventCode getMessageType() {
		return eventCode;
	}

	public Integer getSequenceControlNumber() {
		return sequenceControlNumber;
	}
	
	public void setSequenceControlNumber(Integer sequenceControlNumber) {
		this.sequenceControlNumber = sequenceControlNumber;
	}
	
	public Integer getRetryNumber() {
		return retryNumber;
	}
	
	public void setRetryNumber(Integer retryNumber) {
		this.retryNumber = retryNumber;
	}
	
	public byte[] returnAcknowledge(){
		String niagara2Ack = versionDescription + uniqueUnitIdentifyer + 85+
				StringUtil.parseIntegerToHex(sequenceControlNumber)
				+ StringUtil.parseIntegerToHex(retryNumber);
		
		return niagara2Ack.getBytes();
	}
}
