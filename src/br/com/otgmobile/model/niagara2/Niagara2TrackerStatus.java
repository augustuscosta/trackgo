package br.com.otgmobile.model.niagara2;

public enum Niagara2TrackerStatus {
	
	IGNITION_ON_WITHOUT_PANIC(1,0),
	IGNITION_ON_WITH_PANIC(1,1),
	IGNITION_OFF_WITHOUT_PANIC(0,0),
	IGNITION_OFF_WITH_PANIC(0,1);
	
	private int panicBit;
	private int ignitionBit;
	
	Niagara2TrackerStatus(int igninitonBit , int panicBit) {
		this.ignitionBit = igninitonBit;
		this.panicBit = panicBit;
	}
	
	public static Niagara2TrackerStatus fromValue(int ignitionBit, int panicBit){
		if(ignitionBit == 1 && panicBit == 0){
			return IGNITION_ON_WITHOUT_PANIC;
		}
		
		if(ignitionBit == 1 && panicBit == 1){
			return IGNITION_ON_WITH_PANIC;
		}
		
		if(ignitionBit == 0 && panicBit == 0){
			return IGNITION_OFF_WITHOUT_PANIC;
		}
		
		if(ignitionBit == 0 && panicBit == 1){
			return IGNITION_OFF_WITH_PANIC;
		}
		
		return null;
	}

	public int getPanicBit() {
		return panicBit;
	}

	public void setPanicBit(int panicBit) {
		this.panicBit = panicBit;
	}

	public int getIgnitionBit() {
		return ignitionBit;
	}

	public void setIgnitionBit(int ignitionBit) {
		this.ignitionBit = ignitionBit;
	}

}
