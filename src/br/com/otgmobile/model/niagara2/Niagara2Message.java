package br.com.otgmobile.model.niagara2;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.events.EventMessage;
import br.com.otgmobile.model.Message;

@Entity
@Table(name="niagara2")
public class Niagara2Message implements Message, EventMessage{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="nigra2_sequence", sequenceName="nigra2_sequence")
	private Long id;
	private Long deviceId;
	private String firmWareVersion;
	private String uniqueUnitIdentifier;
	private String eventCode;
	private Integer sequenceControl;
	private Integer retryNumber;
	private Niagara2ProtocolVersion protocolVersion;
	private Date sentDate;
	private Date generationTimeStamp;
	private Double latitude;
	private Double longitude;
	private Integer altitude;
	private Integer speed;
	private Integer direction;
	private Long sateliteCount;
	private String numberTrakingPoints;
	private String tBoxStatus1;
	private String tBoxStatus2;
	private Integer odometerValue;
	private Long analogInputVoltage;
	private Long baterySuplyVoltage;
	private Long tBoxOnTime;
	private Double gsmSingalQuality;
	private Double modemTemperature;
	private Integer idSatellitesView;
	private Integer hdop;
	private Integer numberOfSatellites;
	private Integer gpsStatus;
	private Integer odometer;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getDeviceId() {
		return deviceId;
	}
	
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	
	public String getEventCode() {
		return eventCode;
	}
	
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	
	public Integer getSequenceControl() {
		return sequenceControl;
	}
	
	public void setSequenceControl(Integer sequenceControl) {
		this.sequenceControl = sequenceControl;
	}
	
	public Integer getRetryNumber() {
		return retryNumber;
	}
	
	public void setRetryNumber(Integer retryNumber) {
		this.retryNumber = retryNumber;
	}
	
	public Niagara2ProtocolVersion getProtocolVersion() {
		return protocolVersion;
	}
	
	public void setProtocolVersion(Niagara2ProtocolVersion protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	
	public Date getSentDate() {
		return sentDate;
	}
	
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	public Date getGenerationTimeStamp() {
		return generationTimeStamp;
	}
	
	public void setGenerationTimeStamp(Date generationDate) {
		this.generationTimeStamp = generationDate;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Integer getAltitude() {
		return altitude;
	}
	
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	
	
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	
	public Integer getSpeed1() {
		return speed;
	}
	public Integer getDirection() {
		return direction;
	}
	
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	
	public Long getSateliteCount() {
		return sateliteCount;
	}
	
	public void setSateliteCount(Long sateliteCount) {
		this.sateliteCount = sateliteCount;
	}
	
	public String gettBoxStatus1() {
		return tBoxStatus1;
	}
	
	public void settBoxStatus1(String tBoxStatus1) {
		this.tBoxStatus1 = tBoxStatus1;
	}
	
	public String gettBoxStatus2() {
		return tBoxStatus2;
	}
	
	public void settBoxStatus2(String tBoxStatus2) {
		this.tBoxStatus2 = tBoxStatus2;
	}
	
	public Integer getOdometerValue() {
		return odometerValue;
	}
	
	public void setOdometerValue(Integer odometerValue) {
		this.odometerValue = odometerValue;
	}
	
	public Long getAnalogInputVoltage() {
		return analogInputVoltage;
	}
	
	public void setAnalogInputVoltage(Long analogInputVoltage) {
		this.analogInputVoltage = analogInputVoltage;
	}
	
	public Long getBaterySuplyVoltage() {
		return baterySuplyVoltage;
	}
	
	public void setBaterySuplyVoltage(Long baterySuplyVoltage) {
		this.baterySuplyVoltage = baterySuplyVoltage;
	}
	
	public Long gettBoxOnTime() {
		return tBoxOnTime;
	}
	
	public void settBoxOnTime(Long tBoxOnTime) {
		this.tBoxOnTime = tBoxOnTime;
	}
	
	public Double getGsmSingalQuality() {
		return gsmSingalQuality;
	}
	
	public void setGsmSingalQuality(Double gsmSingalQuality) {
		this.gsmSingalQuality = gsmSingalQuality;
	}
	
	public Double getModemTemperature() {
		return modemTemperature;
	}
	
	public void setModemTemperature(Double modemTemperature) {
		this.modemTemperature = modemTemperature;
	}
	
	public Integer getIdSatellitesView() {
		return idSatellitesView;
	}
	
	public void setIdSatellitesView(Integer idSatellitesView) {
		this.idSatellitesView = idSatellitesView;
	}
	
	public Integer getHdop() {
		return hdop;
	}
	
	public void setHdop(Integer hdop) {
		this.hdop = hdop;
	}
	
	public Integer getNumberOfSatellites() {
		return numberOfSatellites;
	}
	
	public void setNumberOfSatellites(Integer numberOfSatellites) {
		this.numberOfSatellites = numberOfSatellites;
	}
	
	public Integer getGpsStatus() {
		return gpsStatus;
	}
	
	public void setGpsStatus(Integer gpsStatus) {
		this.gpsStatus = gpsStatus;
	}
	
	public String getFirmWareVersion() {
		return firmWareVersion;
	}
	
	public void setFirmWareVersion(String firmWareVersion) {
		this.firmWareVersion = firmWareVersion;
	}
	
	public String getUniqueUnitIdentifier() {
		return uniqueUnitIdentifier;
	}
	
	public void setUniqueUnitIdentifier(String uniqueUnitIdentifier) {
		this.uniqueUnitIdentifier = uniqueUnitIdentifier;
	}
	
	public String getNumberTrakingPoints() {
		return numberTrakingPoints;
	}
	
	public void setNumberTrakingPoints(String numberTrakingPoints) {
		this.numberTrakingPoints = numberTrakingPoints;
	}

	public Integer getOdometer() {
		return odometer;
	}

	public void setOdometer(Integer odometer) {
		this.odometer = odometer;
	}

	@Override
	public Double getCourse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getMoving() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getIgnition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getPanic() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRpm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getOpenedDoor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getEngineBlocked() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public Double getSpeed() {
		return (double) speed;
	}
	
	@Override
	public long getDateInMills() {
		if(getDate() == null){
			return 0;
		}
		return getDate().getTime();
	}
	
}
