package br.com.otgmobile.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.model.events.EventCondition;


@Entity
@Table(name="rota")
public class Rota {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="rota_sequence", sequenceName="rota_sequence")
	private Integer id;
	
	private String name;
	
	@OneToMany(mappedBy="rota", cascade=CascadeType.ALL,targetEntity=GeoPontoRota.class)   
	private List<GeoPontoRota> geoPonto;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "eventcondition_rota", joinColumns = @JoinColumn(name = "rota_id"), inverseJoinColumns = @JoinColumn(name = "eventcondition_id"))
	private List<EventCondition> eventConditions;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GeoPontoRota> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPontoRota> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public List<EventCondition> getEventConditions() {
		return eventConditions;
	}

	public void setEventConditions(List<EventCondition> eventConditions) {
		this.eventConditions = eventConditions;
	}

}
