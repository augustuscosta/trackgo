package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_gps_flag_state")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class GpsFlagState {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_gps_flag_state_sequence", sequenceName="maxtrack_gps_flag_state_sequence")
	private Long id;
	
	@XmlElement(name = "VALID_GPS")
	private Integer validGps;
	@XmlElement(name = "GPS_ANTENA_FAILURE")
	private Integer gpsAntenaFailure;
	@XmlElement(name = "USING_DR")
	private Integer usingDr; // Usando DR para calculo de posições
	@XmlElement(name = "EXCESS_SPEED")
	private Integer excessSpeed;
	@XmlElement(name = "EXCESS_STOPPED_TIME")
	private Integer excessStoppedTime;
	@XmlElement(name = "VOICE_CALL")
	private Integer voiceCall;
	@XmlElement(name = "FORCE_GPS")
	private Integer forceGps;
	@XmlElement(name = "GPRS_CONNECTION")
	private Integer gprsConnection;
	@XmlElement(name = "GPS_SIGNAL")
	private Integer gpsSignal; //0 = memoria 1 = atual
	@XmlElement(name = "GPS_ANTENNA_DISCONNECTED")
	private Integer gpsAntennaDisconnected;
	@XmlElement(name = "GSM_JAMMING")
	private Integer gsmJamming; // 0 - transmissão realizada no estado normal
	@XmlElement(name = "GPS_SLEEP")
	private Integer gpsSleep; //receptor gps em modo sleep no momento da tranmissao
	@XmlElement(name = "MOVING")
	private Boolean moving; // 0 = parado, 1 = movimento
	@XmlElement(name = "CELL_PRESENT")
	private Boolean cellPresent; // presença do MXT dewntro de uma área de uma ERB (célula GSM) 0 = fora, 1 = dentro
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getValidGps() {
		return validGps;
	}
	public void setValidGps(Integer validGps) {
		this.validGps = validGps;
	}
	public Integer getGpsAntenaFailure() {
		return gpsAntenaFailure;
	}
	public void setGpsAntenaFailure(Integer gpsAntenaFailure) {
		this.gpsAntenaFailure = gpsAntenaFailure;
	}
	public Integer getUsingDr() {
		return usingDr;
	}
	public void setUsingDr(Integer usingDr) {
		this.usingDr = usingDr;
	}
	public Integer getExcessSpeed() {
		return excessSpeed;
	}
	public void setExcessSpeed(Integer excessSpeed) {
		this.excessSpeed = excessSpeed;
	}
	public Integer getExcessStoppedTime() {
		return excessStoppedTime;
	}
	public void setExcessStoppedTime(Integer excessStoppedTime) {
		this.excessStoppedTime = excessStoppedTime;
	}
	public Integer getVoiceCall() {
		return voiceCall;
	}
	public void setVoiceCall(Integer voiceCall) {
		this.voiceCall = voiceCall;
	}
	public Integer getForceGps() {
		return forceGps;
	}
	public void setForceGps(Integer forceGps) {
		this.forceGps = forceGps;
	}
	public Integer getGprsConnection() {
		return gprsConnection;
	}
	public void setGprsConnection(Integer gprsConnection) {
		this.gprsConnection = gprsConnection;
	}
	public Integer getGpsSignal() {
		return gpsSignal;
	}
	public void setGpsSignal(Integer gpsSignal) {
		this.gpsSignal = gpsSignal;
	}
	public Integer getGpsAntennaDisconnected() {
		return gpsAntennaDisconnected;
	}
	public void setGpsAntennaDisconnected(Integer gpsAntennaDisconnected) {
		this.gpsAntennaDisconnected = gpsAntennaDisconnected;
	}
	public Integer getGsmJamming() {
		return gsmJamming;
	}
	public void setGsmJamming(Integer gsmJamming) {
		this.gsmJamming = gsmJamming;
	}
	public Integer getGpsSleep() {
		return gpsSleep;
	}
	public void setGpsSleep(Integer gpsSleep) {
		this.gpsSleep = gpsSleep;
	}
	public Boolean getMoving() {
		return moving;
	}
	public void setMoving(Boolean moving) {
		this.moving = moving;
	}
	public Boolean getCellPresent() {
		return cellPresent;
	}
	public void setCellPresent(Boolean cellPresent) {
		this.cellPresent = cellPresent;
	}
	
	
}
