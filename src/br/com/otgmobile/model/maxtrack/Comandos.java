package br.com.otgmobile.model.maxtrack;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="COMMANDS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Comandos {
	
	@XmlElement(name="COMMAND")
	private List<Comando> comandos;

	public List<Comando> getComandos() {
		return comandos;
	}

	public void setComandos(List<Comando> comandos) {
		this.comandos = comandos;	
	}
	
}
