package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(Integer.class)
public enum AntiTheftStatus {
	
	@XmlEnumValue("0") NORMAL,
	@XmlEnumValue("1") ARMADO,
	@XmlEnumValue("2") SUSPENSO,
	@XmlEnumValue("3") DISPARADO

}
