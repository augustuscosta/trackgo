package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(Integer.class)
public enum MotivoTransmissao {

	@XmlEnumValue("1") MODULO_ENERGIZADO(1),
	@XmlEnumValue("2") RECONEXAO_GPRS(2),
	@XmlEnumValue("3") INTERVALO_DE_TRANSMISSAO_PARADO(3),
	@XmlEnumValue("4") INTERVALO_DE_TRANSMISSAO_EM_MOVIMENTO(4),
	@XmlEnumValue("5") INTERVALO_DE_TRANSMISSAO_EM_PANICO(5),
	@XmlEnumValue("6") CONFIGURACAO_DE_ENTRADA(6),
	@XmlEnumValue("7") REQUISICAO_DO_SERVIDOR(7),
	@XmlEnumValue("8") POSICAO_VALIDA_APOS_INTERVALO_DE_TRANSMISSAO(8),
	@XmlEnumValue("9") IGNICAO_LIGADA(9),
	@XmlEnumValue("10") IGNICAO_DESLIGADA(10),
	@XmlEnumValue("11") PANICO_ATIVADO(11),
	@XmlEnumValue("12") PANICO_DESATIVADO(12),
	@XmlEnumValue("13") ENTRADA1_ATIVADA(13),
	@XmlEnumValue("14") ENTRADA1_ABERTA(14),
	@XmlEnumValue("15") ENTRADA2_ATIVADA(15),
	@XmlEnumValue("16") ENTRADA2_ABERTA(16),
	@XmlEnumValue("17") ENTRADA3_ATIVADA(17),
	@XmlEnumValue("18") ENTRADA3_ABERTA(18),
	@XmlEnumValue("19") ENTRADA4_ATIVADA(19),
	@XmlEnumValue("20") ENTRADA4_ABERTA(20),
	@XmlEnumValue("21") GSENSOR_MOVIMENTO(21),
	@XmlEnumValue("22") GSENSOR_PARADO(22),
	@XmlEnumValue("23") ANTIFURTO_ALARMADO(23),
	@XmlEnumValue("24") FALHA_DE_ACESSORIO(24),
	@XmlEnumValue("25") FALHA_DE_ENERGIA_EXTERNA(25),
	@XmlEnumValue("26") ENERGIA_EXTERNA_OK(26),
	@XmlEnumValue("27") FALHA_ANTENA_GPS(27),
	@XmlEnumValue("28") ANTENA_GPS_OK(28),
	@XmlEnumValue("29") PACOTE_RECEBIDO_DE_UM_ACESSORIO_WIRELESS(29),
	@XmlEnumValue("30") ENTROU_EM_MODO_SLEEP(30),
	@XmlEnumValue("31") SAIDA1_ATIVADA(31),
	@XmlEnumValue("32") SAIDA1_DESATIVADA(32),
	@XmlEnumValue("33") SAIDA2_ATIVADA(33),
	@XmlEnumValue("34") SAIDA2_DESATIVADA(34),
	@XmlEnumValue("35") SAIDA3_ATIVADA(35),
	@XmlEnumValue("36") SAIDA3_DESATIVADA(36),
	@XmlEnumValue("37") VELOCIDADE_MAXIMA_EXCEDIDA(37),
	@XmlEnumValue("38") VELOCIDADE_NORMALIZADA(38),
	@XmlEnumValue("39") ENTRADA_EM_PONTO_DE_REFERENCIA(39),
	@XmlEnumValue("40") SAIDA_DE_PONTO_DE_REFERENCIA(40),
	@XmlEnumValue("41") FALHA_NA_BATERIA_DE_BACKUP(41),
	@XmlEnumValue("42") BATERIA_E_BACKUP_OK(42),
	@XmlEnumValue("43") POSICAO_REENVIADA_POR_FALHA_DE_ENVIO_NA_PRIMEIRA_TENTATIVA(43),
	@XmlEnumValue("44") POSICAO_REQUISITADA_POR_SMS(44),
	@XmlEnumValue("45") VIOLACAO_DO_MODULO(45),
	@XmlEnumValue("46") LIMITE_DO_SENSOR_FRENTE_TRAS_ATINGIDO(46),
	@XmlEnumValue("47") LIMITE_DO_SENSOR_LATERAL_ATINGIDO(47),
	@XmlEnumValue("48") LIMITE_DO_SENSOR_VERTICAL_ATINGIDO(48),
	@XmlEnumValue("49") ALTERACAO_NA_DIRECAO_RECEBIDA_PELO_GPS(49),
	@XmlEnumValue("50") TRASMISSAO_REALIZADA_NO_MESMO_MOMENTO_DO_ENVIO_DE_UMA_POSICAO_POR_SMS(50),
	@XmlEnumValue("51") MXT_DESLIGADO(51),
	@XmlEnumValue("52") ANTIFURTO_PASSA_PARA_O_ESTADO_NORMAL(52),
	@XmlEnumValue("53") DETECCAO_DE_JAMMING(53),
	@XmlEnumValue("54") MODULO_DETECTA_QUE_NAO_ESTA_MAIS_EM_SITUACAO_DE_JAMMING(54),
	@XmlEnumValue("55") ALTO_RPM_EM_MOVIMENTO(55),
	@XmlEnumValue("56") ALTO_RPM_EM_NEUTRO(56),
	@XmlEnumValue("57") DETECCAO_DE_VELOCIDADE_EM_PONTO_NEUTRO(57),
	@XmlEnumValue("58") FALHA_DE_GPS(58),
	@XmlEnumValue("59") TRASMISSAO_POR_DISTANCIA(59),
	@XmlEnumValue("60") FALHA_DE_ALIMENTACAO_E_GPS(60),
	@XmlEnumValue("61") AGPS_REQUERIDO(61),
	@XmlEnumValue("62") STATUS_DA_TAG_MUDOU(62),
	@XmlEnumValue("63") STATUS_DA_BATERIA_TAG_MUDOU(63),
	@XmlEnumValue("64") LINK_BROKEN(64),
	@XmlEnumValue("65") ENTRADA_EXPAND_MUDOU(65),
	@XmlEnumValue("66") TAG_ACESSORIES_STATUS_CHANGED_FROM_0_TO_1(66);
	
	private int value;
	
	MotivoTransmissao(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
}
