package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum ProtocoloComando {
	
	@XmlEnumValue("42") MTC400_ID_MENOR_QUE_65535(42),
	@XmlEnumValue("44") MTC400_ID_MAIOR_QUE_65535(44),
	@XmlEnumValue("45") MTC500(45),
	@XmlEnumValue("4") MTC600(4),
	@XmlEnumValue("160") MXT100(160),
	@XmlEnumValue("161") MXT101(161),
	@XmlEnumValue("166") MXT140(166),
	@XmlEnumValue("162") MXT150(162),
	@XmlEnumValue("163") MXT151(163),
	@XmlEnumValue("164") MXT120(164);

	private int value;
	
	ProtocoloComando(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
}
