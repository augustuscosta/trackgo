package br.com.otgmobile.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_hardware_monitor")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class HardwareMonitor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_hardware_monitor", sequenceName="maxtrack_hardware_monitor")
	private Long id;

	@XmlElement(name = "POWER_SUPPLY")
	private Integer powerSupply; // Tensão da alimentação principal
	@XmlElement(name = "TEMPERATURE")
	private Integer temperature; // Temperatura do módulo
	@XmlElement(name = "HOUR_METER")
	private Integer hourmeter; // Horímetro do módulo
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "INPUTS")
	private Inputs inputs;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "OUTPUTS")
	private Outputs outputs;
	@XmlElement(name = "CHARGE")
	private EstadoCarga charge;
	@XmlElement(name = "SHORT_CIRCUIT")
	private Boolean shortCircuit; // falha de alimentação
	@XmlElement(name = "TIME")
	private Long time; // tempo desde o último boot hardware section
	@XmlElement(name = "COUNTER1")
	private Integer counter1;
	@XmlElement(name = "COUNTER2")
	private Integer counter2;
	@XmlElement(name = "COUNTER3")
	private Integer counter3;
	@XmlElement(name = "AD1")
	private Integer ad1;
	@XmlElement(name = "COUNTER2")
	private Integer ad2;
	@XmlElement(name = "COUNTER3")
	private Integer ad3;
	@XmlElement(name = "COUNTER4")
	private Integer ad4;
	@XmlElement(name = "SECURE_INPUT_STATE")
	private Integer secureInputState;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "SECURE_INPUT_MODE")
	private SecureInputMode secureInputMode;
	@XmlElement(name = "SECURE_OUTPUT_STATE")
	private Integer secureOutputState;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "SECURE_OUTPUT_MODE")
	private SecureOutputMode secureOutputMode;
	@XmlElement(name = "ACESSORY_COUNT")
	private Integer acessoryCount;
	@XmlElement(name = "RPM")
	private Integer rpm;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "RANGE_STATE")
	private RangeState rangeState;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "FLAG_STATE")
	private HardwareMonitorFlagState flagState;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPowerSupply() {
		return powerSupply;
	}

	public void setPowerSupply(Integer powerSupply) {
		this.powerSupply = powerSupply;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getHourmeter() {
		return hourmeter;
	}

	public void setHourmeter(Integer hourmeter) {
		this.hourmeter = hourmeter;
	}

	public Inputs getInputs() {
		return inputs;
	}

	public void setInputs(Inputs inputs) {
		this.inputs = inputs;
	}

	public Outputs getOutputs() {
		return outputs;
	}

	public void setOutputs(Outputs outputs) {
		this.outputs = outputs;
	}

	public EstadoCarga getCharge() {
		return charge;
	}

	public void setCharge(EstadoCarga charge) {
		this.charge = charge;
	}

	public Boolean getShortCircuit() {
		return shortCircuit;
	}

	public void setShortCircuit(Boolean shortCircuit) {
		this.shortCircuit = shortCircuit;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getCounter1() {
		return counter1;
	}

	public void setCounter1(Integer counter1) {
		this.counter1 = counter1;
	}

	public Integer getCounter2() {
		return counter2;
	}

	public void setCounter2(Integer counter2) {
		this.counter2 = counter2;
	}

	public Integer getCounter3() {
		return counter3;
	}

	public void setCounter3(Integer counter3) {
		this.counter3 = counter3;
	}

	public Integer getAd1() {
		return ad1;
	}

	public void setAd1(Integer ad1) {
		this.ad1 = ad1;
	}

	public Integer getAd2() {
		return ad2;
	}

	public void setAd2(Integer ad2) {
		this.ad2 = ad2;
	}

	public Integer getAd3() {
		return ad3;
	}

	public void setAd3(Integer ad3) {
		this.ad3 = ad3;
	}

	public Integer getAd4() {
		return ad4;
	}

	public void setAd4(Integer ad4) {
		this.ad4 = ad4;
	}

	public Integer getSecureInputState() {
		return secureInputState;
	}

	public void setSecureInputState(Integer secureInputState) {
		this.secureInputState = secureInputState;
	}

	public SecureInputMode getSecureInputMode() {
		return secureInputMode;
	}

	public void setSecureInputMode(SecureInputMode secureInputMode) {
		this.secureInputMode = secureInputMode;
	}

	public Integer getSecureOutputState() {
		return secureOutputState;
	}

	public void setSecureOutputState(Integer secureOutputState) {
		this.secureOutputState = secureOutputState;
	}

	public SecureOutputMode getSecureOutputMode() {
		return secureOutputMode;
	}

	public void setSecureOutputMode(SecureOutputMode secureOutputMode) {
		this.secureOutputMode = secureOutputMode;
	}

	public Integer getAcessoryCount() {
		return acessoryCount;
	}

	public void setAcessoryCount(Integer acessoryCount) {
		this.acessoryCount = acessoryCount;
	}

	public Integer getRpm() {
		return rpm;
	}

	public void setRpm(Integer rpm) {
		this.rpm = rpm;
	}

	public RangeState getRangeState() {
		return rangeState;
	}

	public void setRangeState(RangeState rangeState) {
		this.rangeState = rangeState;
	}

	public HardwareMonitorFlagState getFlagState() {
		return flagState;
	}

	public void setFlagState(HardwareMonitorFlagState flagState) {
		this.flagState = flagState;
	}

}
