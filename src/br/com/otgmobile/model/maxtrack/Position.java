package br.com.otgmobile.model.maxtrack;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.Index;

import br.com.otgmobile.events.EventMessage;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.Tracker;

@Entity
@Table(name="maxtrack_position")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POSITION")
public class Position implements Tracker, EventMessage{


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_position_sequence", sequenceName="maxtrack_position_sequence")
	private Long id;
	@Index(name="position_serial_index")
	@XmlElement(name = "SERIAL")
	private String serial;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "FIRMWARE")
	private Firmware firmware;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "GPS")
	private Gps gps;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "HARDWARE_MONITOR")
	private HardwareMonitor hardwareMonitor;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSerial() {
		return serial;
	}
	
	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	public Firmware getFirmware() {
		return firmware;
	}
	
	public void setFirmware(Firmware firmware) {
		this.firmware = firmware;
	}
	
	public Gps getGps() {
		return gps;
	}
	
	public void setGps(Gps gps) {
		this.gps = gps;
	}
	
	public HardwareMonitor getHardwareMonitor() {
		return hardwareMonitor;
	}
	
	public void setHardwareMonitor(HardwareMonitor hardwareMonitor) {
		this.hardwareMonitor = hardwareMonitor;
	}
	
	@Override
	public Message getMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub 
		return false;
	}
	
	@Override
	public Double getLatitude() {
		if(gps != null && gps.getLatitude() != null){
			float aux = gps.getLatitude();
			return (double) aux;
		}
		return null;
	}
	
	@Override
	public Double getLongitude() {
		if(gps != null && gps.getLongitude() != null){
			float aux = gps.getLongitude();
			return (double) aux;
		}
		return null;
	}
	
	@Override
	public Integer getAltitude() {
		if(gps != null && gps.getAltitude() != null){
			return gps.getAltitude();
		}
		return null;
	}
	
	@Override
	public Double getCourse() {
		return null;
	}
	
	@Override
	public Double getSpeed() {
		if(gps != null && gps.getSpeed()!= null){
			return gps.getSpeed();
		}

		return null;
	}
	
	@Override
	public Boolean getMoving() {
		if(gps != null && gps.getSpeed()!= null){
			return gps.getSpeed() > 1;
		}
		
		return null;
	}
	
	@Override
	public Date getDate() {
		if(gps != null && gps.getDate() != null){
			return gps.getDate();
		}

		return null;
	}
	
	@Override
	public Boolean getIgnition() {
		if(hardwareMonitor != null && hardwareMonitor.getSecureInputMode() != null) {
			return hardwareMonitor.getSecureInputMode().getIgnition();
		}
	
		return null;
	}
	
	@Override
	public Boolean getPanic() {
		if(hardwareMonitor != null && hardwareMonitor.getInputs() != null) {
			return hardwareMonitor.getInputs().getPanic();
		}

		return null;
	}
	
	@Override
	public Integer getRpm() {
		if(hardwareMonitor != null && hardwareMonitor.getRpm() != null){
			return hardwareMonitor.getRpm();
		}
		
		return null;
	}
	
	@Override
	public Boolean getOpenedDoor() {
		if(hardwareMonitor != null && hardwareMonitor.getFlagState() != null){
			return hardwareMonitor.getFlagState().getOpenedDoor();
		}
		
		return null;
	}
	
	@Override
	public Boolean getEngineBlocked() {
		//TODO see how the engine blocked status comes in the message
		return null;
	}
	
	@Override
	public String getCode() {
		return serial;
	}
	
	@Override
	public long getDateInMills() {
		if(getDate() == null){
			return 0;
		}
		return getDate().getTime();
	}
	
}
