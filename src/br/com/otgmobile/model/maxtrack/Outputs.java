package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_outputs")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class Outputs {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_outputs_sequence", sequenceName="maxtrack_outputs_sequence")
	private Long id;
	
	@XmlElement(name = "OUTPUT1")
	private Integer output1;
	@XmlElement(name = "OUTPUT2")
	private Integer output2;
	@XmlElement(name = "OUTPUT3")
	private Integer output3;
	@XmlElement(name = "OUTPUT4")
	private Integer output4;
	@XmlElement(name = "OUTPUT5")
	private Integer output5;
	@XmlElement(name = "OUTPUT6")
	private Integer output6;
	@XmlElement(name = "OUTPUT7")
	private Integer output7;
	@XmlElement(name = "OUTPUT8")
	private Integer output8;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getOutput1() {
		return output1;
	}
	public void setOutput1(Integer output1) {
		this.output1 = output1;
	}
	public Integer getOutput2() {
		return output2;
	}
	public void setOutput2(Integer output2) {
		this.output2 = output2;
	}
	public Integer getOutput3() {
		return output3;
	}
	public void setOutput3(Integer output3) {
		this.output3 = output3;
	}
	public Integer getOutput4() {
		return output4;
	}
	public void setOutput4(Integer output4) {
		this.output4 = output4;
	}
	public Integer getOutput5() {
		return output5;
	}
	public void setOutput5(Integer output5) {
		this.output5 = output5;
	}
	public Integer getOutput6() {
		return output6;
	}
	public void setOutput6(Integer output6) {
		this.output6 = output6;
	}
	public Integer getOutput7() {
		return output7;
	}
	public void setOutput7(Integer output7) {
		this.output7 = output7;
	}
	public Integer getOutput8() {
		return output8;
	}
	public void setOutput8(Integer output8) {
		this.output8 = output8;
	}
	
}
