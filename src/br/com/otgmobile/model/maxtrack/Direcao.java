package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(Integer.class)
public enum Direcao {
	
	@XmlEnumValue("0") NORTE,
	@XmlEnumValue("1") NORDESTE,
	@XmlEnumValue("2") LESTE,
	@XmlEnumValue("3") SUDESTE,
	@XmlEnumValue("4") SUL,
	@XmlEnumValue("5") SUDOESTE,
	@XmlEnumValue("6") OESTE,
	@XmlEnumValue("7") NOROESTE,
	@XmlEnumValue("8") QUALQUER_DIRECAO
}
