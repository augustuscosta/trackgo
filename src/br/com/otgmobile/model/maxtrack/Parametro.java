package br.com.otgmobile.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_parametro")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PARAMETER")
public class Parametro {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_parameter_sequence", sequenceName="maxtrack_parameter_sequence")
	@XmlTransient
	private Long idParameter;
	
	@XmlElement(name = "ID")
	private String id;
	@XmlElement(name = "VALUE")
	private String value;
	
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@XmlTransient
	private Comando comando;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Comando getComando() {
		return comando;
	}
	public void setComando(Comando comando) {
		this.comando = comando;
	}
	public Long getIdParameter() {
		return idParameter;
	}
	public void setIdParameter(Long idParameter) {
		this.idParameter = idParameter;
	}
}
