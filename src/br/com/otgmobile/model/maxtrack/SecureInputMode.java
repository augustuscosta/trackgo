package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_secure_input_mode")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class SecureInputMode {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_secure_input_mode_sequence", sequenceName="maxtrack_secure_input_mode_sequence")
	private Long id;

	@XmlElement(name = "IGNITION")
	private Boolean ignition;
	@XmlElement(name = "INPUT1")
	private Integer input1;
	@XmlElement(name = "INPUT2")
	private Integer input2;
	@XmlElement(name = "INPUT3")
	private Integer input3;
	@XmlElement(name = "INPUT4")
	private Integer input4;
	@XmlElement(name = "INPUT5")
	private Integer input5;
	@XmlElement(name = "INPUT6")
	private Integer input6;
	@XmlElement(name = "INPUT7")
	private Integer input7;
	@XmlElement(name = "INPUT8")
	private Integer input8;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getIgnition() {
		return ignition;
	}
	public void setIgnition(Boolean ignition) {
		this.ignition = ignition;
	}
	public Integer getInput1() {
		return input1;
	}
	public void setInput1(Integer input1) {
		this.input1 = input1;
	}
	public Integer getInput2() {
		return input2;
	}
	public void setInput2(Integer input2) {
		this.input2 = input2;
	}
	public Integer getInput3() {
		return input3;
	}
	public void setInput3(Integer input3) {
		this.input3 = input3;
	}
	public Integer getInput4() {
		return input4;
	}
	public void setInput4(Integer input4) {
		this.input4 = input4;
	}
	public Integer getInput5() {
		return input5;
	}
	public void setInput5(Integer input5) {
		this.input5 = input5;
	}
	public Integer getInput6() {
		return input6;
	}
	public void setInput6(Integer input6) {
		this.input6 = input6;
	}
	public Integer getInput7() {
		return input7;
	}
	public void setInput7(Integer input7) {
		this.input7 = input7;
	}
	public Integer getInput8() {
		return input8;
	}
	public void setInput8(Integer input8) {
		this.input8 = input8;
	}
	
	
}
