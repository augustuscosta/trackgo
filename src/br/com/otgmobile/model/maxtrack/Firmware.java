package br.com.otgmobile.model.maxtrack;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_firmware")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class Firmware {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_firmware_sequence", sequenceName="maxtrack_firmware_sequence")
	private Long id;
	@XmlElement(name = "SERIAL")
	private String serial;
	@XmlElement(name = "PROTOCOL")
	private Integer protocol;  				// código de protocolo do equipamento
	@XmlElement(name = "MEMORY_INDEX")
	private Long memoryIndex; 			// contador de posições AVL
	@XmlElement(name = "LIFE_TIME")
	private Long lifeTime; 					// Tempo decorrido desde o último boot MXT
	@XmlElement(name = "TRANSMISSION_REASON")
	@Enumerated(EnumType.STRING)
	private MotivoTransmissao transmissionReason;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "FLAG_STATE")
	private FirmwareFlagState flagState;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Integer getProtocol() {
		return protocol;
	}
	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}
	public Long getMemoryIndex() {
		return memoryIndex;
	}
	public void setMemoryIndex(Long memoryIndex) {
		this.memoryIndex = memoryIndex;
	}
	public Long getLifeTime() {
		return lifeTime;
	}
	public void setLifeTime(Long lifeTime) {
		this.lifeTime = lifeTime;
	}
	public MotivoTransmissao getTransmissionReason() {
		return transmissionReason;
	}
	public void setTransmissionReason(MotivoTransmissao transmissionReason) {
		this.transmissionReason = transmissionReason;
	}
	public FirmwareFlagState getFlagState() {
		return flagState;
	}
	public void setFlagState(FirmwareFlagState flagState) {
		this.flagState = flagState;
	}
	
}
