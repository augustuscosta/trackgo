package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_hardware_monitor_flag_state")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class HardwareMonitorFlagState {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_hardware_monitor_flag_state_sequence", sequenceName="maxtrack_hardware_monitor_flag_state_sequence")
	private Long id;

	@XmlElement(name = "LOW_POWER")
	private Boolean lowPower;
	@XmlElement(name = "CFC")
	private Boolean cfc;
	@XmlElement(name = "SATELITAL")
	private Boolean satelital;
	@XmlElement(name = "SLEEP_MODE")
	private Boolean sleepMode;
	@XmlElement(name = "MAIN_SERIAL")
	private Boolean mainSerial;
	@XmlElement(name = "ADDITIONAL_SERIAL")
	private Boolean additionalSerial;
	@XmlElement(name = "INTERNAL_ALARM")
	private Boolean internalAlarm;
	@XmlElement(name = "OPENED_DOOR")
	private Boolean openedDoor;
	@XmlElement(name = "MONEY_MACHINE1_FAILURE")
	private Boolean moneyMachine1Failure;
	@XmlElement(name = "MONEY_MACHINE2_FAILURE")
	private Boolean moneyMachine2Failure;
	@XmlElement(name = "COIN_MACHINE_FAILURE")
	private Boolean coinMachineFailure;
	@XmlElement(name = "PASSENGER_COUNT_FAILURE")
	private Boolean passengerCountFailure;
	@XmlElement(name = "DRV_FAILURE")
	private Boolean drvFailure;
	@XmlElement(name = "KEYBOARD_FAILURE")
	private Boolean keyboardFailure;
	@XmlElement(name = "PANEL_FAILURE")
	private Boolean panelFailure;
	@XmlElement(name = "CAMERA_FAILURE")
	private Boolean cameraFailure;
	@XmlElement(name = "COIN_MACHINE_FULL")
	private Boolean coinMachineFull;
	@XmlElement(name = "SCHEDULE_BLOCK")
	private Boolean scheduleBlock;
	@XmlElement(name = "BATTERY_FAILURE")
	private Boolean batteryFailure;
	@XmlElement(name = "BATTERY_CHARGING")
	private Boolean batteryCharging;
	@XmlElement(name = "TELENODO_FAILURE")
	private Boolean telenodoFailure;
	@XmlElement(name = "STREAD_SPECTRUM_FAILURE")
	private Boolean streadSpectrumFailure;
	@XmlElement(name = "ACESSORY_MISSING")
	private Boolean acessoryMissing;
	@XmlElement(name = "ANTI_THEFT_STATUS")
	private AntiTheftStatus antiTheftStatus;
	@XmlElement(name = "AD2_FILTER")
	private Integer ad2Filter;
	@XmlElement(name = "AD3_FILTER")
	private Integer ad3Filter;
	@XmlElement(name = "AD4_FILTER")
	private Integer ad4Filter;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getLowPower() {
		return lowPower;
	}
	public void setLowPower(Boolean lowPower) {
		this.lowPower = lowPower;
	}
	public Boolean getCfc() {
		return cfc;
	}
	public void setCfc(Boolean cfc) {
		this.cfc = cfc;
	}
	public Boolean getSatelital() {
		return satelital;
	}
	public void setSatelital(Boolean satelital) {
		this.satelital = satelital;
	}
	public Boolean getSleepMode() {
		return sleepMode;
	}
	public void setSleepMode(Boolean sleepMode) {
		this.sleepMode = sleepMode;
	}
	public Boolean getMainSerial() {
		return mainSerial;
	}
	public void setMainSerial(Boolean mainSerial) {
		this.mainSerial = mainSerial;
	}
	public Boolean getAdditionalSerial() {
		return additionalSerial;
	}
	public void setAdditionalSerial(Boolean additionalSerial) {
		this.additionalSerial = additionalSerial;
	}
	public Boolean getInternalAlarm() {
		return internalAlarm;
	}
	public void setInternalAlarm(Boolean internalAlarm) {
		this.internalAlarm = internalAlarm;
	}
	public Boolean getOpenedDoor() {
		return openedDoor;
	}
	public void setOpenedDoor(Boolean openedDoor) {
		this.openedDoor = openedDoor;
	}
	public Boolean getMoneyMachine1Failure() {
		return moneyMachine1Failure;
	}
	public void setMoneyMachine1Failure(Boolean moneyMachine1Failure) {
		this.moneyMachine1Failure = moneyMachine1Failure;
	}
	public Boolean getMoneyMachine2Failure() {
		return moneyMachine2Failure;
	}
	public void setMoneyMachine2Failure(Boolean moneyMachine2Failure) {
		this.moneyMachine2Failure = moneyMachine2Failure;
	}
	public Boolean getCoinMachineFailure() {
		return coinMachineFailure;
	}
	public void setCoinMachineFailure(Boolean coinMachineFailure) {
		this.coinMachineFailure = coinMachineFailure;
	}
	public Boolean getPassengerCountFailure() {
		return passengerCountFailure;
	}
	public void setPassengerCountFailure(Boolean passengerCountFailure) {
		this.passengerCountFailure = passengerCountFailure;
	}
	public Boolean getDrvFailure() {
		return drvFailure;
	}
	public void setDrvFailure(Boolean drvFailure) {
		this.drvFailure = drvFailure;
	}
	public Boolean getKeyboardFailure() {
		return keyboardFailure;
	}
	public void setKeyboardFailure(Boolean keyboardFailure) {
		this.keyboardFailure = keyboardFailure;
	}
	public Boolean getPanelFailure() {
		return panelFailure;
	}
	public void setPanelFailure(Boolean panelFailure) {
		this.panelFailure = panelFailure;
	}
	public Boolean getCameraFailure() {
		return cameraFailure;
	}
	public void setCameraFailure(Boolean cameraFailure) {
		this.cameraFailure = cameraFailure;
	}
	public Boolean getCoinMachineFull() {
		return coinMachineFull;
	}
	public void setCoinMachineFull(Boolean coinMachineFull) {
		this.coinMachineFull = coinMachineFull;
	}
	public Boolean getScheduleBlock() {
		return scheduleBlock;
	}
	public void setScheduleBlock(Boolean scheduleBlock) {
		this.scheduleBlock = scheduleBlock;
	}
	public Boolean getBatteryFailure() {
		return batteryFailure;
	}
	public void setBatteryFailure(Boolean batteryFailure) {
		this.batteryFailure = batteryFailure;
	}
	public Boolean getBatteryCharging() {
		return batteryCharging;
	}
	public void setBatteryCharging(Boolean batteryCharging) {
		this.batteryCharging = batteryCharging;
	}
	public Boolean getTelenodoFailure() {
		return telenodoFailure;
	}
	public void setTelenodoFailure(Boolean telenodoFailure) {
		this.telenodoFailure = telenodoFailure;
	}
	public Boolean getStreadSpectrumFailure() {
		return streadSpectrumFailure;
	}
	public void setStreadSpectrumFailure(Boolean streadSpectrumFailure) {
		this.streadSpectrumFailure = streadSpectrumFailure;
	}
	public Boolean getAcessoryMissing() {
		return acessoryMissing;
	}
	public void setAcessoryMissing(Boolean acessoryMissing) {
		this.acessoryMissing = acessoryMissing;
	}
	public AntiTheftStatus getAntiTheftStatus() {
		return antiTheftStatus;
	}
	public void setAntiTheftStatus(AntiTheftStatus antiTheftStatus) {
		this.antiTheftStatus = antiTheftStatus;
	}
	public Integer getAd2Filter() {
		return ad2Filter;
	}
	public void setAd2Filter(Integer ad2Filter) {
		this.ad2Filter = ad2Filter;
	}
	public Integer getAd3Filter() {
		return ad3Filter;
	}
	public void setAd3Filter(Integer ad3Filter) {
		this.ad3Filter = ad3Filter;
	}
	public Integer getAd4Filter() {
		return ad4Filter;
	}
	public void setAd4Filter(Integer ad4Filter) {
		this.ad4Filter = ad4Filter;
	}
	
}
