package br.com.otgmobile.model.maxtrack;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.annotations.Index;

import br.com.otgmobile.util.JaxbDateSerializer;

@Entity
@Table(name="maxtrack_gps")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class Gps {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_gps_sequence", sequenceName="maxtrack_gps_sequence")
	private Long id;
	
	@Index(name="gps_date_index")
	@XmlElement(name = "DATE")
	@XmlJavaTypeAdapter(JaxbDateSerializer.class)
	private Date date;                      // data hora do módulo
	@XmlElement(name = "LATITUDE")
	private Float latitude;
	@XmlElement(name = "LONGITUDE")
	private Float longitude;
	@XmlElement(name = "ALTITUDE")
	private Integer altitude;
	@XmlElement(name = "COURSE")
	private Direcao course;  
	@XmlElement(name = "SPEED")
	private Double speed;
	@XmlElement(name = "CSQ")
	private Integer csq;					//qualidade do sinal do módulo
	@XmlElement(name = "HDOP")
	private Double hdop;
	@XmlElement(name = "SVN")
	private Integer svn;                    //numero de satelites
	@XmlElement(name = "HODOMETER")
	private Integer hodometer;
	@XmlElement(name = "LAST_VALID_POSITION_TIME")
	private Long lastValidPositionTime;    // última posição válida
	
	//TODO métodos para converter e 
	
	/*Converter para binario. Os 4 bits mais significativos são convertidos 
	 * para hexadecimal e indicam qual eixo teve seu limite atingido onde:
	 *  Limite sensor lateral: 			0x08
	 *  Limite sensor frente - trás: 	0x04
	 *  Limite sensor vertical:			0x02
	 *  Os 4 bits menos significativos registram o maior valor registrado entre a gravação dessa posição e da anterior
	 *  em uma escala de 1 a 8 G com uma variação de 0,5G
	 */
	@XmlElement(name = "ACELEROMETER_EVENT")
	private String acelerometerEvent; 
	
	/*Converter para binario. 
	 * Medido a cada 2 segundos em Gs/10.
	 * Variação desse valor é entre 0 e 1,6G
	 * 4 bits menos significativos: Valor médio das 4 últimas medições feitas pelo acelerômetro em relação ao eixo vertical.
	 * 4 bit mais significativos: Valor médio das 4 últimas medições feitas pelo acelerômetro em relação ao eixo lateral.
	 */
	@XmlElement(name = "ACELEROMETER_VALUE")
	private String acelerometerValue;
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	@XmlElement(name = "FLAG_STATE")
	private GpsFlagState flagState;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Integer getAltitude() {
		return altitude;
	}

	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}

	public Direcao getCourse() {
		return course;
	}

	public void setCourse(Direcao course) {
		this.course = course;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Integer getCsq() {
		return csq;
	}

	public void setCsq(Integer csq) {
		this.csq = csq;
	}

	public Double getHdop() {
		return hdop;
	}

	public void setHdop(Double hdop) {
		this.hdop = hdop;
	}

	public Integer getSvn() {
		return svn;
	}

	public void setSvn(Integer svn) {
		this.svn = svn;
	}

	public Integer getHodometer() {
		return hodometer;
	}

	public void setHodometer(Integer hodometer) {
		this.hodometer = hodometer;
	}

	public Long getLastValidPositionTime() {
		return lastValidPositionTime;
	}

	public void setLastValidPositionTime(Long lastValidPositionTime) {
		this.lastValidPositionTime = lastValidPositionTime;
	}

	public String getAcelerometerEvent() {
		return acelerometerEvent;
	}

	public void setAcelerometerEvent(String acelerometerEvent) {
		this.acelerometerEvent = acelerometerEvent;
	}

	public String getAcelerometerValue() {
		return acelerometerValue;
	}

	public void setAcelerometerValue(String acelerometerValue) {
		this.acelerometerValue = acelerometerValue;
	}

	public GpsFlagState getFlagState() {
		return flagState;
	}

	public void setFlagState(GpsFlagState flagState) {
		this.flagState = flagState;
	}

}
