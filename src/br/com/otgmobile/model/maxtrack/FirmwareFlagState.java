package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_firmware_flag_state")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class FirmwareFlagState {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_firmware_flag_state_sequence", sequenceName="maxtrack_firmware_flag_state_sequence")
	private Long id;
	@XmlElement(name = "SETUP_MODIFIED")
	private Boolean setupModified;			// alteração no setup
	@XmlElement(name = "FIRMWARE_STORED")
	private Boolean firmwareStored;         // firmware armazenado
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getSetupModified() {
		return setupModified;
	}
	public void setSetupModified(Boolean setupModified) {
		this.setupModified = setupModified;
	}
	public Boolean getFirmwareStored() {
		return firmwareStored;
	}
	public void setFirmwareStored(Boolean firmwareStored) {
		this.firmwareStored = firmwareStored;
	}
}
