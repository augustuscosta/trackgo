package br.com.otgmobile.model.maxtrack;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import br.com.otgmobile.util.JaxbDateSerializer;

@Entity
@Table(name="maxtrack_command")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COMMAND")
public class Comando {

	@XmlElement(name = "PROTOCOL")
	@Enumerated(EnumType.STRING)
	private ProtocoloComando protocol;
	@XmlElement(name = "SERIAL")
	private String serial;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_command_sequence", sequenceName="maxtrack_command_sequence")
	@XmlElement(name = "ID_COMMAND")
	private Long idCommand;
	@XmlElement(name = "TYPE")
	@Enumerated(EnumType.STRING)
	private Integer type;
	@XmlElement(name = "ATTEMPTS")
	private Integer attempts;
	@XmlElement(name = "COMMAND_TIMEOUT")
	@XmlJavaTypeAdapter(JaxbDateSerializer.class)
	private Date commandTimeout; // yyyy-mm-dd hh:mm:ss
	@XmlElement(name = "PARAMETER")
	@XmlElementWrapper(name="PARAMETERS")
	@OneToMany(mappedBy="comando", cascade=CascadeType.ALL,targetEntity=Parametro.class)
	private List<Parametro> parameters;
	@OneToOne(cascade = CascadeType.ALL)
	@XmlTransient
	private ComandoEstado estado;
	@Column(name = "sent",nullable=false, columnDefinition="boolean default false")
	@XmlTransient
	private Boolean sent;
	
	
	public String getFileName(){
		if(!isDataCompleteForFileName())
			return null;
		StringBuilder builder = new StringBuilder();
		builder.append("C");
		builder.append("_");
		builder.append(getProtocol().getValue());
		builder.append("_");
		builder.append(getSerial());
		builder.append("_");
		builder.append(getIdCommand());
		builder.append(".cmd");
		return builder.toString();
	}
	private boolean isDataCompleteForFileName() {
		if(getProtocol() == null)
			return false;
		if(getSerial() == null)
			return false;
		if(getIdCommand() == null)
			return false;
		return true;
	}
	public ProtocoloComando getProtocol() {
		return protocol;
	}
	public void setProtocol(ProtocoloComando protocol) {
		this.protocol = protocol;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Long getIdCommand() {
		return idCommand;
	}
	public void setIdCommand(Long idCommand) {
		this.idCommand = idCommand;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getAttempts() {
		return attempts;
	}
	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}
	public Date getCommandTimeout() {
		return commandTimeout;
	}
	public void setCommandTimeout(Date commandTimeout) {
		this.commandTimeout = commandTimeout;
	}
	public List<Parametro> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parametro> parameters) {
		this.parameters = parameters;
	}
	public ComandoEstado getEstado() {
		return estado;
	}
	public void setEstado(ComandoEstado estado) {
		this.estado = estado;
	}
	public Boolean getSent() {
		return sent;
	}
	public void setSent(Boolean sent) {
		this.sent = sent;
	}
}
