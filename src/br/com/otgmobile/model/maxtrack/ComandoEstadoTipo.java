package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum ComandoEstadoTipo {

	@XmlEnumValue("1") COMANDO_ENVIADO,
	@XmlEnumValue("2") TENTATIVAS_EXCEDIDAS,
	@XmlEnumValue("3") EXPIROU_POR_TEMPO,
	@XmlEnumValue("4") CANCELADO,
	@XmlEnumValue("5") CONFIRMADO,
	@XmlEnumValue("6") XML_INVALIDO
}
