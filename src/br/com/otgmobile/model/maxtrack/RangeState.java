package br.com.otgmobile.model.maxtrack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name="maxtrack_range_state")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class RangeState {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="maxtrack_range_state_sequence", sequenceName="maxtrack_range_state_sequence")
	private Long id;
	
	@XmlElement(name = "INTERNAL_BATTERY")
	private EstadoIntervalo internalBattery;
	@XmlElement(name = "EXTERNAL_BATTERY")
	private EstadoIntervalo externalBattery;
	@XmlElement(name = "POWER_SUPPLY")
	private Integer powerSupply; 
	@XmlElement(name = "AD1RANGE")
	private ConfiguracaoIntervalo ad1Range;
	@XmlElement(name = "AD2RANGE")
	private ConfiguracaoIntervalo ad2Range;
	@XmlElement(name = "AD3RANGE")
	private ConfiguracaoIntervalo ad3Range;
	@XmlElement(name = "AD4RANGE")
	private ConfiguracaoIntervalo ad4Range;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public EstadoIntervalo getInternalBattery() {
		return internalBattery;
	}
	public void setInternalBattery(EstadoIntervalo internalBattery) {
		this.internalBattery = internalBattery;
	}
	public EstadoIntervalo getExternalBattery() {
		return externalBattery;
	}
	public void setExternalBattery(EstadoIntervalo externalBattery) {
		this.externalBattery = externalBattery;
	}
	public Integer getPowerSupply() {
		return powerSupply;
	}
	public void setPowerSupply(Integer powerSupply) {
		this.powerSupply = powerSupply;
	}
	public ConfiguracaoIntervalo getAd1Range() {
		return ad1Range;
	}
	public void setAd1Range(ConfiguracaoIntervalo ad1Range) {
		this.ad1Range = ad1Range;
	}
	public ConfiguracaoIntervalo getAd2Range() {
		return ad2Range;
	}
	public void setAd2Range(ConfiguracaoIntervalo ad2Range) {
		this.ad2Range = ad2Range;
	}
	public ConfiguracaoIntervalo getAd3Range() {
		return ad3Range;
	}
	public void setAd3Range(ConfiguracaoIntervalo ad3Range) {
		this.ad3Range = ad3Range;
	}
	public ConfiguracaoIntervalo getAd4Range() {
		return ad4Range;
	}
	public void setAd4Range(ConfiguracaoIntervalo ad4Range) {
		this.ad4Range = ad4Range;
	}
}
