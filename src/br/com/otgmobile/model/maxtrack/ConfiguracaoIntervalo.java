package br.com.otgmobile.model.maxtrack;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(Integer.class)
public enum ConfiguracaoIntervalo {

	@XmlEnumValue("0") NAO_CONFIGURADO,
	@XmlEnumValue("1") BAIXO,
	@XmlEnumValue("2") MEDIO,
	@XmlEnumValue("3") ALTO
}
