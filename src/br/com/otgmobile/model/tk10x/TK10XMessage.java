package br.com.otgmobile.model.tk10x;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.otgmobile.events.EventMessage;
import br.com.otgmobile.model.Message;

@Entity
@Table(name="tk10x")
public class TK10XMessage implements Message , EventMessage{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@SequenceGenerator(name="tk10x_sequence", sequenceName="tk10x_sequence")
	private Long id;
	private Long deviceId;
	private Long command;
	private String imei;
	private Date date;
	private Boolean valid;
	private Double latitude;
	private Double longitude;
	private Double speed;
	private Date time;
	private Double course;

	public TK10XMessage() {
	}
	
	public static TK10XMessage createWith(String message) {
		return new TK10XParser(message).parse();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getCommand() {
		return command;
	}

	public void setCommand(Long command) {
		this.command = command;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getCourse() {
		return course;
	}

	public void setCourse(Double course) {
		this.course = course;
	}

	@Override
	public Integer getAltitude() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getMoving() {
		return getSpeed() > 2;
	}

	@Override
	public Boolean getIgnition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getPanic() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRpm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getOpenedDoor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getEngineBlocked() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCode() {
		return getDeviceId().toString();
	}
	
	@Override
	public long getDateInMills() {
		if(getDate() == null){
			return 0;
		}
		return getDate().getTime();
	}

}
