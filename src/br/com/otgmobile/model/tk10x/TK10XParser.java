package br.com.otgmobile.model.tk10x;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TK10XParser {
	private static final Pattern PATTERN = Pattern.compile(
			"\\(" +
			"(\\d+)" +                   // Device ID
			"(.{4})" +                   // Command
			"(\\d{15})" +                // IMEI (?)
			"(\\d{2})(\\d{2})(\\d{2})" + // Date (YYMMDD)
			"([AV])" +                   // Validity
			"(\\d{2})(\\d{2}.\\d{4})" +  // Latitude (DDMM.MMMM)
			"([NS])" +
			"(\\d{3})(\\d{2}.\\d{4})" +  // Longitude (DDDMM.MMMM)
			"([EW])" +
			"(\\d+.\\d)" +               // Speed
			"(\\d{2})(\\d{2})(\\d{2})" + // Time (HHMMSS)
			"(\\d+.\\d{2})" +            // Course
			"(\\d+)" +                   // State
		".+");                       // Mileage (?)
	private final String message;
	
	TK10XParser(String message) {
		this.message = message;
	}

	public TK10XMessage parse() {
		Matcher parser = PATTERN.matcher(message);
		if (parser.matches()) {
			TK10XMessage message = new TK10XMessage();
			message.setDeviceId(Long.valueOf(parser.group(1)));
//			message.setCommand(Long.valueOf(parser.group(1)));
			message.setImei(parser.group(2));
			try {
				message.setDate(new SimpleDateFormat("yyMMdd").parse(parser.group(4) + parser.group(5) + parser.group(6)));
			} catch (ParseException e) {
				message.setDate(null);
			}
			message.setValid(parser.group(7).compareTo("A") == 0 ? true : false);
			message.setLatitude(Double.valueOf(parser.group(12)));
			message.setLongitude(Double.valueOf(parser.group(9)));
//			message.setSpeed(Double.valueOf(parser.group(7)));
			try {
				message.setTime(new SimpleDateFormat("hhmmss").parse(parser.group(15) + parser.group(16) + parser.group(17)));
			} catch (ParseException e) {
				message.setTime(null);
			}
			message.setCourse(Double.valueOf(parser.group(18)));
			
			return message;
			// Get device by IMEI
//			String imei = parser.group(index++);
//			position.setDeviceId(getDeviceByImei(imei).getId());
//	
//			// Latitude
//			Double latitude = Double.valueOf(parser.group(index++));
//			latitude += Double.valueOf(parser.group(index++)) / 60;
//			if (parser.group(index++).compareTo("S") == 0) latitude = -latitude;
//			position.setLatitude(latitude);
//	
//			// Longitude
//			Double lonlitude = Double.valueOf(parser.group(index++));
//			lonlitude += Double.valueOf(parser.group(index++)) / 60;
//			if (parser.group(index++).compareTo("W") == 0) lonlitude = -lonlitude;
//			position.setLongitude(lonlitude);
//	
//			// Time
//			time.set(Calendar.HOUR, Integer.valueOf(parser.group(index++)));
//			time.set(Calendar.MINUTE, Integer.valueOf(parser.group(index++)));
//			time.set(Calendar.SECOND, Integer.valueOf(parser.group(index++)));
//			position.setTime(time.getTime());
		}
		return null;
	}
}
