package br.com.otgmobile.model;

public enum Permissao {

	ROLE_ADMINISTRATOR,
	ROLE_USER
}
