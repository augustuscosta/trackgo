package br.com.otgmobile.server;

public class UnavailableMessageException extends RuntimeException {

	private static final long serialVersionUID = 2323534649490593811L;

	public UnavailableMessageException() {
		super("Tipo de módulo não suportado!");
	}
	
}
