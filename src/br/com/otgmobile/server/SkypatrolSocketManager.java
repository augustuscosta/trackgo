package br.com.otgmobile.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.model.skypatrol.SkypatrolParser;
import br.com.otgmobile.service.skypatrol.SkypatrolService;

@Service
public class SkypatrolSocketManager {
	private static final Logger logger = Logger.getLogger(SkypatrolSocketManager.class);
	private static final int PORT = 7778;
	private DatagramSocket server;
	private byte[] receiveData = new byte[160];
    private byte[] sendData = new byte[160];
    
    @Autowired
    SkypatrolService service;

	public SkypatrolSocketManager(){
	}
	
	public void start() {
		try {
			if(server == null)
				server = new DatagramSocket(PORT);
		} catch (IOException e) {
			logger.error(e.getMessage(), e.getCause());
			// TODO throw new SocketConnectionException();
		}

		while (true) {
			try {
				SkypatrolParser parser = readSocketMessage();
				if(parser.isReportMessage())
					new SocketThreadManager(parser.getSkypatrolMessage(), service).start();
			} catch (IOException e) {
				logger.error(e.getMessage(), e.getCause());
			} catch (ClassNotFoundException e) {
				logger.error(e.getMessage(), e.getCause());
			}
		}
	}
	
	private SkypatrolParser readSocketMessage() throws IOException, ClassNotFoundException {
		
		
		logger.info("Waiting for skypatrol client message...");
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        server.receive(receivePacket);
        String message = new String( receivePacket.getData());
		logger.info("Message Received: " + message);
		SkypatrolParser parser = new SkypatrolParser(message);
		
		InetAddress IPAddress = receivePacket.getAddress();
        int port = receivePacket.getPort();
        
        if(parser.needAcknowledgement()){
	        String ack = parser.getAcknowledgement();
	        logger.info("Sending ack message: " + message);
			sendData = ack.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
	        server.send(sendPacket);
        }
        
		return parser;
	}
}