package br.com.otgmobile.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.model.niagara2.ParseProtocol;
import br.com.otgmobile.service.niagara2.Niagra2Service;

@Service
public class Niagara2SocketManager {
	private static final Logger logger = Logger.getLogger(Niagara2SocketManager.class);
	private static final int PORT = 7779;
	private DatagramSocket server;
	private byte[] receiveData = new byte[1024];
    private byte[] sendData = new byte[1024];
    
    @Autowired
    Niagra2Service service;

	public Niagara2SocketManager(){
	}
	
	public void start() {
		try {
			if(server == null)
				server = new DatagramSocket(PORT);
		} catch (IOException e) {
			logger.error(e.getMessage(), e.getCause());
			// TODO throw new SocketConnectionException();
		}

		while (true) {
			try {
				ParseProtocol parser = new ParseProtocol(new String(readSocketMessage()));
				new SocketThreadManager(parser.getProtocolNiagara2Message(), service).start();
			} catch (IOException e) {
				logger.error(e.getMessage(), e.getCause());
			} catch (ClassNotFoundException e) {
				logger.error(e.getMessage(), e.getCause());
			}
		}
	}
	
	private byte[] readSocketMessage() throws IOException, ClassNotFoundException {
		
		logger.info("Waiting for niagara  2 client message...");
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        server.receive(receivePacket);
        byte[] message =  new byte[receivePacket.getData().length];
		
		InetAddress IPAddress = receivePacket.getAddress();
        int port = receivePacket.getPort();
    		ParseProtocol parser = new ParseProtocol(new String(receivePacket.getData()));
        logger.info("Sending ack message: " + message);
		sendData = parser.getAckNowlegment();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        server.send(sendPacket);
        
		return message;
	}
}