package br.com.otgmobile.server;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TK10XDaemon extends Thread{

	private static final Logger logger = Logger.getLogger(TK10XDaemon.class);
	private final Integer TIMER = 1000;
	private Thread myselfThread = null;

	@Autowired
	private Tk10XSocketManager socketManager;

	@Override
	public void run() {
		
		
		for (;;) {
			try {
				logger.info("Run tk10x daemon!");
				
				socketManager.start();
				
			} catch (Throwable e) {
				
				logger.error("Erro na thread tk10x", e);

			} finally {
				try {
					Thread.sleep(TIMER);
				} catch (InterruptedException e1) {
					logger.error("Erro colocando a thread tk10x para dormir ", e1);
				}
			}
		}
		
	}

	public void start() {
		if (myselfThread == null) {
			myselfThread = new Thread(this);
			myselfThread.setPriority(Thread.MIN_PRIORITY);
			myselfThread.start();
		}
	}
}
