package br.com.otgmobile.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.niagarahw06.ComandoNiagaraHw06;
import br.com.otgmobile.model.niagarahw06.NiagaraHw06MessageType;
import br.com.otgmobile.model.niagarahw06.NiagaraHw06Parser;
import br.com.otgmobile.service.dao.ComandoNiagaraHw06DAO;
import br.com.otgmobile.service.niagarahw06.NiagraHw06Service;
import br.com.otgmobile.util.StringUtil;

@Service
public class NiagaraSocketManager {
	private static final Logger logger = Logger.getLogger(NiagaraSocketManager.class);
	private static final int PORT = 7777;
	private DatagramSocket server;
	private byte[] receiveData = new byte[1024];
    private byte[] sendData = new byte[1024];
    
    @Autowired
    NiagraHw06Service service;
    
    
    @Autowired
	private ComandoNiagaraHw06DAO comandoDAO;
    

	public NiagaraSocketManager(){
	}
	
	public void start() {
		try {
			if(server == null)
				server = new DatagramSocket(PORT);
		} catch (IOException e) {
			logger.error(e.getMessage(), e.getCause());
		}

		while (true) {
			try {
				NiagaraHw06Parser parser;
			
					parser = new NiagaraHw06Parser(StringUtil.fromByteArray(readSocketMessage()));
					new SocketThreadManager(parser.getParsedMessage(), service).start();
			} catch (IOException e) {
				logger.error(e.getMessage(), e.getCause());
			} catch (ClassNotFoundException e) {
				logger.error(e.getMessage(), e.getCause());
			} catch (DecoderException e) {
				logger.error(e.getMessage(), e.getCause());
			}
		}
	}
	
	private byte[] readSocketMessage() throws IOException, ClassNotFoundException, DecoderException {
		
		logger.info("Waiting for niagara client message...");
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        server.receive(receivePacket);
        byte[] message =  new byte[receivePacket.getData().length];
        message = receivePacket.getData();
        logger.info("RecievingPAcketMessage: " +new String(Hex.encodeHex(receivePacket.getData())));
		InetAddress IPAddress = receivePacket.getAddress();
        int port = receivePacket.getPort();
    	NiagaraHw06Parser parser = new NiagaraHw06Parser(StringUtil.fromByteArray(receivePacket.getData()));
        String ack = parser.generateAcknoledgeMessage().generateHexAcknolegde();
        
        if(parser.parseMessagetype() == NiagaraHw06MessageType.ACK.getValue()){
        	updateComandoStatus(parser.parseSequenceControll());
        	return message;
        }
        
        logger.info("Sending ack message: " + ack);
		sendData = Hex.decodeHex(ack.toCharArray());
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        server.send(sendPacket);
        sendCommads(IPAddress, port, parser);
        return message;
	}

		
	private void updateComandoStatus(int sequenceControl) {
		ComandoNiagaraHw06 comando = comandoDAO.findNotSent(sequenceControl);
		if(comando == null){
			return;
		}
		
		logger.info("Recebido o ACK do comando" + comando.getSequenceControlNumber() + comando.getServerMessageType().name());
		comando.setSent(true);
		comandoDAO.update(comando);
	}

	private void sendCommads(InetAddress IPAddress, int port,NiagaraHw06Parser parser) throws DecoderException, IOException {
		DevicePathCriteria criteria = new DevicePathCriteria();
		Device device = new Device();
		device.setCode(parser.parseProductSerialNumber());
		criteria.setDevice(device);
		List<ComandoNiagaraHw06> comandos = comandoDAO.getComandosNotSent(criteria);
		
		if (comandos == null || comandos.isEmpty()) {
			logger.info("Without commands to send ");
		}
		
		for (ComandoNiagaraHw06 comandoNiagaraHw06 : comandos) {
			logger.info("Sending message: " + comandoNiagaraHw06.generateServerMessage());
			byte[] sendData = Hex.decodeHex(comandoNiagaraHw06.generateServerMessage().toCharArray());
			DatagramPacket sendCommand = new DatagramPacket(sendData, sendData.length, IPAddress, port);			
			server.send(sendCommand);
		}
	}
}