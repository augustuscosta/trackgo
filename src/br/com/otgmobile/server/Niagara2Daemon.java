package br.com.otgmobile.server;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Niagara2Daemon extends Thread{

	private static final Logger logger = Logger.getLogger(Niagara2Daemon.class);
	private final Integer TIMER = 1000;
	private Thread myselfThread = null;

	@Autowired
	private Niagara2SocketManager socketManager;

	@Override
	public void run() {
		
		
		for (;;) {
			try {
				logger.info("Run niagara 2 daemon!");
				
				socketManager.start();
				
			} catch (Throwable e) {
				
				logger.error("Erro na thread niagara", e);

			} finally {
				try {
					Thread.sleep(TIMER);
				} catch (InterruptedException e1) {
					logger.error("Erro colocando a thread niagara ", e1);
				}
			}
		}
		
	}

	public void start() {
		if (myselfThread == null) {
			myselfThread = new Thread(this);
			myselfThread.setPriority(Thread.MIN_PRIORITY);
			myselfThread.start();
		}
	}
}
