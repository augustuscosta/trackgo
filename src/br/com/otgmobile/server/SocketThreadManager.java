package br.com.otgmobile.server;

import org.apache.log4j.Logger;

import br.com.otgmobile.model.Message;

public class SocketThreadManager {
	private final ThreadWrapper thread;
	
	public SocketThreadManager(Message tracker, Service service) {
		thread = new ThreadWrapper(tracker, service);
	}

	public void start() {
		new Thread(thread).start();
	}
}

class ThreadWrapper implements Runnable {
	private static final Logger logger = Logger.getLogger(ThreadWrapper.class);

	private final Message tracker;
	private final Service service;

	public ThreadWrapper(Message tracker, Service service) {
		this.tracker = tracker;
		this.service = service;
	}

	@Override
	public void run() {
		logger.info("Salvando a mensagem");
		service.save(tracker);
		logger.info("Mensagem salva");
	}
	
}