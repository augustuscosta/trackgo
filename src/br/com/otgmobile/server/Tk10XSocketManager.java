package br.com.otgmobile.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.model.tk10x.TK10XMessage;
import br.com.otgmobile.service.tk103.TK103Service;


@Service("tk10xSocketManager")
public class Tk10XSocketManager {
	private static final Logger logger = Logger.getLogger(Tk10XSocketManager.class);
	private static final int PORT = 7771;
	private ServerSocket server;
	
	@Autowired
	private TK103Service service;

	public Tk10XSocketManager(){
	}
	
	public void start() {
		try {
			if(server == null)
				server = new ServerSocket(PORT);
		} catch (IOException e) {
			logger.error(e.getMessage(), e.getCause());
			// TODO throw new SocketConnectionException();
		}

		while (true) {
			try {
				String message = readSocketMessage();
				TK10XMessage tk10Message = TK10XMessage.createWith(message);
				new SocketThreadManager(tk10Message, service).start();
			} catch (IOException e) {
				logger.error(e.getMessage(), e.getCause());
			} 
		}
	}

	private String readSocketMessage() throws IOException{
		logger.info("Waiting for tk10x client message...");
		Socket socket = server.accept();
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		String message = null;
		try {
			message = (String) ois.readObject();
			logger.info("Message Received: " + message);
		} catch (IOException e) {
			logger.error(e.getMessage(), e.getCause());
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e.getCause());
		} finally {
			ois.close();
			socket.close();
		}
		
		return message;
	}
}