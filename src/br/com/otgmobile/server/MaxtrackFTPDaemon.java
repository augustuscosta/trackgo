package br.com.otgmobile.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import scala.App;
import br.com.otgmobile.util.SecurityKey;
import br.com.otgmobile.util.FileTypeUtil;

@Service
public class MaxtrackFTPDaemon extends Thread{

	private static final Logger logger = Logger.getLogger(MaxtrackFTPDaemon.class);

	private Thread myselfThread = null;
	private Properties maxtrackProperties;
	private static final String MAXTRACK_FOLDER_PROPERTIES = "maxtrack.folders.properties";
	private Properties ftpProperties;
	private static final String FTP_PROPERTIES = "ftp.properties";
	private FTPClient ftp;
	
	public void start() {
		if (myselfThread == null) {
			myselfThread = new Thread(this);
			myselfThread.setPriority(Thread.MIN_PRIORITY);
			myselfThread.start();
		}
	}
	
	
	@Override
	public void run() {		
		for (;;) {
			try {
				logger.info("Run FTP maxtrack positions xml daemon!");
				if(SecurityKey.check()){
					if(getConnectedFTPClient().isConnected()){
						//ftp.enterLocalPassiveMode();
						logger.info("Fazendo download dos arquivos de posições");
						downloadFtpPositions(getConnectedFTPClient());
						logger.info("Fazendo upload dos arquivos de comando");
						uploadFtpComands(getConnectedFTPClient());
						logger.info("Fazendo download dos arquivos de resposta dos comandos");
						downloadFtpComandsResponse(getConnectedFTPClient());
					}
					
				}else{
					logger.error("A permissão de uso do software expirou!");
				}
				
			} catch (Throwable e) {
				
				logger.error("Erro na thread FTP de posicoes xml", e);

			} finally {
				try {
					int timer = Integer.parseInt(getMaxtrackProperties().getProperty("xml_read_time"));
					Thread.sleep(timer);
				} catch (Exception e1) {
					logger.error("Erro colocando a thread FTP de posicoes xml para dormir ", e1);
				}
			}
		}
		
	}
	
	private FTPClient getConnectedFTPClient() throws SocketException, IOException{
		if(ftp == null)
			ftp = new FTPClient();
		if(!ftp.isConnected()){
			logger.info("Conectando com o servidor de ftp");
			ftp.connect(getFtpProperties().getProperty("server"));
			logger.info("Logando com o servidor de ftp");
			if(!ftp.login(getFtpProperties().getProperty("user"), getFtpProperties().getProperty("password")))
			{
				ftp.logout();
				logger.error("Não foi possivel logar no servidor de FTP");
			}
			logger.info("Verificando conexão com o servidor de ftp");
			int reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply))
			{
				ftp.disconnect();
				logger.error("Impossivel estabelecer conexão com o servidor de FTP");
			}
			logger.info("Conectado com o servidor de ftp");
		}
		
		ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
		ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
		
		return ftp;
	}
	
	private void downloadFtpPositions(FTPClient ftp) throws IOException{
		ftp.changeWorkingDirectory(getFtpProperties().getProperty("positions_path"));
		logger.info("FTP path: " + ftp.printWorkingDirectory());
        FTPFile[] ftpFiles = ftp.listFiles();
        if (ftpFiles != null && ftpFiles.length > 0) {
            for (FTPFile file : ftpFiles) {
                if (!file.isFile()) {
                    continue;
                }
                logger.info("Arquivo: " + file.getName());
                OutputStream output;
                String pathFile = getMaxtrackProperties().getProperty("xml_data") + file.getName();
                File localfile = new File(pathFile + ".tmp");
                output = new FileOutputStream(localfile);
                ftp.retrieveFile(file.getName(), output);
                output.close();
                //RENAME FILE
                File toBeRenamed = new File(pathFile + ".tmp");
    			toBeRenamed.renameTo(new File(pathFile));
    			
    			//DELETE FTP FILE
    			ftp.deleteFile(file.getName());
            }
        }
	}
	
	private void uploadFtpComands(FTPClient ftp) throws IOException{
		ftp.changeWorkingDirectory(getFtpProperties().getProperty("comands_path"));
		logger.info("FTP path: " + ftp.printWorkingDirectory());
		File directory = new File(getMaxtrackProperties().getProperty("xml_commands"));
		File[] files = FileTypeUtil.getCMDFiles(directory);
		if(files == null || files.length == 0){
			return;
		}
		for(File file: files){
			FileInputStream in = new FileInputStream(file);
			ftp.storeFile(file.getName(), in);
			in.close();
			file.delete();	
		}
	}
	
	private void downloadFtpComandsResponse(FTPClient ftp) throws IOException{
		ftp.changeWorkingDirectory(getFtpProperties().getProperty("comands_response_path"));
		logger.info("FTP path: " + ftp.printWorkingDirectory());
		FTPFile[] ftpFiles = ftp.listFiles();
        if (ftpFiles != null && ftpFiles.length > 0) {
            for (FTPFile file : ftpFiles) {
                if (!file.isFile()) {
                    continue;
                }
                logger.info("Arquivo: " + file.getName());
                OutputStream output;
                String pathFile = getMaxtrackProperties().getProperty("xml_commands_response") + file.getName();
                File localfile = new File(pathFile + ".tmp");
                output = new FileOutputStream(localfile);
                ftp.retrieveFile(file.getName(), output);
                output.close();
                //RENAME FILE
                File toBeRenamed = new File(pathFile + ".tmp");
    			toBeRenamed.renameTo(new File(pathFile));
    			
    			//DELETE FTP FILE
    			ftp.deleteFile(file.getName());
    			
            }
        }
	}
	
	
	private Properties getMaxtrackProperties() throws IOException{
		if(maxtrackProperties == null){
			maxtrackProperties = new Properties();
			maxtrackProperties.load(App.class.getClassLoader().getResourceAsStream(MAXTRACK_FOLDER_PROPERTIES));
		}
		return maxtrackProperties;
	}
	
	private Properties getFtpProperties() throws IOException{
		if(ftpProperties == null){
			ftpProperties = new Properties();
			ftpProperties.load(App.class.getClassLoader().getResourceAsStream(FTP_PROPERTIES));
		}
		return ftpProperties;
	}

}
