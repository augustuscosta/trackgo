package br.com.otgmobile.server;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scala.App;
import br.com.otgmobile.service.maxtrack.MaxtrackService;
import br.com.otgmobile.util.SecurityKey;

@Service
public class MaxtrackCommandsDaemon extends Thread{

	private static final Logger logger = Logger.getLogger(MaxtrackCommandsDaemon.class);

	private Thread myselfThread = null;
	
	private Properties properties;
	private static final String MAXTRACK_FOLDER_PROPERTIES = "maxtrack.folders.properties";

	@Autowired
	private MaxtrackService maxtrackService;

	@Override
	public void run() {
		
		
		for (;;) {
			try {
				logger.info("Run maxtrack commands xml daemon!");
				
				if(SecurityKey.check()){
					maxtrackService.runCommandWriteService();
				}else{
					logger.info("A permissão de uso do software expirou!");
				}
				
			} catch (Throwable e) {
				
				logger.error("Erro na thread de escrita de comandos xml", e);

			} finally {
				try {
					int timer = Integer.parseInt(getMaxtrackProperties().getProperty("xml_read_time"));
					Thread.sleep(timer);
				} catch (Exception e1) {
					logger.error("Erro colocando a thread de escrita de comandos xml para dormir ", e1);
				}
			}
		}
		
	}

	public void start() {
		if (myselfThread == null) {
			myselfThread = new Thread(this);
			myselfThread.setPriority(Thread.MIN_PRIORITY);
			myselfThread.start();
		}
	}
	
	private Properties getMaxtrackProperties() throws IOException{
		if(properties == null){
			properties = new Properties();
			properties.load(App.class.getClassLoader().getResourceAsStream(MAXTRACK_FOLDER_PROPERTIES));
		}
		return properties;
	}
}
