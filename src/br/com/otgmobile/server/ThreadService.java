package br.com.otgmobile.server;

public interface ThreadService {

	void runLocationReadService() throws Exception;
	
	void runCommandWriteService() throws Exception;
	
	void runCommandResponseReadService() throws Exception;
}
