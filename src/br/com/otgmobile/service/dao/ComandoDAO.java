package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.maxtrack.Comando;
import br.com.otgmobile.model.maxtrack.ComandoEstado;

@Component
public class ComandoDAO extends AbstractDAO { 

	@PersistenceContext
	private EntityManager entityManager;
	
	public Comando find(Comando comando) {
		Comando toReturn = (Comando) createCriteria(entityManager, Comando.class)
				.add(Restrictions.eq("idCommand", comando.getIdCommand())).uniqueResult();
		return toReturn;
	}
	
	public Comando find(ComandoEstado comandoEstado) {
		Comando toReturn = (Comando) createCriteria(entityManager, Comando.class)
				.add(Restrictions.eq("idCommand", comandoEstado.getIdCommand()))
				.add(Restrictions.eq("serial", comandoEstado.getSerial()))
				.add(Restrictions.eq("sent", true))
				.add(Restrictions.isNull("estado"))
				.setMaxResults(1)
				.uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Comando> getAllComandos(){
		List<Comando> toReturn = (List<Comando>) createCriteria(entityManager, Comando.class).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Comando> getAllNotSentComandos(){
		List<Comando> toReturn = (List<Comando>) createCriteria(entityManager, Comando.class)
				.add(Restrictions.eq("sent", false))
				.list();
		return toReturn;
	}
	
	public void add(Comando comando){
		entityManager.persist(comando);
	}
	
	public void update(Comando comando){
		entityManager.merge(comando);
	}
	
	public void delete(Comando comando){
		entityManager.remove(comando);
	}
}

