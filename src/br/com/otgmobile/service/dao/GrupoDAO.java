package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Grupo;

@Component
public class GrupoDAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Grupo> getGruposFromDevice(Long id){
		List<Grupo> grupos = createCriteria(entityManager, Grupo.class).createAlias("devices", "d").
				add(Restrictions.eq("d.id", id)).list();
		return grupos;
	}

	

}
