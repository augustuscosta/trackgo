package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.niagarahw06.NiagaraHW06message;

@Component
public class NiagaraHw6DAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public NiagaraHW06message getLastPosition(Device device) {
		NiagaraHW06message toReturn = (NiagaraHW06message) createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.eq("productserialnumber", device.getCode()))
				.addOrder(Order.desc("messageDate")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<NiagaraHW06message> getPositions(DevicePathCriteria devicePath) {
		List<NiagaraHW06message> toReturn = createCriteria(entityManager, NiagaraHW06message.class)
				.add(Restrictions.between("messageDate", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("productserialnumber", devicePath.getDevice().getCode())).list();
		return toReturn;
	}
	
	public void insertPosition(NiagaraHW06message position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from NiagaraHW06message");
		query.executeUpdate();
	}
	
}

