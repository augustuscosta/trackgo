package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.skypatrol.SkypatrolMessage;

@Component
public class SkypatrolDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public SkypatrolMessage getLastPosition(Device device) {
		SkypatrolMessage toReturn = (SkypatrolMessage) createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.eq("uniqueID", device.getCode()))
				.addOrder(Order.desc("gpsUtcTime")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<SkypatrolMessage> getPositions(DevicePathCriteria devicePath) {
		List<SkypatrolMessage> toReturn = createCriteria(entityManager, SkypatrolMessage.class)
				.add(Restrictions.between("gpsUtcTime", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("uniqueID", devicePath.getDevice().getCode())).list();
		return toReturn;
	}
	
	public void insertPosition(SkypatrolMessage position){
		entityManager.persist(position);
	}
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from SkypatrolMessage");
		query.executeUpdate();
	}
	
}

