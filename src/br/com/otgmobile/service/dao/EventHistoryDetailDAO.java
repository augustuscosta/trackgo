package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.events.EventHistoryDetail;

@Component
public class EventHistoryDetailDAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAllnotSent() {
		List<EventHistoryDetail> toReturn = createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("sent", false)).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventHistoryDetail> fetchAllSent() {
		List<EventHistoryDetail> toReturn = createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("sent", false)).list();
		return toReturn;
	}
	
	public void add(EventHistoryDetail eventHistoryDetail){
		entityManager.persist(eventHistoryDetail);
	}
	
	public void add(List<EventHistoryDetail> eventHistoryDetails){
		if(eventHistoryDetails == null || eventHistoryDetails.isEmpty()) return ;
		
		for(EventHistoryDetail detail :eventHistoryDetails){
			add(detail);			

		}
	}
	
	public void merge(EventHistoryDetail eventHistoryDetail){
		entityManager.merge(eventHistoryDetail);
	}
	
	public void delete(EventHistoryDetail eventHistoryDetail){
		entityManager.detach(eventHistoryDetail);
	}

	public EventHistoryDetail find(long eventHistoryDetailId) {
		EventHistoryDetail toReturn = (EventHistoryDetail) createCriteria(entityManager, EventHistoryDetail.class)
				.add(Restrictions.eq("id", eventHistoryDetailId))
				.uniqueResult();
		return toReturn;
	}

}
