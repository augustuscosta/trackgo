package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.events.EventHistory;

@Component
public class EventHistoryDAO extends AbstractDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<EventHistory> fetchAllnotSent() {
		List<EventHistory> toReturn = createCriteria(entityManager, EventHistory.class)
				.add(Restrictions.eq("sent", false)).list();
		return toReturn;
	}	
	
	@SuppressWarnings("unchecked")
	public List<EventHistory> fectchAllSent() {
		List<EventHistory> toReturn = createCriteria(entityManager, EventHistory.class)
				.add(Restrictions.eq("sent", true)).list();
		return toReturn;
	}	
	
	public void add(EventHistory eventHistory){
		entityManager.persist(eventHistory);
	}
	
	public void merge(EventHistory eventHistory){
		entityManager.merge(eventHistory);
	}
	
	public void delete(EventHistory eventHistory){
		entityManager.detach(eventHistory);
	}

}
