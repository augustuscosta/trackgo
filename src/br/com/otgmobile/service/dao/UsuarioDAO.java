package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Usuario;

@Component
public class UsuarioDAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuariosFromGrupo(Long id){
		List<Usuario> usuarios = createCriteria(entityManager, Usuario.class).createAlias("grupos", "g").
				add(Restrictions.eq("g.id", id)).list();
		return usuarios;
	}

	

}
