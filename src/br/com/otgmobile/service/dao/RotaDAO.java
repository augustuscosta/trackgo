package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.GeoPontoRota;
import br.com.otgmobile.model.Rota;

@Component
public class RotaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Rota find(Rota rota){
		Rota toReturn = (Rota) createCriteria(entityManager, Rota.class).add(Restrictions.eq("id", rota.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Rota> getAllRotas(){
		List<Rota> toReturn = (List<Rota>) createCriteria(entityManager, Rota.class).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeoPontoRota> getAllGeoPoints(Integer id){
		List<GeoPontoRota> toReturn = (List<GeoPontoRota>) createCriteria(entityManager, GeoPontoRota.class).createAlias("rota", "c")
			    .add( Restrictions.eq("c.id", id) ).list();
		return toReturn;
	}
	
	public void add(Rota rota){
		entityManager.persist(rota);
	}
	
	public void update(Rota rota){
		entityManager.merge(rota);
	}
	
	public void delete(Rota rota){
		entityManager.remove(rota);
	}
}

