package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.maxtrack.Position;

@Component
public class MaxtrackDAO extends AbstractDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	public synchronized Position getLastPosition(Device device) {
		Position toReturn = (Position) createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.eq("serial", device.getCode()))
				.addOrder(Order.desc("gps.date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized List<Position> getPositions(DevicePathCriteria devicePath) {
		List<Position> toReturn = createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.between("gps.date", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("serial", devicePath.getDevice().getCode())).list();
		return toReturn;
	}
	
	public synchronized void insertPosition(Position position){
		entityManager.persist(position);
	}
	
	public synchronized void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from Position");
		query.executeUpdate();
	}

	public synchronized Position getLastPosition(Device device, Position position) {
		Position toReturn = (Position) createCriteria(entityManager, Position.class)
				.createAlias("gps", "gps")
				.add(Restrictions.eq("serial", device.getCode()))
				.add(Restrictions.not(Restrictions.eq("id", position.getId())))
				.addOrder(Order.desc("gps.date"))
				.addOrder(Order.desc("id")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
}

