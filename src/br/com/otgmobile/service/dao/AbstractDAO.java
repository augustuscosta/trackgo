package br.com.otgmobile.service.dao;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;

public abstract class AbstractDAO {
	
	public Criteria createCriteria(EntityManager entityManager, Class<?> clazz) {
		return ((Session) entityManager.getDelegate()).createCriteria(clazz);
	}

}
