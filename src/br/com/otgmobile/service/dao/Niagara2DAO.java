package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.niagara2.Niagara2Message;

@Component
public class Niagara2DAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public Niagara2Message getLastPosition(Device device) {
		Niagara2Message toReturn = (Niagara2Message) createCriteria(entityManager, Niagara2Message.class)
				.add(Restrictions.eq("uniqueUnitIdentifier", device.getCode()))
				.addOrder(Order.desc("generationTimeStamp")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Niagara2Message> getPositions(DevicePathCriteria devicePath) {
		List<Niagara2Message> toReturn = createCriteria(entityManager, Niagara2Message.class)
				.add(Restrictions.between("generationTimeStamp", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("uniqueUnitIdentifier", devicePath.getDevice().getCode())).list();
		return toReturn;
	}	
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from Niagara2Message");
		query.executeUpdate();
	}

}
