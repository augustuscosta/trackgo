package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;

@Component
public class DeviceDAO extends AbstractDAO { 

	@PersistenceContext
	private EntityManager entityManager;
	
	public synchronized Device find(Device device) {
		Device toReturn = (Device) createCriteria(entityManager, Device.class)
				.add(Restrictions.eq("id", device.getId())).uniqueResult();
		return toReturn;
	}
	
	public synchronized Device find(String code) {
		Device toReturn = (Device) createCriteria(entityManager, Device.class)
				.add(Restrictions.eq("code", code)).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized List<Device> getAllDevices(){
		List<Device> toReturn = (List<Device>) createCriteria(entityManager, Device.class).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized List<Device> getAllEnabledDevices(){
		List<Device> toReturn = (List<Device>) createCriteria(entityManager, Device.class)
				.add(Restrictions.eq("enabled", true))
				.list();
		return toReturn;
	}
	
	public synchronized void add(Device device){
		entityManager.persist(device);
	}
	
	public synchronized void update(Device device){
		entityManager.merge(device);
	}
	
	public void delete(Device device){
		entityManager.remove(device);
	}
}

