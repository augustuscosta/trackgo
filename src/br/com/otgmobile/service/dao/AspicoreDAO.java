package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DevicePathCriteria;
import br.com.otgmobile.model.aspicore.AspicoreMessage;

@Component
public class AspicoreDAO extends AbstractDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public AspicoreMessage getLastPosition(Device device) {
		AspicoreMessage toReturn = (AspicoreMessage) createCriteria(entityManager, AspicoreMessage.class)
				.createAlias("gps", "gps")
				.add(Restrictions.eq("serial", device.getCode()))
				.addOrder(Order.desc("gps.date")).setMaxResults(1).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<AspicoreMessage> getPositions(DevicePathCriteria devicePath) {
		List<AspicoreMessage> toReturn = createCriteria(entityManager, AspicoreMessage.class)
				.createAlias("gps", "gps")
				.add(Restrictions.between("gps.date", devicePath.getStart(), devicePath.getEnd()))
				.add(Restrictions.eq("serial", devicePath.getDevice().getCode())).list();
		return toReturn;
	}	
	
	public void deleteAllPositions(){
		Query query = entityManager.createQuery("delete from AspicoreMessage");
		query.executeUpdate();
	}

}
