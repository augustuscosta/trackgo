package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Cerca;
import br.com.otgmobile.model.GeoPontoCerca;


@Component
public class CercaDAO extends AbstractDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Cerca find(Cerca cerca){
		Cerca toReturn = (Cerca) createCriteria(entityManager, Cerca.class).add(Restrictions.eq("id", cerca.getId())).uniqueResult();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cerca> getAllCercas(){
		List<Cerca> toReturn = (List<Cerca>) createCriteria(entityManager, Cerca.class).list();
		return toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeoPontoCerca> getAllGeoPoints(Integer id){
		List<GeoPontoCerca> toReturn = (List<GeoPontoCerca>) createCriteria(entityManager, GeoPontoCerca.class).createAlias("cerca", "c")
			    .add( Restrictions.eq("c.id", id) ).list();
		return toReturn;
	}
	
	public void add(Cerca cerca){
		entityManager.persist(cerca);
	}
	
	public void update(Cerca cerca){
		entityManager.merge(cerca);
	}
	
	public void delete(Cerca cerca){
		entityManager.remove(cerca);
	}
}

