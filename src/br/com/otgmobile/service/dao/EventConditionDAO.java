package br.com.otgmobile.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Cerca;
import br.com.otgmobile.model.Rota;
import br.com.otgmobile.model.events.EventCondition;

@Component
public class EventConditionDAO extends AbstractDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<EventCondition> getEventConditionsFromEvent(Long id){
		List<EventCondition> conditions = createCriteria(entityManager, EventCondition.class)
				.add(Restrictions.eq("event.id", id)).list();
		return conditions;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cerca> getCercasFromEventConditions(Long id){
		List<Cerca> cercas = createCriteria(entityManager, Cerca.class)
			    .createAlias("eventConditions", "c")
			    .add( Restrictions.eq("c.id", id) )
			    .list();
		return cercas;
	}

	@SuppressWarnings("unchecked")
	public List<Rota> getRotasFromEventConditions(Long id){
		List<Rota> rotas = createCriteria(entityManager, Rota.class)
			    .createAlias("eventConditions", "c")
			    .add( Restrictions.eq("c.id", id) )
			    .list();
		return rotas;
	}

}
