package br.com.otgmobile.service.skypatrol;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.skypatrol.SkypatrolMessage;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.SkypatrolDAO;

@Service
public class SkypatrolServiceImpl implements SkypatrolService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	EventProcesser eventProcesser;
	
	@Override
	public void salva(SkypatrolMessage message) {
		DeviceDAO dao = new DeviceDAO();
		Device device = dao.find(message.getCode());
		SkypatrolMessage oldMessage = new SkypatrolDAO().getLastPosition(device);
		entityManager.persist(message);
		eventProcesser.processEvent(message, device, oldMessage);
	}

	@Override
	public void save(Message message) {
		entityManager.persist(message);
	}

	
}
