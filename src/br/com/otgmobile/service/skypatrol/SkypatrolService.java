package br.com.otgmobile.service.skypatrol;

import org.springframework.stereotype.Service;

import br.com.otgmobile.model.skypatrol.SkypatrolMessage;

@Service
public interface SkypatrolService extends br.com.otgmobile.server.Service {
	
	public abstract void salva(SkypatrolMessage message);
	
}
