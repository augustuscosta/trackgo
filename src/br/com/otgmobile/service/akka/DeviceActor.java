package br.com.otgmobile.service.akka;

import akka.actor.UntypedActor;
import br.com.otgmobile.SpringLauncher;
import br.com.otgmobile.config.Spring;
import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.maxtrack.Position;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.MaxtrackDAO;

/**
 * Será criado um ator para cada carro
 * 
 */
public class DeviceActor extends UntypedActor {
	
	private MaxtrackDAO maxtrackDAO;
	
	private DeviceDAO deviceDAO;
	
	private EventProcesser processer;
	
	//private static final Logger logger = Logger.getLogger(DeviceActor.class); 
	
	private Long cont = 0l;
	
	private Device device;
	
	private Position lastPosition;

	@Override
	public void onReceive(Object msg) throws Exception {
		if(msg instanceof Position){
			setUpBeans();
			//Aqui se coloca a logica de negocio que pode ser processada em paralelo.
			++cont;
			Position message = (Position)msg;
			//nesse print... pode verificar o nome das Threads que estão rodando... e verificar que é usada a mesma Thread para varios atores.
			//o framework tem um pool de threads. e vai criando a media que vai precisando.
			//logger.info(Thread.currentThread().getName() +  " | Atualizando: dispositivo:"+ message.getSerial() + " - cont -> " + cont);
			System.out.println(Thread.currentThread().getName() +  " | Atualizando: dispositivo:"+ message.getSerial() + " - cont -> " + cont);
			maxtrackDAO.insertPosition(message);
			//logger.info(Thread.currentThread().getName() +  " | Processando evento: dispositivo:"+ message.getSerial() + " - cont -> " + cont);
			System.out.println(Thread.currentThread().getName() +  " | Processando evento: dispositivo:"+ message.getSerial() + " - cont -> " + cont);
			processEvent(message);
		}

	}
	
	private void processEvent(Position position){
		if(device == null)
			device = deviceDAO.find(position.getCode());
		if(device == null)
			return;
		if(lastPosition == null)
			lastPosition = maxtrackDAO.getLastPosition(device,position);
		processer.processEvent(position, device, lastPosition);
		lastPosition = position;
	}
	
	private void setUpBeans(){
		Spring spring = SpringLauncher.getSpring();
		maxtrackDAO = spring.getMaxtrackDAO();
		deviceDAO = spring.getDeviceDAO();
		processer = spring.getEventProcesser();
	}

}
