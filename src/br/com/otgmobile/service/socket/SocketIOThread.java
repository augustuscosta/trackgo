package br.com.otgmobile.service.socket;

import java.util.concurrent.Semaphore;

import com.github.nkzawa.socketio.client.Socket;

public class SocketIOThread implements Runnable {

	private Socket socket;
	final private Semaphore semaphore;
	
	public SocketIOThread(Socket socket, Semaphore semaphore) {
		this.socket = socket;
		this.semaphore = semaphore;
	}
	
	@Override
	public void run() {
		if (socket != null) {

			try {
				semaphore.acquire();
			} catch (InterruptedException e2) {

			}
			
	    	Boolean conectou = false;
	   	
	    	while (!conectou) {  		
    		
				try {
					System.out.println("Tentando conectar com o SocketIO atravás da Thread");
					socket.connect();
					conectou = true;
				} catch (Exception e) {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						break;
					}
					System.out.println("Erro conectando com o SocketIO atravás da Thread");
					conectou = false;
				}					   		
	    	}	
	    	
	    	semaphore.release();
		}
	}

}
