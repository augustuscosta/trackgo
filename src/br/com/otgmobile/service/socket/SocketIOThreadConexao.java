package br.com.otgmobile.service.socket;

import java.util.concurrent.Semaphore;

public class SocketIOThreadConexao implements Runnable {

	final private Semaphore semaphore;
	
	public SocketIOThreadConexao(final  Semaphore semaphore) {
		this.semaphore = semaphore;
	}
	
	@Override
	public void run() {
		
		try {
			semaphore.acquire();
		} catch (InterruptedException e2) {

		}
		
		semaphore.release();

	}

}
