package br.com.otgmobile.service.socket;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.model.events.EventHistoryDetail;
import br.com.otgmobile.model.events.EventOcurred;
import br.com.otgmobile.service.dao.EventHistoryDetailDAO;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Service
public class SocketIOTask {

	private static final String ROOT_OBJECT = "result";
	private static final Logger logger = Logger.getLogger(SocketIOTask.class);
	private Socket socket;

	@Autowired
	EventHistoryDetailDAO eventHistoryDAO;
	

	public static final String EVENTS = "events/event_history";

	private static final String URL = "http://127.0.0.1:9090";

	private static final int WAIT_TIME = 30000;

	private Long lastCheckedTime;

	public SocketIOTask() {
	}

	public void start() {
		
		try {
			socket = IO.socket(URL);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		prepareSocket();
		connect();
		for(;;){
			if(lastCheck()){
				sendAllHistoryDetails();					
			}
		}
	}

	private boolean lastCheck() {
		long currentTime = new Date().getTime();
		if(lastCheckedTime == null){
			lastCheckedTime = new Date().getTime();
			return true;
		}

		if((lastCheckedTime+WAIT_TIME) < (currentTime)){
			lastCheckedTime = currentTime;
			return true;
		}

		return false;
	}

	private void sendAllHistoryDetails() {
		List<EventHistoryDetail> eventsHistoryDetails = eventHistoryDAO.fetchAllnotSent();
		for(EventHistoryDetail eventHistory : eventsHistoryDetails){
			eventHistory = eventHistoryDAO.find(eventHistory.getId());
			EventOcurred eventOcurred = new EventOcurred(eventHistory);
			enviaMsg(EVENTS, eventOcurred);
		}

	}


	public boolean enviaMsg(String metodo, JSONObject msg) {
		socket.emit(metodo, msg);
		return true;
	} 
	
	public boolean enviaComWraper(EventHistoryDetail eventHistoryDetail){
		return enviaMsg(SocketIOTask.EVENTS, new EventOcurred(eventHistoryDetail));
	}

	public boolean enviaMsg(String metodo, Object objeto) {

		JSONObject jsonNode;
		JSONObject jsonResult = new JSONObject();

		try {
			Gson gson = new Gson();
			JsonElement je = gson.toJsonTree(objeto);	    
			jsonNode = new JSONObject(je.toString());
			jsonResult.put(ROOT_OBJECT, jsonNode);
			
			socket.emit(metodo, jsonResult);

			return true;

		} catch (Exception e) {			
			logger.error("Erro na thread do socket.io",e);
			return false;
		} 
	}
	
	public void connect() {
    	System.out.println("Conectando com o SocketIO"); 
    	socket.connect();
    }
    
    public void open() {
    	System.out.println("Abrindo conexção com o SocketIO"); 
    	socket.open();
    }
    
    public void conectou(){
    	System.out.println("Conectou com SocketIO");
    }
	
	private void prepareSocket(){
		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			conectou();
    		}

    	});
    	
    	socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Caiu a conexção com o SocketIO"); 
				open();
    		}

    	});
    	
    	socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Erro de conexção SocketIO"); 
				open();
    		}

    	});
    	
    	socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Time out SocketIO"); 
				open();
    		}

    	});

    	socket.on(EVENTS, new Emitter.Listener() {

		  @Override
		  public void call(Object... args) {
			  JSONObject jsonObject =  null;
				logger.info("Received Event: "+EVENTS);
				try {
					for (int i = 0; i < args.length; i++) {
						jsonObject = new JSONObject(args[i].toString());
						Gson gson = new Gson();
						EventOcurred eventOcurred = gson.fromJson(jsonObject.getString(ROOT_OBJECT), EventOcurred.class);
						EventHistoryDetail eventHistory = eventHistoryDAO.find(eventOcurred.getEventHistoryDetailId());
						eventHistory.setSent(true);
						eventHistoryDAO.merge(eventHistory);			    		
					}
				} catch (Exception e) {
					logger.error("erro ao pegar json do evento", e);
				}
		  }

    	});
	}

}
