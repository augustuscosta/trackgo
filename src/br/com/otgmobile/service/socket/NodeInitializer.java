package br.com.otgmobile.service.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import scala.App;

@Service
public class NodeInitializer {
	private static final String NODE_STARTUP_PROPERTIES = "node.startup.properties";
	private static final Logger logger = Logger.getLogger(NodeInitializer.class);	
	private Process nodeProcess;
	ProcessBuilder processBuilder;

	public void start(){
		try {
			Properties props = new Properties();
			props.load(App.class.getClassLoader().getResourceAsStream(NODE_STARTUP_PROPERTIES));
			List<String> commands = new ArrayList<String>();
			commands.add(props.getProperty("nodepath"));
			commands.add(props.getProperty("scriptpath"));
			processBuilder = new ProcessBuilder(commands);
			nodeProcess = processBuilder.start();
			while(nodeProcess != null){
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(nodeProcess.getInputStream()));
				String interfaceName = stdInput.readLine();//reading the output
				logger.info(interfaceName);
			}
		} catch (IOException e) {
			logger.error(e.getCause(), e);
		}
	}

}
