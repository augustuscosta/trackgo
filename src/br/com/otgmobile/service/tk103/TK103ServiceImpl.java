 package br.com.otgmobile.service.tk103;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.tk10x.TK10XMessage;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.Tk10xDAO;

@Service
public class TK103ServiceImpl implements TK103Service {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	EventProcesser eventProcesser;
 
	@Override
	public void salva(TK10XMessage message) {
		DeviceDAO dao = new DeviceDAO();
		Device device = dao.find(message.getCode());
		TK10XMessage oldMessage = new Tk10xDAO().getLastPosition(device);
		entityManager.persist(message);
		eventProcesser.processEvent(message, device, oldMessage);
	}

	@Override
	public void save(Message message) {
		entityManager.persist(message);
	}

}
