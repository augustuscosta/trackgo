package br.com.otgmobile.service.tk103;

import org.springframework.stereotype.Service;

import br.com.otgmobile.model.tk10x.TK10XMessage;

@Service
public interface TK103Service extends br.com.otgmobile.server.Service {
	
	public abstract void salva(TK10XMessage message);
	
}
