package br.com.otgmobile.service.maxtrack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scala.App;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import br.com.otgmobile.model.maxtrack.Comando;
import br.com.otgmobile.model.maxtrack.ComandoEstado;
import br.com.otgmobile.model.maxtrack.Comandos;
import br.com.otgmobile.model.maxtrack.Position;
import br.com.otgmobile.model.maxtrack.Positions;
import br.com.otgmobile.service.akka.DeviceActor;
import br.com.otgmobile.service.dao.ComandoDAO;
import br.com.otgmobile.util.FileComparator;
import br.com.otgmobile.util.FileTypeUtil;
import br.com.otgmobile.util.StringUtil;

@Service
public class MaxtrackServiceImpl implements MaxtrackService {

	private static final Logger logger = Logger.getLogger(MaxtrackServiceImpl.class);
	private static final String MAXTRACK_FOLDER_PROPERTIES = "maxtrack.folders.properties";

	private JAXBContext jaxbContextPosition;
	private Unmarshaller unmarshallerPosition;
	
	private JAXBContext jaxbContextCommand;
	private Marshaller marshallerCommand;
	
	private JAXBContext jaxbContextCommandReport;
	private Unmarshaller unmarshallerCommandReport;
	
	@Autowired
	private ComandoDAO comandoDAO;

	private ActorSystem system;
	private Map<String,ActorRef> mapDevices;
	private Properties properties;

	@Override
	public void runLocationReadService() throws Exception {
		String xmlPath = getMaxtrackProperties().getProperty("xml_data");
		if(!StringUtil.isValid(xmlPath)){
			logger.error("Caminho da pasta de posições xmls não configurado");
			return;
		}
		logger.info("Iniciando a leitura. Caminho da pasta: " + xmlPath + " data: " + new Date().toString());
		File directory = new File(xmlPath);
		File[] files = FileTypeUtil.getXMLFiles(directory);
		if(files == null || files.length == 0){
			logger.info("Nenhum novo arquivo encontrado.");
			return;
		}
		logger.info(files.length + " Arquivos encontrados.");
		checkActorsState();
		Arrays.sort(files,new FileComparator());
		for(File file: files){
			List<Position> positions = getPositionListFrom(file);
			for(Position position: positions){
				ActorRef deviceActor = null;
				
				if(isValidPosition(position)){
					//verificando se o ator já existe ou não
					if(mapDevices.containsKey(position.getSerial())){
						//identificando o ator existente
						deviceActor = mapDevices.get(position.getSerial());
					}else{
						//criando um novo ator.
						deviceActor = (ActorRef) system.actorOf(new Props(DeviceActor.class),position.getSerial());
						//colocando o novo ator no mapa.
						mapDevices.put(position.getSerial(), deviceActor);
					}
				}else{
					logger.error("A posição para o dispositivo: " + position.getSerial() + "não é válida.");
				}
				//Enviando a msg para o ator.
				if(deviceActor!= null)
					deviceActor.tell(position);
			}
			file.delete();
		}
	}
	
	private boolean isValidPosition(Position position) {
		if(position.getDate().getTime() > new Date().getTime())
			return false;
		
		return true;
	}

	@Override
	public void runCommandWriteService() throws Exception {
		String xmlPath = getMaxtrackProperties().getProperty("xml_commands");
		if(!StringUtil.isValid(xmlPath)){
			logger.error("Caminho da pasta de comandos xmls não configurado");
			return;
		}
		String tempXmlPath = getMaxtrackProperties().getProperty("temp_xml_commands");
		if(!StringUtil.isValid(tempXmlPath)){
			logger.error("Caminho da pasta de comandos xmls temporarios não configurado");
			return;
		}
		List<Comando> comandos = comandoDAO.getAllNotSentComandos();
		if(comandos == null || comandos.size() == 0){
			logger.info("Nenhum comando para escrever.");
			return;
		}
		Comandos comandosWrapper = new Comandos();
		List<Comando> toWrapper;
		String pathFile;
		for (Comando comando : comandos) {
			toWrapper = new ArrayList<Comando>();
			toWrapper.add(comando);
			comandosWrapper.setComandos(toWrapper);
			pathFile = xmlPath + comando.getFileName();
			FileWriter file = new FileWriter(pathFile + ".tmp");
			try {
				getCommandsJaxbMarshaller().marshal(comandosWrapper, file);
			} finally {
				file.close();
			}
			File toBeRenamed = new File(pathFile + ".tmp");
			toBeRenamed.renameTo(new File(pathFile));
			comando.setSent(true);
			comandoDAO.update(comando);
		}
		comandosWrapper.setComandos(comandos);
	}

	@Override
	public void runCommandResponseReadService() throws Exception {
		String xmlPath = getMaxtrackProperties().getProperty("xml_commands_response");
		if(!StringUtil.isValid(xmlPath)){
			logger.error("Caminho da pasta de retorno dos comandos xmls não configurado");
			return;
		}
		File directory = new File(xmlPath);
		File[] files = FileTypeUtil.getXMLFiles(directory);
		if(files == null || files.length == 0){
			logger.info("Nenhum novo arquivo encontrado.");
			return;
		}
		logger.info(files.length + " Arquivos encontrados.");
		for(File file: files){
			ComandoEstado report = getReportFrom(file);
			
			if(save(report));
				file.delete();
			
			deleteCommandFile(report);	
		}
	}
	
	private void deleteCommandFile(ComandoEstado report) throws IOException{
		String commandsXmlPath = getMaxtrackProperties().getProperty("xml_commands");
		if(!StringUtil.isValid(commandsXmlPath)){
			logger.error("Caminho da pasta dos comandos xmls não configurado");
			return;
		}
		if(report == null || report.getIdCommand() == null)
			return;
		Comando comando = new Comando();
		comando.setIdCommand(report.getIdCommand());
		comando = comandoDAO.find(comando);
		if(comando == null)
			return;
		String pathFile = commandsXmlPath + comando.getFileName();
		File file = new File(pathFile);
		if(file.exists())
			file.delete();
	}

	private boolean save(ComandoEstado report) {
		Comando comando = comandoDAO.find(report);
		if(comando == null)
			return false;
		comando.setEstado(report);
		comandoDAO.update(comando);
		return true;
	}

	private ComandoEstado getReportFrom(File file) throws JAXBException {
		Unmarshaller um = getCommandsReportJaxbUnmarshaller();
		ComandoEstado comandoEstado = (ComandoEstado) um.unmarshal(file);
		return comandoEstado;
	}
	

	private List<Position> getPositionListFrom(File file) throws JAXBException{
		logger.info("Processando o arquivo: " + file.getName());
		Unmarshaller um = getPositionsJaxbUnmarshaller();
		Positions positions = (Positions) um.unmarshal(file);
		if(positions != null){
			return positions.getPositions();
		}
		return null;
	}

	private JAXBContext getPositionsJaxbContext() throws JAXBException{
		if(jaxbContextPosition == null){
			jaxbContextPosition = JAXBContext.newInstance(new Class[] {Positions.class});
		}
		return jaxbContextPosition;
	}

	private Unmarshaller getPositionsJaxbUnmarshaller() throws JAXBException{
		if(unmarshallerPosition == null){
			unmarshallerPosition = getPositionsJaxbContext().createUnmarshaller();
		}
		return unmarshallerPosition;
	}
	
	private JAXBContext getCommandsJaxbContext() throws JAXBException{
		if(jaxbContextCommand == null){
			jaxbContextCommand = JAXBContext.newInstance(new Class[] {Comandos.class});
		}
		return jaxbContextCommand;
	}

	private Marshaller getCommandsJaxbMarshaller() throws JAXBException{
		if(marshallerCommand == null){
			marshallerCommand = getCommandsJaxbContext().createMarshaller();
		}
		return marshallerCommand;
	}
	
	private JAXBContext getCommandsReportJaxbContext() throws JAXBException{
		if(jaxbContextCommandReport == null){
			jaxbContextCommandReport = JAXBContext.newInstance(new Class[] {ComandoEstado.class});
		}
		return jaxbContextCommandReport;
	}

	private Unmarshaller getCommandsReportJaxbUnmarshaller() throws JAXBException{
		if(unmarshallerCommandReport == null){
			unmarshallerCommandReport = getCommandsReportJaxbContext().createUnmarshaller();
		}
		return unmarshallerCommandReport;
	}
	
	public  void moveFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;
	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();

	        // previous code: destination.transferFrom(source, 0, source.size());
	        // to avoid infinite loops, should be:
	        long count = 0;
	        long size = source.size();              
	        while((count += destination.transferFrom(source, count, size-count))<size);
	        sourceFile.delete();
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	
	private void checkActorsState(){
		//Sistema de atores em Akka.
		if(system == null)
			system = ActorSystem.create("trackgo");
		
		//mapa que sera guardado os atores, identificados pela placa do carro.
		if(mapDevices == null)
			mapDevices = new HashMap<String, ActorRef>();
	}
	
	private Properties getMaxtrackProperties() throws IOException{
		if(properties == null){
			properties = new Properties();
			properties.load(App.class.getClassLoader().getResourceAsStream(MAXTRACK_FOLDER_PROPERTIES));
		}
		return properties;
	}
}
