package br.com.otgmobile.service.niagara2;

import org.springframework.stereotype.Service;

import br.com.otgmobile.model.niagara2.Niagara2Message;

@Service
public interface Niagra2Service extends br.com.otgmobile.server.Service {
	
	public abstract void salva(Niagara2Message message); 

}
