package br.com.otgmobile.service.niagara2;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.niagara2.Niagara2Message;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.Niagara2DAO;

@Service
public class Niagra2ServiceImpl implements Niagra2Service {
	
	@Autowired
	private DeviceDAO dao;
	
	@Autowired
	EventProcesser eventProcesser;
	
	@Autowired
	private Niagara2DAO niagaraDAO;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Message tracker) {
		entityManager.persist(tracker);
	}

	@Override
	public void salva(Niagara2Message message) {
		Device device = dao.find(message.getUniqueUnitIdentifier());
		Niagara2Message oldMessage = niagaraDAO.getLastPosition(device);
		entityManager.persist(message);
		eventProcesser.processEvent(message, device, oldMessage);
	}
}



