package br.com.otgmobile.service.niagarahw06;

import org.springframework.stereotype.Service;

import br.com.otgmobile.model.niagarahw06.NiagaraHW06message;

@Service
public interface NiagraHw06Service extends br.com.otgmobile.server.Service {
	
	public abstract void salva(NiagaraHW06message message);
	
}
