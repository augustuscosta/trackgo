package br.com.otgmobile.service.niagarahw06;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.niagarahw06.NiagaraHW06message;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.NiagaraHw6DAO;

@Service
public class NiagraHW06ServiceImpl implements NiagraHw06Service {

	@Autowired
	private DeviceDAO dao;
	
	@Autowired
	EventProcesser eventProcesser;
	
	@Autowired
	NiagaraHw6DAO niagaraDHw6dao;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Message tracker) {
		salva((NiagaraHW06message) tracker);
		entityManager.persist(tracker);
	}

	@Override
	public void salva(NiagaraHW06message message) {
		Device device = dao.find(message.getProductserialnumber());
		NiagaraHW06message oldMessage = niagaraDHw6dao.getLastPosition(device);
		eventProcesser.processEvent(message, device, oldMessage);
	}

}
