package br.com.otgmobile.service.aspicore;

import org.springframework.stereotype.Service;

import br.com.otgmobile.model.aspicore.AspicoreMessage;

@Service
public interface AspicoreService extends br.com.otgmobile.server.Service {
	
	public abstract void salva(AspicoreMessage message);
	
}
