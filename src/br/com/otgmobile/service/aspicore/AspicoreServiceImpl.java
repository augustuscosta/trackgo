package br.com.otgmobile.service.aspicore;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.Message;
import br.com.otgmobile.model.aspicore.AspicoreMessage;
import br.com.otgmobile.service.dao.AspicoreDAO;
import br.com.otgmobile.service.dao.DeviceDAO;

@Service
public class AspicoreServiceImpl implements AspicoreService {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void salva(AspicoreMessage message) {
		DeviceDAO dao = new DeviceDAO();
		Device device = dao.find(message.getCode());
		AspicoreMessage oldMessage = new AspicoreDAO().getLastPosition(device);
		entityManager.persist(message);
		EventProcesser eventProcesser = new EventProcesser();
		eventProcesser.processEvent(message, device, oldMessage);
	}

	@Override
	public void save(Message message) {
		entityManager.persist(message);
	}

}
