package br.com.otgmobile.events;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

import scala.App;
import br.com.otgmobile.model.Usuario;
import br.com.otgmobile.model.events.EventHistoryDetail;

@Component
public class MailNotifiyer {

	private static final String COMMA = ",";
	private static final String HTTPS_MAPS_LINK = "https://maps.google.com/maps?q=";
	private static final String CLICK_NO_LINK_PARA_VER_A_LOCALIZAÇÃO = " click no link para ver a localização ";
	private static final String MAIL_AUTH_PROPERTIES = "mail.auth.properties";
	private static final String MAIL_PROPERTIES = "mail.properties";
	private static final String PASSWORD = "password";
	private static final String USERNAME = "username";
	private static String INIT = "O DISPOSITIVO ";
	private static String LAUNCH = " ATIVOU O EVENTO ";

	public  void sendMAilNotification(Usuario usuario,EventHistoryDetail eventHistoryDetail) throws Exception{		
		final Properties emailProperties = new Properties();
		final Properties emailAuthenticationProperties = new Properties();
		loadProperties(emailProperties, emailAuthenticationProperties);
		Session session = setSession(emailProperties,emailAuthenticationProperties);
		Message message = setMessage(usuario, eventHistoryDetail,emailAuthenticationProperties, session);
		Transport.send(message);
	}

	private Message setMessage(Usuario usuario,
			EventHistoryDetail eventHistoryDetail,
			final Properties emailAuthenticationProperties, Session session)
			throws MessagingException, AddressException {
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(emailAuthenticationProperties.getProperty(USERNAME)));
		message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(usuario.getEmail()));
		message.setSubject(eventHistoryDetail.getEvent().getName());
		message.setText(INIT + eventHistoryDetail.getDevice().getName()+ LAUNCH+ 
				eventHistoryDetail.getEvent().getName()+CLICK_NO_LINK_PARA_VER_A_LOCALIZAÇÃO+HTTPS_MAPS_LINK+eventHistoryDetail.getLatitude()+COMMA+eventHistoryDetail.getLongitude());
		return message;
	}

	private void loadProperties(final Properties emailProperties,
			final Properties emailAuthenticationProperties) throws IOException {
		emailProperties.load(App.class.getClassLoader().getResourceAsStream(MAIL_PROPERTIES));
		emailAuthenticationProperties.load(App.class.getClassLoader().getResourceAsStream(MAIL_AUTH_PROPERTIES));
	}

	private Session setSession(final Properties emailProperties,
			final Properties emailAuthenticationProperties) {
		Session session = Session.getDefaultInstance(emailProperties,new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailAuthenticationProperties.getProperty(USERNAME),
						emailAuthenticationProperties.getProperty(PASSWORD));
			}
		});
		return session;
	}
}

