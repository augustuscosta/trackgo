package br.com.otgmobile.events;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ognl.Ognl;
import ognl.OgnlException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.otgmobile.model.Cerca;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.GeoPontoCerca;
import br.com.otgmobile.model.GeoPontoRota;
import br.com.otgmobile.model.Grupo;
import br.com.otgmobile.model.Rota;
import br.com.otgmobile.model.Usuario;
import br.com.otgmobile.model.events.Event;
import br.com.otgmobile.model.events.EventCondition;
import br.com.otgmobile.model.events.EventHistoryDetail;
import br.com.otgmobile.service.dao.CercaDAO;
import br.com.otgmobile.service.dao.EventConditionDAO;
import br.com.otgmobile.service.dao.EventHistoryDetailDAO;
import br.com.otgmobile.service.dao.GrupoDAO;
import br.com.otgmobile.service.dao.RotaDAO;
import br.com.otgmobile.service.dao.UsuarioDAO;
import br.com.otgmobile.service.socket.SocketIOTask;
import br.com.otgmobile.util.CercaUtil;

@Component
public class EventProcesser {
	
	@Autowired
	private EventConditionDAO eventConditionDAO;
	
	@Autowired
	private GrupoDAO grupoDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Autowired
	private RotaDAO rotaDAO;
	
	@Autowired
	private CercaDAO cercaDAO;
	
	@Autowired
	private EventHistoryDetailDAO eventHistoryDetailDAO;
	
	@Autowired
	private MailNotifiyer mailNotifiyer;
	
	@Autowired
	private HttpSMSNotifiyer smsNotifiyer;
	
	@Autowired
	private SocketIOTask socketIOTask;
	
	
	private static final Logger logger = Logger.getLogger(EventProcesser.class);
	private static final String EVENT_MESSAGE = "eventMessage";


	public Boolean isOgnlEvent(EventMessage eventMessage, String expression) throws OgnlException{
		Object ognlObject = Ognl.parseExpression(expression);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(EVENT_MESSAGE, eventMessage);
		return (Boolean) Ognl.getValue(ognlObject, map);
	}

	public Boolean enteredCercaEvent(EventMessage eventMessage, Cerca cerca, EventMessage previousMessage){
		if(previousMessage == null) return false;
		if(cerca == null) return false;
		GeoPontoCerca geoponto1 = new GeoPontoCerca();
		geoponto1.setLatitude(eventMessage.getLatitude());
		geoponto1.setLongitude(eventMessage.getLongitude());
		GeoPontoCerca geoponto2 = new GeoPontoCerca();
		geoponto2.setLatitude(previousMessage.getLatitude());
		geoponto2.setLongitude(previousMessage.getLongitude());
		boolean toReturn = (!CercaUtil.isInsideCercaWithGeometry(cerca, geoponto2) && CercaUtil.isInsideCercaWithGeometry(cerca, geoponto1));
		return toReturn;
	}

	public Boolean gotOutofCercaEvent(EventMessage eventMessage, Cerca cerca, EventMessage previousMessage){
		if(previousMessage == null) return false;

		if(cerca == null) return false;

		GeoPontoCerca geoponto1 = new GeoPontoCerca();
		geoponto1.setLatitude(eventMessage.getLatitude());
		geoponto1.setLongitude(eventMessage.getLongitude());
		GeoPontoCerca geoponto2 = new GeoPontoCerca();
		geoponto2.setLatitude(previousMessage.getLatitude());
		geoponto2.setLongitude(previousMessage.getLongitude());
		boolean a = CercaUtil.isInsideCercaWithGeometry(cerca, geoponto2);
		boolean b = !CercaUtil.isInsideCercaWithGeometry(cerca, geoponto1);
		boolean toReturn = (a && b);
		return toReturn;
	}

	public void processEvent(EventMessage position, Device device,EventMessage oldMessage) {
		if(device == null || position == null || device.getEvents() == null || device.getEvents().isEmpty()) return;
		for(Event event : device.getEvents()){
			if(event.getEnable() == true){
				boolean ocurred = true;
				List<EventCondition> conditions = eventConditionDAO.getEventConditionsFromEvent(event.getId());
				
				for(EventCondition condition : conditions){
					if(condition.isEnable()){

						switch (condition.getEventType().getValue()) {
						case 0:
							try {
								boolean	eventOcurred = isOgnlEvent(position, condition.getScriptCondition());
								ocurred = ocurred && eventOcurred;

							} catch (Throwable e) {
								logger.error(e);
							}

							break;
						case 1:
							List<Cerca> cercasIn = eventConditionDAO.getCercasFromEventConditions(condition.getId());
							for(Cerca cerca :cercasIn){
								cerca.setGeoPonto(cercaDAO.getAllGeoPoints(cerca.getId()));
								ocurred = ocurred && (enteredCercaEvent(position, cerca, oldMessage));
							}

							break;
						case 2:
							List<Cerca> cercasOut = eventConditionDAO.getCercasFromEventConditions(condition.getId());
							for(Cerca cerca :cercasOut){
								cerca.setGeoPonto(cercaDAO.getAllGeoPoints(cerca.getId()));
								ocurred = ocurred && (gotOutofCercaEvent(position, cerca, oldMessage));
							}
							break;
						case 3:
							List<Rota> rotasOut = eventConditionDAO.getRotasFromEventConditions(condition.getId());
							for(Rota rota :rotasOut){
								boolean gotOutofRotaEvent = (gotOutofRotaEvent(position,oldMessage ,rota));
								ocurred = ocurred && gotOutofRotaEvent;
								
							}
							break;
						case 4:
							List<Rota> rotasIn = eventConditionDAO.getRotasFromEventConditions(condition.getId());
							for(Rota rota :rotasIn){
								boolean gotEnterofRotaEvent = (gotEnterofRotaEvent(position,oldMessage ,rota));
								ocurred = ocurred && gotEnterofRotaEvent;
							}
							break;

						default:
							break;
						}
					}
				}
				if(ocurred){
					notify(addEventDetail(position, event,device),device,event);
				}
			}
		}
	}

	private boolean gotOutofRotaEvent(EventMessage position, EventMessage oldMessage,Rota rota) {
		rota = rotaDAO.find(rota);
		rota.setGeoPonto(rotaDAO.getAllGeoPoints(rota.getId()));
		for(GeoPontoRota ponto: rota.getGeoPonto()){
			if(LocatorUtil.isInRoute(position, ponto)){
				return false;
			}
		}
		for(GeoPontoRota ponto: rota.getGeoPonto()){
			if(LocatorUtil.isInRoute(oldMessage, ponto)){
				return true;
			}
		}
		return false;
	}
	
	private boolean gotEnterofRotaEvent(EventMessage position, EventMessage oldMessage,Rota rota) {
		rota = rotaDAO.find(rota);
		rota.setGeoPonto(rotaDAO.getAllGeoPoints(rota.getId()));
		for(GeoPontoRota ponto: rota.getGeoPonto()){
			if(LocatorUtil.isInRoute(oldMessage, ponto)){
				return false;
			}
		}
		for(GeoPontoRota ponto: rota.getGeoPonto()){
			if(LocatorUtil.isInRoute(position, ponto)){
				return true;
			}
		}
		return false;
	}

	private EventHistoryDetail addEventDetail(EventMessage position, Event event, Device device) {
		EventHistoryDetail eventHistoryDetail = new EventHistoryDetail();
		eventHistoryDetail.setEvent(event);
		double lat = position.getLatitude();
		double lon = position.getLongitude();
		eventHistoryDetail.setLatitude((float) lat);
		eventHistoryDetail.setLongitude((float) lon);
		eventHistoryDetail.setMetricDate(position.getDate());
		eventHistoryDetail.setDevice(device);
		return eventHistoryDetail;
	}
	
	private void notify(EventHistoryDetail eventHistoryDetail, Device device, Event event){
		HashMap<Long,Long> map = new HashMap<Long,Long>();
		eventHistoryDetailDAO.add(eventHistoryDetail);
		boolean sent = false;
		List<Grupo> grupos = grupoDAO.getGruposFromDevice(device.getId());
		for(Grupo grupo: grupos){
			List<Usuario> usuarios = usuarioDAO.getUsuariosFromGrupo(grupo
					.getId());
			for (Usuario usuario : usuarios) {
				if (!map.containsKey(usuario.getId())) {
					map.put(usuario.getId(), usuario.getId());
					sent = sendEmailNotification(eventHistoryDetail, sent, usuario);
					if (!sent) {
						sent = sendSMSNotification(eventHistoryDetail, sent, usuario);
					} else {
						sendSMSNotification(eventHistoryDetail, sent, usuario);
					}
				}
			}
		}
		if(!sent){
			sent = socketIOTask.enviaComWraper(eventHistoryDetail);
		}else{
			socketIOTask.enviaComWraper(eventHistoryDetail);			
		}
		eventHistoryDetail.setSent(sent);
		eventHistoryDetailDAO.merge(eventHistoryDetail);
	}

	private boolean sendEmailNotification(
			EventHistoryDetail eventHistoryDetail, boolean sent, Usuario usuario) {
		if(usuario.getMailNotifications() != null && usuario.getMailNotifications()){
			try {
				mailNotifiyer.sendMAilNotification(usuario, eventHistoryDetail);
				sent = true;
			} catch (Exception e) {
				logger.error("erro ao notificar por email"+usuario.getName(), e);
				sent = false;
			}
		}
		return sent;
	}
	

	private boolean sendSMSNotification(EventHistoryDetail eventHistoryDetail,boolean sent, Usuario usuario) {
		if(usuario.getSmsNotifications() != null && usuario.getSmsNotifications()){
			try {
				smsNotifiyer.sendSMSNotification(usuario, eventHistoryDetail);
				sent = true;
			} catch (Exception e) {
				logger.error("erro ao notificar por sms"+usuario.getName(), e);
				sent = false;
			}
		}
		return sent;
	}
	
}
