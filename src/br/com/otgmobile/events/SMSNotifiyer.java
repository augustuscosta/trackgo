package br.com.otgmobile.events;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

import scala.App;

import br.com.otgmobile.model.Usuario;
import br.com.otgmobile.model.events.EventHistoryDetail;

@Component
public class SMSNotifiyer {
	
	private static final String USERNAME = "username";
	private static final String COMPRESSION = "compression";
	private static final String SMSHOST = "smtphost";
	private static final String FROM = "from";
	private static final String PASSWORD = "password";
	private static String INIT = "O DISPOSITIVO ";
	private static String LAUNCH = " ATIVOU O EVENTO ";
	
	public void sendSMSNotification(Usuario usuario,EventHistoryDetail eventHistoryDetail) throws Exception{
		String body = INIT + eventHistoryDetail.getDevice().getName()+ LAUNCH+ eventHistoryDetail.getEvent().getName();
		Properties smsProps = new Properties() ;
		Properties props = System.getProperties();
		loadPropertites(smsProps, props);
		Session mailSession = Session.getDefaultInstance(props, null);
		Message msg = initAndSetMEssage(usuario, body, smsProps, mailSession);
		sendSMS(smsProps, mailSession, msg);
	}

	private void sendSMS(Properties smsProps, Session mailSession, Message msg)
			throws NoSuchProviderException, MessagingException {
		Transport myTransport;
		myTransport = mailSession.getTransport("smtp");
		myTransport.connect(smsProps.getProperty(SMSHOST), smsProps.getProperty(USERNAME), smsProps.getProperty(PASSWORD));
		myTransport.sendMessage(msg, msg.getAllRecipients());
		myTransport.close();
	}

	private void loadPropertites(Properties smsProps, Properties props)
			throws IOException {
		smsProps.load(App.class.getClassLoader().getResourceAsStream("sms.properties"));
		props.put("mail.smtp.auth", "true");
	}

	private Message initAndSetMEssage(Usuario usuario, String body,Properties smsProps, Session mailSession)
			throws MessagingException, AddressException {
		Message msg = new MimeMessage(mailSession);
		String to = usuario.getMobilephoneNumber()+smsProps.getProperty(SMSHOST);
		msg.saveChanges();
		setMEssage(body, smsProps, to, msg);
		return msg;
	}

	private void setMEssage(String body, Properties smsProps, String to,Message msg) throws MessagingException, AddressException {
		msg.setFrom(new InternetAddress(smsProps.getProperty(FROM)));
		InternetAddress[] address = {new InternetAddress(to)};
		msg.setRecipients(Message.RecipientType.TO, address);
		msg.setSubject(smsProps.getProperty(COMPRESSION));
		msg.setText(body);
		msg.setSentDate(new Date());
	}

}
