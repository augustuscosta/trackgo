package br.com.otgmobile.events;

import br.com.otgmobile.model.GeoPontoRota;

public class LocatorUtil {

	private static final double TOLERANCE_IN_KM = 0.05;
	private static Double EARTH_RADIUS = 6371.00; // Raio da terra em kilometros

	public static Double calculateDistance(Double lat1, Double lon1, Double lat2, Double lon2) {
		Double Radius = LocatorUtil.EARTH_RADIUS; // 6371.00;
		Double dLat = LocatorUtil.toRadians(lat2 - lat1);
		Double dLon = LocatorUtil.toRadians(lon2 - lon1);
		Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(LocatorUtil.toRadians(lat1))
				* Math.cos(LocatorUtil.toRadians(lat2)) * Math.sin(dLon / 2)
				* Math.sin(dLon / 2);
		Double c = 2 * Math.asin(Math.sqrt(a));
		return Radius * c;
	}

	public static boolean isInRoute(EventMessage position, GeoPontoRota ponto) {
		return LocatorUtil.calculateDistance(position.getLatitude(),
				position.getLongitude(), ponto.getLatitude(),
				ponto.getLongitude()) < TOLERANCE_IN_KM;
	}

	public static Double toRadians(Double degree) {
		return degree * 3.1415926 / 180;
	}
}
