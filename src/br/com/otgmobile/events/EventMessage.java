package br.com.otgmobile.events;

import java.util.Date;


public interface EventMessage {
	
	public Double getLatitude();
	public Double getLongitude();
	public Integer getAltitude();
	public Double getCourse();
	public Double getSpeed();
	public Boolean getMoving();
	public Date getDate();
	public long getDateInMills();
	public Boolean getIgnition();
	public Boolean getPanic();
	public Integer getRpm();
	public Boolean getOpenedDoor();
	public Boolean getEngineBlocked();
	public String getCode();
}
