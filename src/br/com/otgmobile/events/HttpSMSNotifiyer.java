package br.com.otgmobile.events;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import scala.App;
import br.com.otgmobile.model.Usuario;
import br.com.otgmobile.model.events.EventHistoryDetail;

@Component
public class HttpSMSNotifiyer {
	
	private static final String UTF_8 = "UTF-8";
	private static final Logger logger = Logger.getLogger(EventProcesser.class);
	private static final String MESSAGE = "&MESSAGE=";
	private static final String SEND_PROJECT = "&SEND_PROJECT=";
	private static final String MOBILE = "&MOBILE=";
	private static final String AUX_USER = "&AUX_USER=";
	private static final String USER = "user";
	private static final String PRINCIPAL_USER = "&PRINCIPAL_USER=";
	private static final String CREDENTIAL = "credential";
	private static final String HTTPSMS_PROPERTIES = "httpsms.properties";
	private static final String COMMA = ",";
	private static final String HTTPS_MAPS_LINK = "https://maps.google.com/maps?q=";
	private static final String CLICK_NO_LINK_PARA_VER_A_LOCALIZAÇÃO = " click no link para ver a localização ";
	private static String INIT = "O DISPOSITIVO ";
	private static String LAUNCH = " ATIVOU O EVENTO ";

	public void sendSMSNotification(Usuario usuario,EventHistoryDetail eventHistoryDetail) throws Exception {
		Properties props = new Properties();
		props.load(App.class.getClassLoader().getResourceAsStream(HTTPSMS_PROPERTIES));
		StringBuilder stringBuilder = new StringBuilder();
		populateBuilder(usuario, eventHistoryDetail, props, stringBuilder);
		sendRequest(stringBuilder);
	}

	private void sendRequest(StringBuilder stringBuilder)throws Exception {
		URL url = new URL(stringBuilder.toString());
		InputStream inputStream = url.openStream();
		byte[] b = new byte[4];
		inputStream.read(b, 0, b.length);
		logger.info(new String(b));
	}

	private void populateBuilder(Usuario usuario,EventHistoryDetail eventHistoryDetail, Properties props,StringBuilder stringBuilder) throws UnsupportedEncodingException {
		String msg = INIT + eventHistoryDetail.getDevice().getName()+ LAUNCH+ 
				eventHistoryDetail.getEvent().getName()+CLICK_NO_LINK_PARA_VER_A_LOCALIZAÇÃO+HTTPS_MAPS_LINK+eventHistoryDetail.getLatitude()+COMMA+eventHistoryDetail.getLongitude();
		msg = URLEncoder.encode(msg,UTF_8);
		stringBuilder.append("http://www.mpgateway.com/v_2_00/smspush/enviasms.aspx?CREDENCIAL=");
		stringBuilder.append(props.getProperty(CREDENTIAL));
		stringBuilder.append(PRINCIPAL_USER);
		stringBuilder.append(props.getProperty(USER));
		stringBuilder.append(AUX_USER);
		stringBuilder.append(props.getProperty(USER));
		stringBuilder.append(MOBILE);
		stringBuilder.append(usuario.getMobilephoneNumber());
		stringBuilder.append(SEND_PROJECT);
		stringBuilder.append("S");
		stringBuilder.append(MESSAGE);
		stringBuilder.append(msg);
	}
	
	

}
