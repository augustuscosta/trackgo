package br.com.otgmobile.util;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 27/02/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public class StringUtil {

	/**
	 * @param value
	 * @return True if value is not null and trim().length() > 0, otherwise false.
	 */
	public static boolean isValid(final String value) {
		return value != null && value.trim().length() > 0;
	}
	
	public static String fromByteArray(final byte[] bytes) {
		StringBuilder builder = new StringBuilder(bytes.length*2);
		for (byte b : bytes) {
			builder.append(String.format("%1$02X", b));
		}

		return builder.toString();
	}
	
	public static String parseIntegerToHex(Integer number){
		if(number == null) return null;
		
		return Integer.toHexString(number);
	}
	
	public static String parseLongToHex(Long number){
		if(number == null) return null;
		
		return Long.toHexString(number);
	}
	
	public static byte[] hexStringTobyteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		 for (int i = 0; i < len; i+=2) {
			 data[i/2] =  (byte) ((Character.digit(s.charAt(i), 16) << 4)
					 + Character.digit(s.charAt(i+1), 16));
		}
		 return data;
	}

}
