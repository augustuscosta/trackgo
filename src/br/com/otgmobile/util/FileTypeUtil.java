package br.com.otgmobile.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileTypeUtil {
	
	public static File[] getCMDFiles(File folder) {
	    List<File> aList = new ArrayList<File>();

	    File[] files = folder.listFiles();
	    if(files == null)
	    	return null;
	    for (File pf : files) {

	      if (pf.isFile() && getFileExtensionName(pf).indexOf("cmd") != -1) {
	        aList.add(pf);
	      }
	    }
	    return aList.toArray(new File[aList.size()]);
	  }

	public static File[] getXMLFiles(File folder) {
	    List<File> aList = new ArrayList<File>();

	    File[] files = folder.listFiles();
	    if(files == null)
	    	return null;
	    for (File pf : files) {

	      if (pf.isFile() && getFileExtensionName(pf).indexOf("xml") != -1) {
	        aList.add(pf);
	      }
	    }
	    return aList.toArray(new File[aList.size()]);
	  }

	  public static String getFileExtensionName(File f) {
	    if (f.getName().indexOf(".") == -1) {
	      return "";
	    } else {
	      return f.getName().substring(f.getName().length() - 3, f.getName().length());
	    }
	  }
}
