package br.com.otgmobile.util;

public class CRC16CCITT {

	public static int getCRC(String bytes) {
//		int[] msg = {0x55,0x21,0x11,0x22,0x51,0x85,0x01};
		int[] msg = new int[bytes.length()/2];
		int y=0;
		for (int i = 0; i < bytes.length(); i+=2) {
			msg[y] = Integer.parseInt(bytes.substring(i, i+2), 16);
			y++;
		}
		

		return getCRC(msg, 0x1021);
	}
	
	public static  String appendZeroToCRCifNeeded(int i){
		String a = Integer.toHexString(i);
		switch (a.length()) {
		
		case 1:
			return "000"+a;
			
		case 2:
			return "00"+a;
			
		case 3:
			return "0"+a;
  
		default:
			return a;
		}
		
	}
	
	public static String getHexCRC(String bytes){
		return appendZeroToCRCifNeeded(getCRC(bytes));
	}

	public static int getCRC(int[] bytes, int polynomial) {
		int crc = 0x0000;
		for (int b : bytes) {
			for (int i = 0; i < 8; i++) {
				boolean bit = ((b >> (7 - i) & 1) == 1);
				boolean c15 = ((crc >> 15 & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit) {
					crc ^= polynomial;
				}
			}
		}
		crc &= 0xffff;

		System.out.println("{CRC16 CCITT} Bytes=" + String.valueOf(bytes) + " | CRC=" + Integer.toHexString(crc));

		return crc;
	}

}
