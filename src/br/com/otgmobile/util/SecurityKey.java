package br.com.otgmobile.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SecurityKey {

	public static boolean check() throws Exception {   
        String data = "14/11/2014";
        Date date = null;  
        try {  
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
            date = (java.util.Date)formatter.parse(data); 
            if(new Date().getTime() > date.getTime()){
            	return false;
            }
            	
        } catch (ParseException e) {              
            throw e;  
        }
        return true;  
    }  
}
