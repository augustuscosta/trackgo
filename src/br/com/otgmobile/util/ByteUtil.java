package br.com.otgmobile.util;

import java.util.BitSet;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

public class ByteUtil {

	public static String crcCittCalulator(byte[] bytes){
		return Integer.toHexString(getCRC(bytes));
	}

	public static int getCRC(byte[] bytes) {
		return getCRC(bytes, 0x1021);
	}

	public static int getCRC(byte[] bytes, int polynomial) {
		int crc = 0x0000;
		for (int b : bytes) {
			for (int i = 0; i < 8; i++) {
				boolean bit = ((b >> (7 - i) & 1) == 1);
				boolean c15 = ((crc >> 15 & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit) {
					crc ^= polynomial;
				}
			}
		}
		crc &= 0xffff;
		return crc;
	}

	public static byte[] toByteArray(String s) {
		return DatatypeConverter.parseHexBinary(s);
	}

	public static byte[] fromBitSet(BitSet bits)  {  
		byte[] b = new byte[1];  

		for(int i = 0; i<bits.length(); i++) {  

			if(bits.get(i) == true){
				b[0] |= 1 <<i ;              
			}
		}

		return b;  
	}

	public static BitSet fromByteArray(byte[] bytes) {
		BitSet bits = new BitSet();

		for (int i=0; i<bytes.length*8; i++) {

			if ((bytes[bytes.length-i/8-1]&(1<<(i%8))) > 0) {
				bits.set(i);
			}
		}
		return bits;
	}

	public static byte[] fromHexString(final String encoded) {
		if ((encoded.length() % 2) != 0)
			throw new IllegalArgumentException("Input string must contain an even number of characters");

		final byte result[] = new byte[encoded.length()/2];
		final char enc[] = encoded.toCharArray();
		for (int i = 0; i < enc.length; i += 2) {
			StringBuilder curr = new StringBuilder(2);
			curr.append(enc[i]).append(enc[i + 1]);
			result[i/2] = (byte) Integer.parseInt(curr.toString(), 16);
		}
		return result;
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

	public static byte[] hexToBytes(String hexString) {
		HexBinaryAdapter adapter = new HexBinaryAdapter();
		byte[] bytes = adapter.unmarshal(hexString);
		return bytes;
	}
	
	public static String calculateSingleHexByte(Integer value) {
		if(value == null){
			return "00";
		}else if (value <16) {
			return "0"+Integer.toHexString(value);
		}else{
			return Integer.toHexString(value);
		}
	}
	
	public static String guessEncoding(byte[] bytes) {
	    String DEFAULT_ENCODING = "UTF-8";
	    org.mozilla.universalchardet.UniversalDetector detector =
	        new org.mozilla.universalchardet.UniversalDetector(null);
	    detector.handleData(bytes, 0, bytes.length);
	    detector.dataEnd();
	    String encoding = detector.getDetectedCharset();
	    detector.reset();
	    if (encoding == null) {
	        encoding = DEFAULT_ENCODING;
	    }
	    return encoding;
	}

}
