package br.com.otgmobile.util;

import java.io.File;
import java.util.Comparator;

public class FileComparator implements Comparator<File> {
	public int compare(File o1, File o2) {
		if (o1 == o2)
			return 0;
		return Long.valueOf(o1.lastModified()).compareTo(o2.lastModified());
	}
}
