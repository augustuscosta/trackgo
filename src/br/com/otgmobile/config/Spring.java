package br.com.otgmobile.config;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.server.MaxtrackCommandResponsesDaemon;
import br.com.otgmobile.server.MaxtrackCommandsDaemon;
import br.com.otgmobile.server.MaxtrackFTPDaemon;
import br.com.otgmobile.server.MaxtrackPositionsDaemon;
import br.com.otgmobile.server.NiagaraDaemon;
import br.com.otgmobile.server.NiagaraSocketManager;
import br.com.otgmobile.server.SkypatrolDaemon;
import br.com.otgmobile.server.TK10XDaemon;
import br.com.otgmobile.server.Tk10XSocketManager;
import br.com.otgmobile.service.aspicore.AspicoreService;
import br.com.otgmobile.service.dao.DeviceDAO;
import br.com.otgmobile.service.dao.EventHistoryDetailDAO;
import br.com.otgmobile.service.dao.MaxtrackDAO;
import br.com.otgmobile.service.skypatrol.SkypatrolService;
import br.com.otgmobile.service.socket.NodeInitializer;
import br.com.otgmobile.service.socket.SocketIOTask;
import br.com.otgmobile.service.tk103.TK103Service;

public class Spring {
	private static final Logger logger = Logger.getLogger(Spring.class);
	
	private static ClassPathXmlApplicationContext ctx; 
	
	public Spring() {
		if (ctx == null) {
			logger.info("Strating Spring...");
			ctx = new ClassPathXmlApplicationContext(
					"META-INF" + File.separator + "spring" + File.separator + "applicationContext.xml", 
					"META-INF" + File.separator + "spring" + File.separator + "applicationContext-persistence.xml");
			logger.info("Spring Started...");
		}
	}

	public Tk10XSocketManager getTk10XSocketManagerBean() {
		logger.debug(ctx);
		return ctx.getBean(Tk10XSocketManager.class);
	}
	
	public NiagaraDaemon getNiagaraDaemon(){
		return ctx.getBean(NiagaraDaemon.class);
	}
	
	public NiagaraSocketManager getNiagaraSocketManagerBean() {
		logger.debug(ctx);
		return ctx.getBean(NiagaraSocketManager.class);
	}
	
	public TK10XDaemon getTK10XDaemon(){
		return ctx.getBean(TK10XDaemon.class);
	}

	public TK103Service getTK103Service() {
		return ctx.getBean(TK103Service.class);
	}

	public AspicoreService getAspicoreService() {
		return ctx.getBean(AspicoreService.class);
	}
	
	public MaxtrackPositionsDaemon getMaxtrackPositionsDaemon(){
		return ctx.getBean(MaxtrackPositionsDaemon.class);
	}
	
	public MaxtrackFTPDaemon getMaxtrackFTPDaemon(){
		return ctx.getBean(MaxtrackFTPDaemon.class);
	}
	
	public SkypatrolDaemon getSkypatrolServiceDaemon(){
		return ctx.getBean(SkypatrolDaemon.class);
	}

	public SkypatrolService getSkypatrolServiceService() {
		return ctx.getBean(SkypatrolService.class);
	}
	
	public SocketIOTask getSocketIODaemon(){
		return ctx.getBean(SocketIOTask.class);
	}
	
	public MaxtrackCommandsDaemon getMaxtrackCommandsDaemon(){
		return ctx.getBean(MaxtrackCommandsDaemon.class);
	}
	
	public MaxtrackCommandResponsesDaemon getMaxtrackCommandResponsesDaemon(){
		return ctx.getBean(MaxtrackCommandResponsesDaemon.class);
	}
	
	public synchronized MaxtrackDAO getMaxtrackDAO(){
		return ctx.getBean(MaxtrackDAO.class);
	}
	
	public synchronized DeviceDAO getDeviceDAO(){
		return ctx.getBean(DeviceDAO.class);
	}
	
	public synchronized EventProcesser getEventProcesser(){
		return ctx.getBean(EventProcesser.class);
	}

	public synchronized EventHistoryDetailDAO getEventHistoryDetailDAO() {
		return ctx.getBean(EventHistoryDetailDAO.class);
	}
	
	public synchronized NodeInitializer getNodeInitializer() {
		return ctx.getBean(NodeInitializer.class);
	}
	
}
