package br.otgmobile.skypatrol.tests;

import static org.junit.Assert.assertEquals;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.model.skypatrol.SkypatrolMessage;
import br.com.otgmobile.model.skypatrol.SkypatrolParser;

public class SkypatrolParserTests {

	private final String message = "\0\4,005F,0,GTGEO,020100,135790246811220,,0,0,1,1,4.3,92,70.0,121.354335,31.222073,20 090214013254,0460,0000,18d8,6141,90,20090214093254,11F0$";
	private final String heartbeat = "\0\4,005F,0,GTHBD,020100,135790246811220,,20100214093254,11F0$";
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMessageParser() throws JAXBException {
		final SkypatrolParser parser = new SkypatrolParser(message);
		assertEquals(true, parser.isReportMessage());
		SkypatrolMessage skypatrolMessage = parser.getSkypatrolMessage();
		assertEquals(true, skypatrolMessage != null);
	}
	
	@Test
	public void testHeartBeat(){
		final SkypatrolParser parser = new SkypatrolParser(heartbeat);
		assertEquals(true, parser.isHeartbeat());
		assertEquals("135790246811220", parser.getHeartbeat().getUniqueID());
	}

}
