package br.otgmobile.events.tests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceContext;

import junit.framework.Assert;
import ognl.OgnlException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.events.EventProcesser;
import br.com.otgmobile.events.EventType;
import br.com.otgmobile.model.Cerca;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DeviceType;
import br.com.otgmobile.model.GeoPontoCerca;
import br.com.otgmobile.model.events.Event;
import br.com.otgmobile.model.events.EventCondition;
import br.com.otgmobile.model.niagarahw06.NiagaraHW06message;

public class NiagaraHw06EventTester {
	
	
	
	
	private static final String RANDOM_SPEEDING_EVENT = "eventMessage.getSpeed() > 60";
	
	@PersistenceContext
	private Device device;


	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testEvents() throws OgnlException{
		EventProcesser processer = new EventProcesser();
		NiagaraHW06message message = new NiagaraHW06message();
		message.setLatitude(-3.345972);
		message.setLongitude(-38.187759);
		message.setSpeed(120);
		message.setProductIdentification("123456789");
		NiagaraHW06message oldmessage = new NiagaraHW06message();
		oldmessage.setLatitude(-22.971451);
		oldmessage.setLongitude(-43.187218);
		oldmessage.setSpeed(50);
		oldmessage.setProductIdentification("123456789");
		Assert.assertTrue(processer.isOgnlEvent(message, RANDOM_SPEEDING_EVENT));
		Assert.assertFalse(processer.isOgnlEvent(oldmessage, RANDOM_SPEEDING_EVENT));
		Assert.assertTrue(processer.gotOutofCercaEvent(oldmessage, generateDefaultCerca(), message));
		Assert.assertTrue(processer.enteredCercaEvent(message, generateDefaultCerca(), oldmessage));
	}
	
	@Test
	public void testEventsOnPersistence(){
		
		
	}
	
	public void createDeviceWithEvents(){
		device = new Device();
		device.setCode("123456789");
		device.setDescription("NiagaraHW06TestDevice");
		device.setDeviceType(DeviceType.NIAGARAHW6);
		device.setEnabled(true);
		device.setName("TestDevice");
		device.setEvents(createEventList(device));
	}




	private List<Event> createEventList(Device device) {
		List<Event> events = new ArrayList<Event>();
		events.add(createOgnlEvent(device));
		events.add(createGotOutOfCercaEvent(device));
		events.add(createEnteredCercaEvent(device));
		return events;
	}
	
	private Event createOgnlEvent(Device device) {
		Event event= new Event();
		EventCondition eventCondition = new EventCondition();
		eventCondition.setEventType(EventType.OGNL_EVENT);
		eventCondition.setScriptCondition("EventConditon.getSpeed() > 60");
		eventCondition.setDescription("Excedendo velocidade");
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}
	
	private Event createGotOutOfCercaEvent(Device device) {
		Event event= new Event();
		EventCondition condition= new EventCondition();
		condition.setEventType(EventType.GOT_OUT_OF_CERCA);
		condition.setCercas(new ArrayList<Cerca>());
		condition.getCercas().add(generateDefaultCerca());
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}
	
	private Event createEnteredCercaEvent(Device device) {
		Event event= new Event();
		EventCondition condition= new EventCondition();
		condition.setEventType(EventType.ENTERED_CERCA);
		condition.setCercas(new ArrayList<Cerca>());
		condition.getCercas().add(generateDefaultCerca());
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}

	private Cerca generateDefaultCerca() {
		Cerca cerca = new Cerca();
		cerca.setGeoPonto(getGeopontosofCerca());
		return cerca;
	}

	private List<GeoPontoCerca> getGeopontosofCerca() {
		List<GeoPontoCerca> pontos = new ArrayList<GeoPontoCerca>();
		GeoPontoCerca geoponto = new GeoPontoCerca();
		geoponto.setLatitude(-3.3725813849249344);
		geoponto.setLongitude(-38.947213281250015);
		GeoPontoCerca geoponto1 = new GeoPontoCerca();
		geoponto1.setLatitude(-3.920783448052272);
		geoponto1.setLongitude(-39.068062890625015);
		GeoPontoCerca geoponto2 = new GeoPontoCerca();
		geoponto2.setLatitude(-4.457673245837118);
		geoponto2.setLongitude(-38.200142968750015);
		GeoPontoCerca geoponto3 = new GeoPontoCerca();
		geoponto3.setLatitude(-3.7563582795245964);
		geoponto3.setLongitude(-38.068307031250015);
		pontos.add(geoponto);
		pontos.add(geoponto1);
		pontos.add(geoponto2);
		pontos.add(geoponto3);
		return pontos;
	}


}
