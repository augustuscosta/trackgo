package br.otgmobile.events.tests;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import br.com.otgmobile.events.EventType;
import br.com.otgmobile.model.Cerca;
import br.com.otgmobile.model.Device;
import br.com.otgmobile.model.DeviceType;
import br.com.otgmobile.model.GeoPontoCerca;
import br.com.otgmobile.model.events.Event;
import br.com.otgmobile.model.events.EventCondition;
import br.com.otgmobile.service.dao.DeviceDAO;

public class MaxtrackEventTester {
	
	
	private DeviceDAO deviceDAO;


	@Before
	public void setUp() throws Exception {
		createDeviceWithEvents();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	public void testEvents(){
		
	}
	
	private void createDeviceWithEvents(){
		Device device = new Device();
		device.setCode("123456789");
		device.setDescription("MaxtrackTestDevice");
		device.setDeviceType(DeviceType.MAXTRACK);
		device.setEnabled(true);
		device.setName("TestDevice");
		device.setEvents(createEventList(device));
		deviceDAO().add(device);
	}


	private DeviceDAO deviceDAO() {
		if(deviceDAO == null){
			deviceDAO = new DeviceDAO();
		}
		
		return deviceDAO;
	}


	private List<Event> createEventList(Device device) {
		List<Event> events = new ArrayList<Event>();
		events.add(createOgnlEvent(device));
		events.add(createGotOutOfCercaEvent(device));
		events.add(createEnteredCercaEvent(device));
		return events;
	}
	
	private Event createOgnlEvent(Device device) {
		Event event= new Event();
		EventCondition eventCondition = new EventCondition();
		eventCondition.setEventType(EventType.OGNL_EVENT);
		eventCondition.setScriptCondition("EventConditon.getSpeed() > 60");
		eventCondition.setDescription("Excedendo velocidade");
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}
	
	private Event createGotOutOfCercaEvent(Device device) {
		Event event= new Event();
		EventCondition condition= new EventCondition();
		condition.setEventType(EventType.GOT_OUT_OF_CERCA);
		condition.setCercas(new ArrayList<Cerca>());
		condition.getCercas().add(generateDefaultCerca());
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}
	
	private Event createEnteredCercaEvent(Device device) {
		Event event= new Event();
		EventCondition condition= new EventCondition();
		condition.setEventType(EventType.ENTERED_CERCA);
		condition.setCercas(new ArrayList<Cerca>());
		condition.getCercas().add(generateDefaultCerca());
		event.setDevices(new ArrayList<Device>());
		event.getDevices().add(device);
		return event;
	}

	private Cerca generateDefaultCerca() {
		Cerca cerca = new Cerca();
		cerca.setGeoPonto(getGeopontosofCerca());
		return cerca;
	}

	private List<GeoPontoCerca> getGeopontosofCerca() {
		// TODO Auto-generated method stub
		return null;
	}


}
