package br.otgmobile.niagara2.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;

import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.model.niagara2.ParseProtocol;

public class ParseUtilTest {

	private String protocolo;
	private ParseProtocol parse;

	@Before
	public void init() {
		protocolo = "037647115876C1080002AB24F15829106AFC85806AF50000190301081048125101010000000000EC24";
		parse = new ParseProtocol(protocolo);
		System.out.println(parse.toString());
	}

	@Test
	public void testReverse() {
		assertEquals("58F124AB", parse.reverseHex("AB24F158"));
	}

	@Test
	public void testBinaryToInt() {
		assertEquals(new Integer(18), parse.binaryToInt("01011000111100010010010010101011", 20, 26));
	}

	@Test
	public void testHex() {
		assertEquals(new Integer(255), parse.hexToInteger("FF"));
	}

	@Test
	public void testHexToCoord() {
		assertEquals("-16.710531", parse.hexToCoordinate("FC6A1029"));
	}

	@Test
	public void testSubstring() {
		assertNotNull(parse.substringInProtocol(protocolo, 0, 2));
		assertEquals(new Integer(3), parse.getFirmWareVersion());
		assertEquals("", parse.substringInProtocol(protocolo, 0, 150));
	}

	@Test
	public void testFirmWareVersion() {
		assertNotNull(parse.getFirmWareVersion());
		assertEquals(new Integer(3), parse.getFirmWareVersion());
	}

	@Test
	public void testUniqueUnitIdentifier() {
		assertNotNull(parse.getUniqueUnitIdentifier());
		assertEquals("7647115876", parse.getUniqueUnitIdentifier());
	}

	@Test
	public void testEvenCode() {
		assertNotNull(parse.getEventCode());
		assertEquals("C1", parse.getEventCode());
	}

	@Test
	public void testSequenceControlNumber() {
		assertNotNull(parse.getSequenceControlNumber());
		assertEquals(new Integer(8), parse.getSequenceControlNumber());
	}

	@Test
	public void testRetryNumber() {
		assertNotNull(parse.getRetryNumber());
		assertEquals(new Integer(0), parse.getRetryNumber());
	}

	@Test
	public void testGenerationTimestamp() {
		assertNotNull(parse.getGenerationTimestamp());
//		assertTrue("Tem que ser um TimeStamp", (parse.getGenerationTimestamp() instanceof DateTime));
//		assertEquals(new DateTime(2012, 3, 24, 18, 18, 43), parse.getGenerationTimestamp());
	}

	@Test
	public void testGetLatitude() {
		assertNotNull(parse.getLatitude());
		assertEquals(new Double(-16.710531), parse.getLatitude());
	}

	@Test
	public void testGetLongitude() {
		assertNotNull(parse.getLongitude());
		assertEquals(new Double(-49.324905), parse.getLongitude());
	}

	@Test
	public void testGetSpeed() {
		assertNotNull(parse.getSpeed());
		assertEquals("Velocidade deve ser igual a 0", new Integer(0), parse.getSpeed());
	}

	@Test
	public void testGetDirection() {
		assertNotNull(parse.getDirection());
		assertEquals("Nesse  a direção deve ser 0", new Integer(0), parse.getDirection());
	}

	@Test
	public void testAltitude() {
		assertNotNull(parse.getAltitude());
		assertEquals("Nesse  a altura deve ser 319", new Integer(319), parse.getAltitude());
	}

	@Test
	public void testGpsStatus() {
		assertNotNull(parse.getGpsStatus());
		assertEquals(new Integer(1), parse.getGpsStatus());
	}

	@Test
	public void testNumberSatelits() {
		assertNotNull(parse.getNumberOfSatellites());
		assertEquals(new Integer(8), parse.getNumberOfSatellites());
	}

	@Test
	public void testIdSattelits() {
		assertNotNull(parse.getIdSatellitesView());
		assertEquals(new Integer(10481251), parse.getIdSatellitesView());
	}

	@Test
	public void testHdop() {
		assertNotNull(parse.getHdop());
		assertEquals("Nesse  o hdop deve ser 1", new Integer(1), parse.getHdop());
	}

	@Test
	public void testOdometer() {
		assertNotNull(parse.getOdometer());
		assertEquals("Nesse protocolo o Odometer deve ser 0", new Integer(0), parse.getOdometer());
	}
	
	@Test
	public void testStringToArray() {
		protocolo = "037647115876C1080002AB24F15829106AFC85806AF50000190301081048125101010000000000EC24";
		byte[] bytes = new byte[protocolo.length()/2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(protocolo.substring(i*2, (i+1)*2), 16);
		}
		File file = new File("tests/br/otgmobile/niagra2/tests/precise");
		FileOutputStream fis;
		try {
			fis = new FileOutputStream(file);
			fis.write(bytes,0,bytes.length);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		}
		
		
	}
}
