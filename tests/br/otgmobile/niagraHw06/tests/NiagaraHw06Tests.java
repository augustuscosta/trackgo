package br.otgmobile.niagraHw06.tests;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.model.niagarahw06.ComandoNiagaraHw06;
import br.com.otgmobile.model.niagarahw06.NiagaraHw06Parser;

public class NiagaraHw06Tests {

	private String messageInString = "55213180030301500F02BF500F0238FF8258D8FEFCBD8F0023025E000B0D000000000097C3";
									  
	@Before
	public void setUp() throws Exception {
	}


	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMessageParser() throws JAXBException{
		NiagaraHw06Parser parser = new NiagaraHw06Parser(messageInString);
		System.out.println(parser.toString());
		System.out.println(messageInString);
		
	}
	
	@Test
	public void teestComandSequenceControl(){
		for(int i =0; i <300; i++ ){
			ComandoNiagaraHw06 comando = new ComandoNiagaraHw06();
			comando.setProductserialnumber("112551");
			comando.setSent(false);
		}
	}
	
	
	public  String byteArrayToString(byte [] preciseBytes){
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < preciseBytes.length; i++) {
			builder.append(Integer.toHexString(preciseBytes[i] & 0xFF));
		}

		return builder.toString();
	}
	
	
	
	
}