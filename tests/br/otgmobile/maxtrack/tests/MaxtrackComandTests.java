package br.otgmobile.maxtrack.tests;


import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.model.maxtrack.Comando;
import br.com.otgmobile.model.maxtrack.ComandoEstado;
import br.com.otgmobile.model.maxtrack.ComandoTipo;
import br.com.otgmobile.model.maxtrack.Comandos;
import br.com.otgmobile.model.maxtrack.Parametro;
import br.com.otgmobile.model.maxtrack.ProtocoloComando;

public class MaxtrackComandTests {
	
	private final String path = "tests/br/otgmobile/maxtrack/tests/";

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testCommandXmlMarshaller() throws JAXBException{
		JAXBContext ctx = JAXBContext.newInstance(new Class[] {Comandos.class});
		Marshaller marshaller = ctx.createMarshaller();
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		
		String pathFile;
		
		Comandos comandos = new Comandos();
		comandos.setComandos(new ArrayList<Comando>());
		
		Comando  comando = new Comando();
		comando.setIdCommand(1L);
		comando.setProtocol(ProtocoloComando.MXT101);
		comando.setSerial("1");
		comando.setCommandTimeout(new Date());
		comando.setType(ComandoTipo.getType(ComandoTipo.ENABLE_MICROPHONE));
		comando.setAttempts(1);
		comando.setParameters(new ArrayList<Parametro>());
		Parametro parametro = new Parametro();
		parametro.setId(ComandoTipo.ENABLE_MICROPHONE.toString());
		parametro.setValue("1");
		comando.getParameters().add(parametro);
		comandos.getComandos().add(comando);
		
		pathFile = path + comando.getFileName();
		File file = new File(pathFile);
		marshaller.marshal(comandos, file);
		
		comandos = (Comandos) unmarshaller.unmarshal(file);
		Comando xmlComando = comandos.getComandos().get(0);
		assertEquals(comando.getIdCommand(), xmlComando.getIdCommand());
		assertEquals(comando.getProtocol(), xmlComando.getProtocol());
		assertEquals(comando.getType(), xmlComando.getType());
		
		file.delete();
		
	}
	
	@Test
	public void testReportXmlUnmarshaller() throws JAXBException{
		JAXBContext ctx = JAXBContext.newInstance(new Class[] {ComandoEstado.class});
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		File file = new File(path + "report.xml");
		ComandoEstado estado = (ComandoEstado) unmarshaller.unmarshal(file);
		assertEquals(estado.getSerial(), "1");
	}
	
	
	
	
}
