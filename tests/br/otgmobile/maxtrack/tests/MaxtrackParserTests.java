package br.otgmobile.maxtrack.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.otgmobile.model.maxtrack.Position;
import br.com.otgmobile.model.maxtrack.Positions;

public class MaxtrackParserTests {

	private Positions positions;
	
	@Before
	public void setUp() throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(new Class[] {Positions.class});
        Unmarshaller um = ctx.createUnmarshaller();
        positions = (Positions) um.unmarshal(new File("tests/br/otgmobile/maxtrack/tests/20090326100656.xml"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPositionObject() throws JAXBException {
		Position position = positions.getPositions().get(0);
		assertEquals(true, position.getFirmware() != null);
		assertEquals(true, position.getGps() != null);
		assertEquals(true, position.getHardwareMonitor() != null);
		assertEquals(true, position.getSerial() != null);
		assertEquals("7", position.getSerial());
	}

}
